#include "dynamic_array.h"
#include <stdlib.h>
#include <string.h>

static void _darray_resize(DArray* array)
{
    if (!array || !array->data) {
        return;
    }
    // Allocate double the capacity
    usize old_size = array->capacity * array->stride;
    usize new_capacity = 2 * array->capacity;
    usize new_size = new_capacity * array->stride;
    void* new_data = malloc(new_size);
    // Perform a shallow copy of the data
    memcpy(new_data, array->data, old_size);
    free(array->data);
    array->data = new_data;
    array->capacity = new_capacity;
}

DArray* darray_create(usize capacity, usize stride)
{
    usize total_size = capacity * stride;
    DArray* array = malloc(sizeof(DArray));
    // Set capacity
    array->capacity = capacity;
    // Initialise to zero elements
    array->size = 0;
    // Store the stride
    array->stride = stride;
    // Allocate the actual data
    void* data = malloc(total_size);
    array->data = data;
    return array;
}

DArray* darray_create_and_init(usize size, usize stride, void* data)
{
    DArray* array = darray_create(size, stride);
    for (usize index = 0; index < size; ++index) {
        usize next_address_offset = array->stride * index;
        memcpy(&array->data[next_address_offset], data, array->stride);
    }
    array->size = size;
    return array;
}

void darray_destroy(DArray* array)
{
    // Return on null pointers
    if (!array) {
        return;
    }
    // Free the inner data if it is non-null
    if (array->data) {
        free(array->data);
    }
    // Free the array as well
    free(array);
}

void darray_clear(DArray* array)
{
    // Return on null pointers
    if (!array) {
        return;
    }

    array->size = 0;
}

void* darray_get(DArray const* array, usize index)
{
    if (!array) {
        return NULL;
    }
    // Bounds check
    if (index >= array->size) {
        return NULL;
    }
    usize address_offset = array->stride * index;
    return &array->data[address_offset];
}

void darray_set(DArray const* array, usize index, void* data)
{
    if (!array) {
        return;
    }
    // Bounds check
    if (index >= array->size) {
        return;
    }
    usize address_offset = array->stride * index;
    memcpy(&array->data[address_offset], data, array->stride);
}

// NOTE: This only removes the data at index from the darray
//       It does not free the data!
b8 darray_remove(DArray* array, usize index)
{
    if (!array || index >= array->size) {
        return false;
    }

    usize address_offset = array->stride * index;
    usize items_to_right = array->size - 1 - index;
    // Shift items after to fill in space
    if (items_to_right > 0) {
        usize next_address_offset = array->stride * (index + 1);
        usize right_size = array->stride * items_to_right;
        memcpy(&array->data[address_offset], &array->data[next_address_offset], right_size);
    }

    // Decrement size
    array->size--;
    return true;
}

// Removes an item at index and fills in the gap with the last element.
// This is will change the order of items.
b8 darray_swap_remove(DArray* array, usize index)
{
    if (!array || index >= array->size) {
        return false;
    }

    usize address_offset = array->stride * index;
    usize items_to_right = array->size - 1 - index;
    // Copy over the last element
    if (items_to_right > 0) {
        usize last_item_offset = array->stride * (array->size - 1);
        memcpy(&array->data[address_offset], &array->data[last_item_offset], array->stride);
    }
    // Decrement size
    array->size--;
    return true;
}

// NOTE: Assumed that the data has been allocated at least `stride` bytes.
b8 darray_push(DArray* array, void* data)
{
    if (!array) {
        return false;
    }
    // We aren't doing any resizing here!
    if (array->size + 1 > array->capacity) {
        _darray_resize(array);
    }
    usize next_address_offset = array->stride * array->size;
    memcpy(&array->data[next_address_offset], data, array->stride);
    // Increment size
    array->size++;
    return true;
}

void* darray_iterator_next(DArrayIterator* iterator)
{
    // Null check iterator pointer
    if (!iterator) {
        return NULL;
    }
    // Null check iterator array
    if (!iterator->array || !iterator->array->data) {
        return NULL;
    }

    if (iterator->index >= iterator->array->size) {
        return NULL;
    }

    return darray_get(iterator->array, iterator->index++);
}

// Same as darray_iterator_next but also gives the index
void* darray_iterator_enumerate_next(DArrayIterator* iterator, usize* index)
{
    // Null check iterator pointer
    if (!iterator) {
        return NULL;
    }
    // Null check iterator array
    if (!iterator->array || !iterator->array->data) {
        return NULL;
    }

    if (iterator->index >= iterator->array->size) {
        return NULL;
    }

    if (index) {
        *index = iterator->index;
    }
    return darray_get(iterator->array, iterator->index++);
}
