#pragma once
#include "defines.h"

typedef struct
{
    void* data;
    usize capacity;
    usize size;
    usize stride;
} DArray;

DArray* darray_create(usize capacity, usize stride);
DArray* darray_create_and_init(usize capacity, usize stride, void* data);
void darray_destroy(DArray* array);
void darray_clear(DArray* array);
void* darray_get(DArray const* array, usize index);
void darray_set(DArray const* array, usize index, void* data);
b8 darray_remove(DArray* array, usize index);
b8 darray_swap_remove(DArray* array, usize index);
[[nodiscard]] b8 darray_push(DArray* array, void* data);

typedef struct
{
    DArray const* array;
    usize index;
} DArrayIterator;

void* darray_iterator_next(DArrayIterator* iterator);
void* darray_iterator_enumerate_next(DArrayIterator* iterator, usize* index);
