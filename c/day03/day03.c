#include "defines.h"
#include "dynamic_array.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
    u32 value;
    i32 row_index;
    i32 column_start;
    i32 column_end;
} Token;

typedef struct {
    u8 const* buffer;
    DArray const* numeric_tokens;
    DArray const* symbolic_tokens;
} Parser;

void parser_destroy(Parser* parser)
{
    darray_destroy((DArray*)parser->numeric_tokens);
    darray_destroy((DArray*)parser->symbolic_tokens);
}

b8 parse_number(u8 const* buffer, usize start, usize size, u32* out_number)
{
    usize index = 0;
    u32 result = 0;
    while (index >= 0 && index < size) {
        u8 character = buffer[start + index];
        if (character >= '0' && character <= '9') {
            result = 10 * result + (character - '0');
        } else {
            printf("Unexpected non-numeric character %c |%d| while parsing a number\n", character, character);
            return false;
        }
        ++index;
    }

    *out_number = result;
    return true;
}

b8 create_numeric_token(u8 const* buffer, usize start, usize size, usize row_index, usize column_index, Token* out_token)
{
    u32 numeric_value;
    if (!parse_number(buffer, start, size, &numeric_value)) {
        printf("Failed to parse numeric token %.*s\n", (int)size, &buffer[start]);
        return false;
    }
    Token numeric_token = { .value = numeric_value, .row_index = row_index, .column_start = column_index, .column_end = column_index + size };
    *out_token = numeric_token;
    return true;
}

Parser* tokenise_input(u8 const* buffer)
{
    DArray* numeric_tokens = darray_create(10, sizeof(Token));
    DArray* symbolic_tokens = darray_create(10, sizeof(Token));

    usize index = 0;
    u8 character;
    usize row_index = 0;
    usize column_index = 0;
    b8 parse_began = false;
    usize start;
    while ((character = buffer[index]) != '\0') {
        b8 is_numeric = character >= '0' && character <= '9';
        if (is_numeric) {
            if (!parse_began) {
                start = index;
                parse_began = true;
            }
        }
        // Finish parse of number
        else {
            if (parse_began) {
                Token numeric_token;
                usize token_length = index - start;
                if (!create_numeric_token(buffer, start, token_length, row_index, column_index - token_length, &numeric_token)) {
                    printf("Failed to parse numeric token!\n");
                    darray_destroy(numeric_tokens);
                    darray_destroy(symbolic_tokens);
                    return NULL;
                }
                if (!darray_push(numeric_tokens, &numeric_token)) {
                    printf("Failed to push token into token array\n");
                    darray_destroy(numeric_tokens);
                    darray_destroy(symbolic_tokens);
                    return NULL;
                }
                start = 0;
                parse_began = false;
            }
        }
        b8 is_delimiter = character == '.' || character == '\r' || character == '\n';
        // Symbol token
        if (!is_numeric && !is_delimiter) {
            Token symbolic_token = { .value = character, .row_index = row_index, .column_start = column_index, .column_end = column_index + 1 };
            if (!darray_push(symbolic_tokens, &symbolic_token)) {
                printf("Failed to push token into token array\n");
                darray_destroy(numeric_tokens);
                darray_destroy(symbolic_tokens);
                return NULL;
            }
        }
        if (character == '\n') {
            ++row_index;
            column_index = 0;
        } else {
            ++column_index;
        }
        ++index;
    }
    Parser* parser = malloc(sizeof(Parser));
    parser->buffer = buffer;
    parser->numeric_tokens = numeric_tokens;
    parser->symbolic_tokens = symbolic_tokens;
    return parser;
}

void calculate_sums(Parser const* parser, usize* out_part_number_sum, usize* out_gear_ratio_sum)
{
    usize gear_ratio_sum = 0;
    usize part_number_sum = 0;
    usize symbolic_index = 0;
    Token* symbolic_token;
    usize num_numeric_tokens = parser->numeric_tokens->size;
    b8* global_touched = (b8*)calloc(num_numeric_tokens, sizeof(b8));
    b8* local_touched = (b8*)calloc(num_numeric_tokens, sizeof(b8));
    while ((symbolic_token = (Token*)darray_get(parser->symbolic_tokens, symbolic_index++)) != NULL) {
        u8 adjacent_numbers = 0;
        usize gear_ratio = 1;
        memset(local_touched, 0, sizeof(*local_touched));
        for (i32 row_offset = -1; row_offset <= 1; ++row_offset) {
            for (i32 column_offset = -1; column_offset <= 1; ++column_offset) {
                if (row_offset == 0 && column_offset == 0) {
                    continue;
                }
                Token* numeric_token;
                usize numeric_index = 0;
                while ((numeric_token = (Token*)darray_get(parser->numeric_tokens, numeric_index)) != NULL) {
                    // Iterate over all adjacent cells
                    i32 adjacent_row = symbolic_token->row_index + row_offset;
                    i32 adjacent_column = symbolic_token->column_start + column_offset;
                    // Iterate over all part numbers
                    if (numeric_token->row_index == adjacent_row && (numeric_token->column_start <= adjacent_column && adjacent_column < numeric_token->column_end)) {
                        if (!local_touched[numeric_index]) {
                            gear_ratio *= numeric_token->value;
                            adjacent_numbers++;
                            local_touched[numeric_index] = true;
                        }
                        if (!global_touched[numeric_index]) {
                            part_number_sum += numeric_token->value;
                            global_touched[numeric_index] = true;
                        }
                        break;
                    }
                    ++numeric_index;
                }
            }
        }
        if (symbolic_token->value == '*' && adjacent_numbers == 2) {
            gear_ratio_sum += gear_ratio;
        }
    }
    free(global_touched);
    free(local_touched);
    *out_part_number_sum = part_number_sum;
    *out_gear_ratio_sum = gear_ratio_sum;
}

int main(int argc, char** argv)
{
    // Get the input path (required)
    if (argc < 2) {
        printf("Usage: %s <input_path>\n", argv[0]);
        printf("Please provide a path to the input file as a command line argument.\n");
        return -1;
    }
    char const* input_path = argv[1];

    // Read in file contents
    FILE* handle;
    i32 open_error = fopen_s(&handle, input_path, "rb");
    if (open_error) {
        printf("Could not open file %s!\n", input_path);
        return -2;
    }
    fseek(handle, 0L, SEEK_END);
    usize file_size = ftell(handle);
    fseek(handle, 0L, SEEK_SET);
    u8* buffer = (u8*)malloc(file_size + 1);
    buffer[file_size] = '\0';
    fread(buffer, 1, file_size, handle);
    Parser const* parser = tokenise_input(buffer);
    usize part_number_sum;
    usize gear_ratio_sum;
    calculate_sums(parser, &part_number_sum, &gear_ratio_sum);
    printf("Part 1 - Result = %llu\n", part_number_sum);
    printf("Part 2 - Result = %llu\n", gear_ratio_sum);
    parser_destroy((Parser*)parser);
    free(buffer);

    return 0;
}
