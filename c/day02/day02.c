#include "defines.h"
#include "dynamic_array.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char const* delimiters = " \r:;,";
char const* special_delimiters = "\n";

typedef struct {
    usize start;
    usize size;
} Token;

typedef struct {
    u8 const* buffer;
    DArray const* tokens;
} Parser;

void parser_destroy(Parser* parser)
{
    darray_destroy((DArray*)parser->tokens);
}

void print_tokens(Parser const* parser)
{
    DArrayIterator iterator = { .array = parser->tokens, .index = 0 };
    Token* current_token;
    usize index;
    while ((current_token = (Token*)darray_iterator_enumerate_next(&iterator, &index)) != NULL) {
        if (!memcmp(&parser->buffer[current_token->start], "\n", 1)) {
            printf("Token %llu = `NewLine`\n", index);
        } else {
            printf("Token %llu = %.*s\n", index, (int)current_token->size, &parser->buffer[current_token->start]);
        }
    }
}

b8 parse_numeric_token(Parser const* parser, usize token_index, usize* out_value)
{
    usize number = 0;
    Token* current_token = (Token*)darray_get(parser->tokens, token_index);
    usize index = 0;
    while (index >= 0 && index < current_token->size) {
        u8 character = parser->buffer[current_token->start + index];
        // Digit
        if (character >= '0' && character <= '9') {
            number = number * 10 + (character - '0');
        } else {
            return false;
        }
        ++index;
    }

    *out_value = number;
    return true;
}

[[nodiscard]] Parser* tokenise(u8 const* buffer, u8 const* delimiters, u8 const* special_delimiters)
{
    DArray* tokens = darray_create(10, sizeof(Token));
    usize index = 0;
    u8 character;
    Token current_token = { 0 };
    b8 parse_began = false;
    while ((character = buffer[index]) != '\0') {
        b8 is_delimiter = strchr((char const*)delimiters, character) != NULL;
        b8 is_special_delimiter = strchr((char const*)special_delimiters, character) != NULL;
        if (is_special_delimiter) {
            current_token.start = index;
            current_token.size = 1;
            if (!darray_push(tokens, &current_token)) {
                printf("Failed to push token into dynamic array\n");
                return NULL;
            }
            parse_began = false;
        } else if (!is_delimiter) {
            if (!parse_began) {
                current_token.start = index;
                parse_began = true;
            }
        } else {
            if (parse_began) {
                current_token.size = (index - current_token.start);
                if (!darray_push(tokens, &current_token)) {
                    printf("Failed to push token into dynamic array\n");
                    return NULL;
                }
            }
            parse_began = false;
        }
        ++index;
    }
    Parser* parser = malloc(sizeof(Parser));
    parser->buffer = buffer;
    parser->tokens = tokens;
    return parser;
}

b8 calculate_possible_game_sum(Parser const* parser, int* out_possible_game_sum, int* out_game_power_sum)
{
    // Convenience handle
    DArray const* tokens = parser->tokens;
    // Initialise indices and counters
    int possible_game_sum = 0;
    int game_power_sum = 0;
    usize current_red = 0;
    usize current_green = 0;
    usize current_blue = 0;
    usize game_id;
    usize index = 0;
    while (index < tokens->size) {
        Token* current_token = (Token*)darray_get(tokens, index);
        // Finish game
        if (!memcmp(&parser->buffer[current_token->start], "\n", 1)) {
            if (current_red <= 12 && current_green <= 13 && current_blue <= 14) {
                possible_game_sum += game_id;
            }
            game_power_sum += current_red * current_green * current_blue;
            current_red = 0;
            current_green = 0;
            current_blue = 0;
        }
        // New game
        else if (!memcmp(&parser->buffer[current_token->start], "Game", 4)) {
            if (!parse_numeric_token(parser, ++index, &game_id)) {
                Token* next_token = (Token*)darray_get(tokens, index);
                printf("Failed to parse token as numeric = %.*s\n", (int)next_token->size, &parser->buffer[next_token->start]);
                return false;
            }
        }
        // Should only be numeric tokens
        else {
            usize count;
            if (!parse_numeric_token(parser, index++, &count)) {
                printf("Failed to parse token as numeric = %.*s\n", (int)current_token->size, &parser->buffer[current_token->start]);
                return false;
            }
            Token* next_token = (Token*)darray_get(tokens, index);
            // Red
            if (!memcmp(&parser->buffer[next_token->start], "red", 3)) {
                current_red = max(count, current_red);
            } else if (!memcmp(&parser->buffer[next_token->start], "green", 5)) {
                current_green = max(count, current_green);
            } else if (!memcmp(&parser->buffer[next_token->start], "blue", 4)) {
                current_blue = max(count, current_blue);
            } else {
                printf("Unexpected token %.*s at token index %llu\n", (int)next_token->size, &parser->buffer[next_token->start], index);
                print_tokens(parser);
                return 0;
            }
        }
        ++index;
    }

    *out_possible_game_sum = possible_game_sum;
    *out_game_power_sum = game_power_sum;
    return true;
}

int main(int argc, char** argv)
{
    // Get the input path (required)
    if (argc < 2) {
        printf("Usage: %s <input_path>\n", argv[0]);
        printf("Please provide a path to the input file as a command line argument.\n");
        return -1;
    }
    char const* input_path = argv[1];

    // Read in file contents
    FILE* handle;
    i32 open_error = fopen_s(&handle, input_path, "rb");
    if (open_error) {
        printf("Could not open file %s!\n", input_path);
        return -2;
    }
    fseek(handle, 0L, SEEK_END);
    usize file_size = ftell(handle);
    fseek(handle, 0L, SEEK_SET);
    u8* buffer = (u8*)malloc(file_size + 1);
    buffer[file_size] = '\0';
    fread(buffer, 1, file_size, handle);
    Parser const* parser = tokenise(buffer, (u8 const*)delimiters, (u8 const*)special_delimiters);
    if (!parser) {
        printf("Failed to tokenise\n");
    }
    int possible_game_sum;
    int game_power_sum;
    b8 success = calculate_possible_game_sum(parser, &possible_game_sum, &game_power_sum);
    if (!success) {
        printf("Failed to parse games!\n");
        parser_destroy((Parser*)parser);
        free(buffer);
        return -3;
    }
    printf("Part 1 - Result = %d\n", possible_game_sum);
    printf("Part 2 - Result = %d\n", game_power_sum);
    parser_destroy((Parser*)parser);
    free(buffer);

    return 0;
}
