#include <ctype.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdbool.h>
#include <stdint.h>

typedef struct
{
    char *start;
    int size;
} Token;

typedef struct LinkedListNode
{
    uint64_t value;
    uint64_t new_value;
    bool visited;
    struct LinkedListNode *next;
} LinkedListNode;

LinkedListNode *get_from_list(LinkedListNode *root, int index)
{
    if (root == NULL)
    {
        printf("Can't index a null node!\n");
        return NULL;
    }

    int count = 0;
    LinkedListNode *current = root;
    while (current != NULL)
    {
        if (count == index)
        {
            return current;
        }
        current = current->next;
        count++;
    }
    printf("Could not find index!\n");
    return NULL;
}

void set_list_index(LinkedListNode *root, int index, uint64_t value)
{
    if (root == NULL)
    {
        printf("Can't assign to a null node!\n");
        return;
    }

    int count = 0;
    LinkedListNode *current = root;
    while (current != NULL)
    {
        if (count == index)
        {
            current->value = value;
            return;
        }
        current = current->next;
        count++;
    }
    printf("Could not find index!\n");
    return;
}

void append_to_list(LinkedListNode *root, LinkedListNode *tail)
{
    if (root == NULL)
    {
        printf("Can't append to a null node!\n");
        return;
    }
    LinkedListNode *current = root;
    while (current->next != NULL)
    {
        current = current->next;
    }
    current->next = tail;
    tail->next = NULL;
}

void update_list(LinkedListNode *root)
{
    if (root == NULL)
    {
        printf("Can't update a null node!\n");
        return;
    }
    LinkedListNode *current = root;
    while (current != NULL)
    {
        if (current->visited)
        {
            current->value = current->new_value;
            current->new_value = 0;
            current->visited = false;
        }
        current = current->next;
    }
}

void print_list(LinkedListNode *root)
{
    if (root == NULL)
    {
        printf("Can't print a null node!\n");
        return;
    }
    int index = 0;
    LinkedListNode *current = root;
    while (current != NULL)
    {
        printf("Index %d: %llu\n", index++, current->value);
        current = current->next;
    }
    printf("List has %d elements\n", index);
}

int get_list_size(LinkedListNode *root)
{
    if (root == NULL)
    {
        printf("Can't calculate the length of a null node!\n");
        return 0;
    }
    int index = 0;
    LinkedListNode *current = root;
    while (current != NULL)
    {
        index++;
        current = current->next;
    }
    return index;
}

uint64_t get_min_value_from_list(LinkedListNode *root)
{
    if (root == NULL)
    {
        printf("Can't get the minimum value of a null node!\n");
        return 0;
    }
    uint64_t min_value = UINT64_MAX;
    LinkedListNode *current = root;
    while (current != NULL)
    {
        if (current->value < min_value)
        {
            min_value = current->value;
        }
        current = current->next;
    }
    return min_value;
}

typedef enum
{
    SEEDS,
    SEED_TO_SOIL,
    SOIL_TO_FERTILISER,
    FERTILISER_TO_WATER,
    WATER_TO_LIGHT,
    LIGHT_TO_TEMPERATURE,
    TEMPERATURE_TO_HUMIDITY,
    HUMIDITY_TO_LOCATION,
} ParseState;

typedef enum
{
    RANGE_DEST,
    RANGE_SRC,
    RANGE_LENGTH,
} ListState;

uint64_t powers_of_ten[20] = {
    1ull,
    10ull,
    100ull,
    1000ull,
    10000ull,
    100000ull,
    1000000ull,
    10000000ull,
    100000000ull,
    1000000000ull,
    10000000000ull,
    100000000000ull,
    1000000000000ull,
    10000000000000ull,
    100000000000000ull,
    1000000000000000ull,
    10000000000000000ull,
    100000000000000000ull,
    1000000000000000000ull,
    10000000000000000000ull};

bool parse_token(Token *token, uint64_t *value)
{
    uint64_t result = 0;
    for (int i = 0; i < token->size; ++i)
    {
        char character = token->start[i];
        if (!isdigit(character))
        {
            return false;
        }
        else
        {
            int digit = character - '0';
            result += digit * powers_of_ten[token->size - i - 1];
        }
    }
    *value = result;
    return true;
}

bool token_compare(Token *token, const char *literal)
{
    int result = strncmp(token->start, literal, token->size);
    return !result;
}

uint64_t get_min_location(const char *text)
{
    ParseState parse_state = SEEDS;
    ListState list_state = RANGE_DEST;
    LinkedListNode *seed_map = NULL;
    char character = text[0];
    uint64_t seed_ids[10];
    int i = 0;
    int token_start = -1;
    int64_t offset = 0;
    uint64_t dest_start = 0;
    uint64_t src_start = 0;
    while ((character = text[i]) != '\0')
    {
        character = text[i];
        if ((character == ' ' || character == '\n' || character == ':') && token_start != -1)
        {
            int token_size = i - token_start;
            Token token = {(char *)(text + token_start), token_size};
            uint64_t value;
            if (parse_token(&token, &value))
            {
                if (parse_state != SEEDS)
                {
                    switch (list_state)
                    {
                    case RANGE_DEST:
                        // printf("Destination range start is %llu\n", value);
                        list_state = RANGE_SRC;
                        dest_start = value;
                        break;
                    case RANGE_SRC:
                        // printf("Source range start is %llu\n", value);
                        list_state = RANGE_LENGTH;
                        src_start = value;
                        offset = dest_start - src_start;
                        // printf("Offset = %lld\n", offset);
                        break;
                    case RANGE_LENGTH:
                        uint64_t range_length = value;
                        uint64_t src_end = src_start + range_length;
                        // printf("The range of src values is [%llu, %llu)\n", src_start, src_end);
                        list_state = RANGE_DEST;
                        for (int i = 0; i < get_list_size(seed_map); ++i)
                        {
                            LinkedListNode *current_node = get_from_list(seed_map, i);
                            uint64_t node_value = current_node->value;
                            printf("Seed %d: %llu\n", i, node_value);
                            if (node_value >= src_start && node_value < src_end)
                            {
                                uint64_t new_value = node_value + offset;
                                // printf("Value %llu going to %llu\n", node_value, new_value);
                                current_node->new_value = new_value;
                                current_node->visited = true;
                            }
                            else if (!current_node->visited)
                            {
                                printf("Unvisited node %llu\n", node_value);
                                current_node->new_value = node_value;
                            }
                        }
                        break;
                    default:
                        // printf("Something went wrong in the list state!\n");
                        break;
                    }
                }
                switch (parse_state)
                {
                case SEEDS:
                    if (seed_map == NULL)
                    {
                        seed_map = (LinkedListNode *)malloc(sizeof(LinkedListNode));
                        seed_map->value = value;
                        seed_map->new_value = 0;
                        seed_map->visited = 0;
                        seed_map->next = NULL;
                    }
                    else
                    {
                        LinkedListNode *new_node = (LinkedListNode *)malloc(sizeof(LinkedListNode));
                        new_node->value = value;
                        new_node->new_value = 0;
                        new_node->visited = 0;
                        new_node->next = NULL;
                        append_to_list(seed_map, new_node);
                        printf("Added seed %llu\n", value);
                        print_list(seed_map);
                    }
                    break;
                default:
                    break;
                }
            }
            else
            {
                if (token_compare(&token, "seeds"))
                {
                    parse_state = SEEDS;
                }
                else if (token_compare(&token, "seed-to-soil"))
                {
                    parse_state = SEED_TO_SOIL;
                    printf("\tSeed to soil\n");
                    update_list(seed_map);
                    print_list(seed_map);
                }
                else if (token_compare(&token, "soil-to-fertilizer"))
                {
                    parse_state = SOIL_TO_FERTILISER;
                    printf("\tSoil to fertiliser\n");
                    update_list(seed_map);
                    print_list(seed_map);
                }
                else if (token_compare(&token, "fertilizer-to-water"))
                {
                    parse_state = FERTILISER_TO_WATER;
                    printf("\tFertiliser to water\n");

                    update_list(seed_map);
                    print_list(seed_map);
                }
                else if (token_compare(&token, "water-to-light"))
                {
                    parse_state = WATER_TO_LIGHT;
                    printf("\tWater to light\n");
                    update_list(seed_map);
                    print_list(seed_map);
                }
                else if (token_compare(&token, "light-to-temperature"))
                {
                    parse_state = LIGHT_TO_TEMPERATURE;
                    printf("\tLight to temperature\n");
                    update_list(seed_map);
                    print_list(seed_map);
                }
                else if (token_compare(&token, "temperature-to-humidity"))
                {
                    parse_state = TEMPERATURE_TO_HUMIDITY;
                    printf("\tTemperature to humidity\n");
                    update_list(seed_map);
                    print_list(seed_map);
                }
                else if (token_compare(&token, "humidity-to-location"))
                {
                    parse_state = HUMIDITY_TO_LOCATION;
                    printf("\tHumidity to location\n");
                    update_list(seed_map);
                    print_list(seed_map);
                }
                else if (token_compare(&token, "map"))
                {
                }
                else
                {
                    printf("Unknown token!\n");
                }
            }
            // Reset the token start position
            token_start = -1;
        }
        else if (token_start == -1 && isalnum(character))
        {
            // printf("Start of token at byte %d is %d\n", i, character);
            token_start = i;
        }
        ++i;
    }
    update_list(seed_map);
    print_list(seed_map);
    uint64_t min_value = get_min_value_from_list(seed_map);
    return min_value;
}

int main(int argc, char **argv)
{
    if (argc < 2)
    {
        printf("Please give the path to the input file!\n");
        return -1;
    }
    FILE *handle;
    int err = fopen_s(&handle, argv[1], "r");
    if (err == EINVAL)
    {
        printf("Could not open file at path %s!\n", argv[1]);
        return -2;
    }

    // Get file size
    fseek(handle, 0, SEEK_END);
    int file_size = ftell(handle);
    fseek(handle, 0, SEEK_SET);
    printf("The given file contains %d bytes\n", file_size);

    // Read in text into buffer
    char *text_buffer = malloc(file_size + 1);
    memset(text_buffer, 0, file_size + 1);
    int character;
    int i = 0;
    while ((character = fgetc(handle)) != EOF)
    {
        text_buffer[i++] = character;
    }
    uint64_t min_value = get_min_location(text_buffer);
    printf("Part 1 - Result = %llu\n", min_value);

    free(text_buffer);
    if (fclose(handle) == EOF)
    {
        printf("Could not close file handle!\n");
    }
    return 0;
}
