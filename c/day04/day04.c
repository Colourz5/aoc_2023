#include "defines.h"
#include "dynamic_array.h"
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TRY_OR_ELSE(condition, default) \
    if (!condition) {                   \
        return default;                 \
    }

char const* delimiters = " \r:;,";
char const* special_delimiters = "|\n";

typedef struct {
    usize start;
    usize length;
} Token;

typedef struct {
    u8 const* buffer;
    DArray* tokens;
} Parser;

b8 compare_token_to_str(u8 const* buffer, Token token, char const* comparison)
{
    // Must be the same length
    if (token.length != strlen(comparison)) {
        return false;
    }

    for (usize index = 0; index < token.length; ++index) {
        if (buffer[token.start + index] != comparison[index]) {
            return false;
        }
    }

    return true;
}

b8 compare_token_to_token(u8 const* buffer, Token token, Token other_token)
{
    // Must be the same length
    if (token.length != other_token.length) {
        return false;
    }

    for (usize index = 0; index < token.length; ++index) {
        if (buffer[token.start + index] != buffer[other_token.start + index]) {
            return false;
        }
    }

    return true;
}

usize count_lines(u8 const* buffer)
{
    u8 character;
    usize index = 0;
    usize line_count = 0;
    while ((character = buffer[index]) != '\0') {
        if (character == '\n') {
            ++line_count;
        }
        ++index;
    }
    // Case: Missing new line on the last line
    if (buffer[index - 1] != '\n') {
        ++line_count;
    }
    return line_count;
}

b8 darray_contains_token_value(u8 const* buffer, DArray const* container, Token search)
{
    for (usize index = 0; index < container->size; ++index) {
        Token* current_token = darray_get(container, index);
        if (compare_token_to_token(buffer, *current_token, search)) {
            return true;
        }
    }
    return false;
}

u32 count_matches(u8 const* buffer, DArray const* winning_tokens, DArray const* ticket_tokens)
{
    u32 num_matches = 0;
    for (usize index = 0; index < ticket_tokens->size; ++index) {
        // Check match
        Token* current_token = darray_get(ticket_tokens, index);
        num_matches += darray_contains_token_value(buffer, winning_tokens, *current_token) ? 1 : 0;
    }
    return num_matches;
}

void print_tokens(u8 const* buffer, DArray* tokens)
{
    DArrayIterator iterator = { .array = tokens, .index = 0 };
    Token* current_token;
    usize token_index;
    while ((current_token = (Token*)darray_iterator_enumerate_next(&iterator, &token_index)) != NULL) {
        u8 first_character = buffer[current_token->start];
        if (first_character == '\n') {
            printf("Token %llu: \033[91mNewLine\033[0m\n", token_index);
        } else {
            printf("Token %llu: %.*s\n", token_index, (int)current_token->length, &buffer[current_token->start]);
        }
    }
}

u32 calculate_total_score(Parser const* parser)
{
    DArray* winning_numbers = darray_create(10, sizeof(Token));
    DArray* ticket_numbers = darray_create(10, sizeof(Token));

    b8 in_winning_section = true;
    usize index = 0;
    u32 points = 0;
    while (index < parser->tokens->size) {
        Token* current_token = darray_get(parser->tokens, index);
        if (compare_token_to_str(parser->buffer, *current_token, "Card")) {
            // Consume to get card number
            ++index;
            in_winning_section = true;
        } else if (compare_token_to_str(parser->buffer, *current_token, "|")) {
            in_winning_section = false;
        } else if (compare_token_to_str(parser->buffer, *current_token, "\n")) {
            u32 num_matches = count_matches(parser->buffer, winning_numbers, ticket_numbers);
            if (num_matches > 0) {
                u32 earned_points = 1 << (num_matches - 1);
                points += earned_points;
            }
            darray_clear(winning_numbers);
            darray_clear(ticket_numbers);
        } else {
            // Number
            DArray* selected_array = in_winning_section ? winning_numbers : ticket_numbers;
            TRY_OR_ELSE(darray_push(selected_array, current_token), 0);
        }
        ++index;
    }

    darray_destroy(winning_numbers);
    darray_destroy(ticket_numbers);
    return points;
}

u32 count_total_cards(Parser const* parser)
{
    usize num_games = count_lines(parser->buffer);
    static u32 initial_count = 1;
    DArray* card_counts = darray_create_and_init(num_games, sizeof(u32), &initial_count);
    DArray* winning_numbers = darray_create(10, sizeof(Token));
    DArray* ticket_numbers = darray_create(10, sizeof(Token));

    b8 in_winning_section = true;
    usize card_number = 0;
    usize index = 0;
    while (index < parser->tokens->size) {
        Token* current_token = darray_get(parser->tokens, index);
        if (compare_token_to_str(parser->buffer, *current_token, "Card")) {
            // Consume to get card number
            ++index;
            ++card_number;
            in_winning_section = true;
        } else if (compare_token_to_str(parser->buffer, *current_token, "|")) {
            in_winning_section = false;
        } else if (compare_token_to_str(parser->buffer, *current_token, "\n")) {
            u32 num_matches = count_matches(parser->buffer, winning_numbers, ticket_numbers);
            u32* num_copies = darray_get(card_counts, card_number - 1);
            for (usize offset = 0; offset < num_matches; ++offset) {
                u32* old_num_copies = darray_get(card_counts, card_number + offset);
                u32 new_num_copies = *old_num_copies + *num_copies;
                darray_set(card_counts, card_number + offset, &new_num_copies);
            }
            darray_clear(winning_numbers);
            darray_clear(ticket_numbers);
        } else {
            // Number
            DArray* selected_array = in_winning_section ? winning_numbers : ticket_numbers;
            TRY_OR_ELSE(darray_push(selected_array, current_token), 0);
        }
        ++index;
    }
    u32 total_cards = 0;
    u32* current_count;
    DArrayIterator iterator = { .array = card_counts, .index = 0 };
    while ((current_count = (u32*)darray_iterator_next(&iterator)) != NULL) {
        total_cards += *current_count;
    }

    darray_destroy(card_counts);
    darray_destroy(winning_numbers);
    darray_destroy(ticket_numbers);
    return total_cards;
}

Parser* tokenise_input(u8 const* buffer, char const* delimiters, char const* special_delimiters)
{
    DArray* tokens = darray_create(10, sizeof(Token));

    usize index = 0;
    u8 character;
    Token current_token;
    b8 parse_began = false;
    while ((character = buffer[index]) != '\0') {
        b8 is_delimiter = strchr((char const*)delimiters, character) != NULL;
        b8 is_special_delimiter = strchr((char const*)special_delimiters, character) != NULL;
        if (is_special_delimiter) {
            current_token.start = index;
            current_token.length = 1;
            TRY_OR_ELSE(darray_push(tokens, &current_token), NULL);
            parse_began = false;
        } else if (!is_delimiter) {
            if (!parse_began) {
                current_token.start = index;
                parse_began = true;
            }
        } else {
            if (parse_began) {
                current_token.length = (index - current_token.start);
                TRY_OR_ELSE(darray_push(tokens, &current_token), NULL);
            }
            parse_began = false;
        }
        ++index;
    }

    Parser* parser = malloc(sizeof(Parser));
    parser->tokens = tokens;
    parser->buffer = buffer;

    return parser;
}

int main(int argc, char** argv)
{
    // Get the input path (required)
    if (argc < 2) {
        printf("Usage: %s <input_path>\n", argv[0]);
        printf("Please provide a path to the input file as a command line argument.\n");
        return -1;
    }
    char const* input_path = argv[1];

    // Read in file contents
    FILE* handle;
    i32 open_error = fopen_s(&handle, input_path, "rb");
    if (open_error) {
        printf("Could not open file %s!\n", input_path);
        return -2;
    }
    fseek(handle, 0L, SEEK_END);
    usize file_size = ftell(handle);
    fseek(handle, 0L, SEEK_SET);
    u8* buffer = (u8*)malloc(file_size + 1);
    buffer[file_size] = '\0';
    fread(buffer, 1, file_size, handle);
    {
        Parser* parser = tokenise_input(buffer, delimiters, special_delimiters);
        u32 total_score = calculate_total_score(parser);
        printf("Part 1 - Result %u\n", total_score);
        u32 total_cards = count_total_cards(parser);
        printf("Part 2 - Result %u\n", total_cards);
        darray_destroy(parser->tokens);
        free(parser);
    }
    free(buffer);

    return 0;
}
