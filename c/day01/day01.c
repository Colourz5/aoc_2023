#include "defines.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char const* valid_digit_strings[9] = {
    "one",
    "two",
    "three",
    "four",
    "five",
    "six",
    "seven",
    "eight",
    "nine",
};

/**
 * @brief Stores calibration values while parsing.
 */
typedef struct {
    /** @brief The first digit in the line.*/
    u8 first;
    /** @brief The second digit in the line.*/
    u8 second;
    /** @brief A flag to determine whether the first digit has been set or not.*/
    b8 parsed_first;
} CalibrationValues;

/**
 * @brief Updates calibration values given a digit.
 *
 * @param values The values to update.
 * @param digit The digit to add.
 */
inline void update_calibration_values(CalibrationValues* values, u8 digit)
{
    if (!values->parsed_first) {
        values->first = digit;
        values->parsed_first = true;
    }
    values->second = digit;
}

/**
 * @brief Calculates the calibration sum value parsing only digits.
 *
 * @param buffer The calibration document.
 */
int calculate_calibration_value_sum(u8 const* buffer)
{
    u8 character;
    usize index = 0;
    CalibrationValues values = { 0 };
    int result = 0;
    while ((character = buffer[index]) != '\0') {
        ++index;
        // New line
        if (character == '\n') {
            int number = 10 * values.first + values.second;
            result += number;
            values = (CalibrationValues const) { 0 };
        }
        // Digit
        else if (character > '0' && character <= '9') {
            int digit = (int)(character - '0');
            update_calibration_values(&values, digit);
        }
    }
    return result;
}

/**
 * @brief Calculates the calibration sum value parsing both digits and words.
 *
 * @param buffer The calibration document.
 */
int calculate_calibration_value_sum_with_words(u8 const* buffer)
{
    usize buffer_length = strlen((char const*)buffer);
    int result = 0;
    CalibrationValues values = { 0 };
    usize index = 0;
    u8 character;
    while ((character = buffer[index]) != '\0') {
        // New line
        if (character == '\n') {
            int number = 10 * values.first + values.second;
            result += number;
            values = (CalibrationValues const) { 0 };
        }
        // Digit
        else if (character > '0' && character <= '9') {
            int digit = (int)(character - '0');
            update_calibration_values(&values, digit);
        }
        // Character
        else {
            // Iterate over all valid digit strings and test if it matches
            for (usize digit_index = 0; digit_index < 9; ++digit_index) {
                usize string_length = strlen(valid_digit_strings[digit_index]);
                if (index + string_length >= buffer_length) {
                    continue;
                }
                bool comparison_success = !memcmp(&buffer[index], valid_digit_strings[digit_index], string_length);
                if (comparison_success) {
                    int digit = digit_index + 1;
                    update_calibration_values(&values, digit);
                }
            }
        }
        ++index;
    }
    return result;
}

int main(int argc, char** argv)
{
    // Get the input path (required)
    if (argc < 2) {
        printf("Usage: %s <input_path>\n", argv[0]);
        printf("Please provide a path to the input file as a command line argument.\n");
        return -1;
    }
    char const* input_path = argv[1];

    // Read in file contents
    FILE* handle;
    i32 open_error = fopen_s(&handle, input_path, "rb");
    if (open_error) {
        printf("Could not open file %s!\n", input_path);
        return -2;
    }
    fseek(handle, 0L, SEEK_END);
    usize file_size = ftell(handle);
    fseek(handle, 0L, SEEK_SET);
    u8* buffer = (u8*)malloc(file_size + 1);
    buffer[file_size] = '\0';
    fread(buffer, 1, file_size, handle);
    {
        int result_without_words = calculate_calibration_value_sum(buffer);
        printf("Part 1 - Result = %d\n", result_without_words);
        int result_with_words = calculate_calibration_value_sum_with_words(buffer);
        printf("Part 2 - Result = %d\n", result_with_words);
    }
    free(buffer);

    return 0;
}
