cmake_minimum_required(VERSION 3.27)

project(day01)

# Source files
add_executable(day01
    "day01.c"
)

# Include files
target_include_directories(day01 PRIVATE
    ${CMAKE_SOURCE_DIR}/include
)

# Link against helpers library
target_link_libraries(day01 PRIVATE aoc_helpers)

# Target compile options
target_compile_options(day01 PUBLIC -W -Wall -Wextra -pedantic -pedantic-errors)
