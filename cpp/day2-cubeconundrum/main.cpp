#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <string_view>
#include <vector>
#include <charconv>
#include <chrono>
#include "../common.hpp"

std::vector<std::string_view> tokenise_input(std::string_view text)
{
    int start_index = 0;
    std::vector<std::string_view> tokens;
    for (int i = 0; i < text.length(); ++i)
    {
        char character = text[i];
        if (character == '\n' || character == ' ' || character == ':' || character == ',' || character == ';')
        {
            int count = i - start_index;
            if (count >= 1)
            {
                tokens.push_back(text.substr(start_index, i - start_index));
                if (character == ';' || character == ',' || character == '\n')
                {
                    tokens.push_back(text.substr(i, 1));
                }
            }
            start_index = i + 1;
        }
    }
    return tokens;
}

int parse_tokens_part1(std::vector<std::string_view> tokens)
{
    int game = 0;
    int num_cube = 0;
    int cube_type = 0;
    int max_red = 0;
    int max_blue = 0;
    int max_green = 0;
    int sum = 0;
    for (int i = 0; i < tokens.size(); i++)
    {
        std::string_view current_token = tokens[i];
        // Parse Game x: where x is the game number
        if (current_token == "Game")
        {
            max_red = 0;
            max_blue = 0;
            max_green = 0;

            auto next_token = tokens[i + 1];
            auto result = std::from_chars(next_token.data(), next_token.data() + next_token.size(), game);
            if (result.ec == std::errc::invalid_argument)
            {
                return 0;
            }
            i++;
            continue;
        }
        auto result = std::from_chars(current_token.data(), current_token.data() + current_token.size(), num_cube);
        if (result.ec == std::errc::invalid_argument)
        {
            return 0;
        }
        auto next_token = tokens[i + 1];
        auto third_token = tokens[i + 2];
        if (next_token == "red")
        {
            max_red = std::max(num_cube, max_red);
        }
        else if (next_token == "blue")
        {
            max_blue = std::max(num_cube, max_blue);
        }
        else
        {
            max_green = std::max(num_cube, max_green);
        }
        if (third_token == "\n")
        {
            if (max_red <= 12 && max_blue <= 14 && max_green <= 13)
            {
                sum += game;
            }
        }
        i += 2;
    }
    return sum;
}

int parse_tokens_part2(std::vector<std::string_view> tokens)
{
    int game = 0;
    int num_cube = 0;
    int cube_type = 0;
    int max_red = 0;
    int max_blue = 0;
    int max_green = 0;
    int total = 0;
    for (int i = 0; i < tokens.size(); i++)
    {
        std::string_view current_token = tokens[i];
        // std::cout << "Token " << i << ": " << current_token << std::endl;
        // Parse Game x: where x is the game number
        if (current_token == "Game")
        {
            max_red = 0;
            max_blue = 0;
            max_green = 0;

            auto next_token = tokens[i + 1];
            auto result = std::from_chars(next_token.data(), next_token.data() + next_token.size(), game);
            if (result.ec == std::errc::invalid_argument)
            {
                std::cout << "No game number token?" << std::endl;
                return 0;
            }
            i++;
            continue;
        }
        auto result = std::from_chars(current_token.data(), current_token.data() + current_token.size(), num_cube);
        if (result.ec == std::errc::invalid_argument)
        {
            std::cout << "Can't parse cube number? Got " << current_token << std::endl;
            return 0;
        }
        auto next_token = tokens[i + 1];
        auto third_token = tokens[i + 2];
        if (next_token == "red")
        {
            max_red = std::max(num_cube, max_red);
        }
        else if (next_token == "blue")
        {
            max_blue = std::max(num_cube, max_blue);
        }
        else
        {
            max_green = std::max(num_cube, max_green);
        }
        if (third_token == "\n")
        {
            total += max_red * max_blue * max_green;
        }
        i += 2;
    }
    return total;
}

int main(int argc, char **argv)
{
    if (argc < 2)
    {
        std::cout << "Please input the path to the puzzle text!" << std::endl;
        return -1;
    }
    int num_trials;
    if (argc < 3)
    {
        num_trials = 100;
    }
    else
    {
        num_trials = atoi(argv[2]);
    }

    const char *file_path = argv[1];
    std::ifstream input_file(file_path);
    std::stringstream buffer;
    if (input_file)
    {
        buffer << input_file.rdbuf();
        input_file.close();
    }
    else
    {
        std::cout << "Failed to open file!";
        return -1;
    }
    std::string text_string = buffer.str();
    std::string_view view{text_string};
    auto tokens = tokenise_input(view);
    int result;
    std::chrono::duration<double> total_time;
    std::chrono::duration<double> average_time;

    // Part 1
    for (int i = 0; i < num_trials; ++i)
    {
        auto start_time = std::chrono::high_resolution_clock::now();
        result = parse_tokens_part1(tokens);
        auto end_time = std::chrono::high_resolution_clock::now();
        total_time += end_time - start_time;
    }
    average_time = total_time / num_trials;
    std::cout << "Part 1 - Result = " << result << std::endl;
    std::cout << "Part 1 took " << format_execution_time(average_time) << std::endl;

    // Part 2
    for (int i = 0; i < num_trials; ++i)
    {
        auto start_time = std::chrono::high_resolution_clock::now();
        result = parse_tokens_part2(tokens);
        auto end_time = std::chrono::high_resolution_clock::now();
        total_time += end_time - start_time;
    }
    average_time = total_time / num_trials;
    std::cout << "Part 2 - Result = " << result << std::endl;
    std::cout << "Part 2 took " << format_execution_time(average_time) << std::endl;
    return 0;
}