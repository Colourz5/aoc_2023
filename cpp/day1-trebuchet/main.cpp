#include <iostream>
#include <fstream>
#include <sstream>
#include <cstring>
#include <ctime>
#include <chrono>
#include "../common.hpp"

const char *valid_digit_strings[9] = {
    "one",
    "two",
    "three",
    "four",
    "five",
    "six",
    "seven",
    "eight",
    "nine",
};

int sum_calibration_values(const char *puzzle)
{
    int result = 0;
    char number_chars[3] = "\0\0";
    for (int i = 0; i < strlen(puzzle); ++i)
    {
        char current_char = puzzle[i];
        if (isdigit(current_char))
        {
            if (number_chars[0] == '\0')
            {
                number_chars[0] = current_char;
                number_chars[1] = current_char;
            }
            else
            {
                number_chars[1] = current_char;
            }
        }
        else if (current_char == '\n' || current_char == '\0')
        {
            result += atoi(number_chars);
            number_chars[0] = '\0';
            number_chars[1] = '\0';
        }
    }
    return result;
}

int sum_calibration_values_with_letters(const char *puzzle)
{
    int result = 0;
    int two_digits[2] = {0, 0};
    int current_index = 0;
    for (int i = 0; i < strlen(puzzle); ++i)
    {
        char current_char = puzzle[i];
        // Current char is a digit so the buffer must be cleared
        if (isdigit(current_char))
        {
            current_index = 0;
            if (two_digits[0] == 0)
            {
                two_digits[0] = current_char;
            }
            two_digits[1] = current_char;
        }
        else if (current_char == '\n' || current_char == '\0')
        {
            result += 10 * two_digits[1] + two_digits[0];
            memset(two_digits, 0, sizeof(two_digits));
        }
        else
        {
            // Go through every valid letter digit
            for (int j = 0; j < 9; ++j)
            {
                int number_length_in_chars = strlen(valid_digit_strings[j]);
                if (i + number_length_in_chars > strlen(puzzle))
                {
                    continue;
                }

                bool is_equal = !std::memcmp(&puzzle[i], valid_digit_strings[j], number_length_in_chars);
                if (is_equal)
                {
                    int value = 1 + j;
                    if (two_digits[0] == 0)
                    {
                        two_digits[0] = value;
                    }
                    two_digits[1] = value;
                }
            }
        }
    }
    return result;
}

int main(int argc, char **argv)
{
    if (argc < 2)
    {
        std::cout << "Please input the file path to the puzzle text!" << std::endl;
        return -1;
    }
    const char *file_path = argv[1];
    int num_trials;

    if (argc < 3)
    {
        num_trials = 1000;
    }
    else
    {
        num_trials = atoi(argv[2]);
    }
    std::cout << "Running " << num_trials << " trials..." << std::endl;

    std::ifstream input_file(file_path);
    std::stringstream buffer;
    if (input_file)
    {
        buffer << input_file.rdbuf();
        input_file.close();
    }
    else
    {
        std::cout << "Failed to open file!";
        return -1;
    }
    std::string text_string = buffer.str();

    int result;
    std::chrono::duration<double> total_time;
    std::chrono::duration<double> average_time;

    for (int i = 0; i < num_trials; ++i)
    {
        auto t1 = std::chrono::high_resolution_clock::now();
        result = sum_calibration_values(text_string.c_str());
        auto t2 = std::chrono::high_resolution_clock::now();
        total_time += t2 - t1;
    }
    average_time = total_time / num_trials;
    std::cout << "Part 1 - Result = " << result << std::endl;
    std::cout << "Part 1 took " << format_execution_time(average_time) << std::endl;

    total_time = {};
    for (int i = 0; i < num_trials; ++i)
    {
        auto t1 = std::chrono::high_resolution_clock::now();
        result = sum_calibration_values_with_letters(text_string.c_str());
        auto t2 = std::chrono::high_resolution_clock::now();
        total_time += t2 - t1;
    }
    average_time = total_time / num_trials;
    std::cout << "Part 2 - Result = " << result << std::endl;
    std::cout << "Part 2 took " << format_execution_time(average_time) << std::endl;
    return 0;
}
