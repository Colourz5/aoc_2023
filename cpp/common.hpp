#pragma once
#include <string>
#include <chrono>
#include <sstream>
#include <iomanip>

std::string format_execution_time(std::chrono::duration<double> time_taken)
{
    auto seconds = std::chrono::duration_cast<std::chrono::seconds>(time_taken);
    auto milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(time_taken - seconds);
    auto microseconds = std::chrono::duration_cast<std::chrono::microseconds>(time_taken - seconds - milliseconds);
    auto nanoseconds = std::chrono::duration_cast<std::chrono::nanoseconds>(time_taken - seconds - milliseconds - microseconds);

    std::ostringstream result;
    result << std::fixed << std::setprecision(0) << seconds.count() << " seconds, ";
    result << std::fixed << std::setprecision(0) << milliseconds.count() << " milliseconds, ";
    result << std::fixed << std::setprecision(0) << microseconds.count() << " microseconds, ";
    result << std::fixed << std::setprecision(0) << nanoseconds.count() << " nanoseconds";
    return result.str();
}