const std = @import("std");

const BenchmarkTime = struct {
    seconds: u64,
    milliseconds: u64,
    microseconds: u64,
    nanoseconds: u64,
};

// Create global array of strings

pub fn main() !void {
    var arena_allocator = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    const arena = arena_allocator.allocator();
    const args = try std.process.argsAlloc(arena);
    if (args.len < 2) {
        std.debug.print("Usage: {s} <arg>\n", .{args[0]});
        return;
    }
    const input_path = args[1];
    const num_trials: u32 = if (args.len < 3)
        1000
    else
        try std.fmt.parseInt(u32, args[2], 10);

    // Open file
    var file = try std.fs.cwd().openFile(input_path, .{ .mode = .read_only });
    defer file.close();

    // Get the file size
    const file_size = (try file.stat()).size;
    // Allocate a buffer for the file
    const file_buffer = try arena.alloc(u8, file_size);
    // Read the file into the buffer
    const num_read_bytes = try file.readAll(file_buffer);
    defer arena.free(file_buffer);
    // Check that we read the entire file
    if (num_read_bytes != file_size) {
        std.debug.print("Failed to read entire file\n", .{});
        return;
    }
    std.debug.print("Running {d} trials...\n", .{num_trials});

    // Part 1
    var result: u32 = 0;
    var total_time: u64 = 0;
    var timer = try std.time.Timer.start();
    for (0..num_trials) |_| {
        // Time the function
        timer.reset();
        result = calculateCalibrationValueSum(file_buffer);
        total_time += timer.lap();
    }
    var average_time = @divFloor(total_time, num_trials);
    var time_taken = format_execution_time(average_time);
    std.debug.print("Part 1 - Result = {d}\n", .{result});
    std.debug.print("Part 1 took {d} seconds, {d} milliseconds, {d} microseconds, {d} nanoseconds\n", .{ time_taken.seconds, time_taken.milliseconds, time_taken.microseconds, time_taken.nanoseconds });

    // Part 2
    const valid_digit_strings: [9][]const u8 = [_][]const u8{ "one", "two", "three", "four", "five", "six", "seven", "eight", "nine" };
    result = 0;
    total_time = 0;
    for (0..num_trials) |_| {
        // Time the function
        timer.reset();
        result = calculateCalibrationValueSumWithLetters(file_buffer, valid_digit_strings);
        total_time += timer.lap();
    }
    average_time = @divFloor(total_time, num_trials);
    time_taken = format_execution_time(average_time);
    std.debug.print("Part 2 - Result = {d}\n", .{result});
    std.debug.print("Part 2 took {d} seconds, {d} milliseconds, {d} microseconds, {d} nanoseconds\n", .{ time_taken.seconds, time_taken.milliseconds, time_taken.microseconds, time_taken.nanoseconds });
}

fn calculateCalibrationValueSum(buffer: []const u8) u32 {
    var line_it = std.mem.splitScalar(u8, buffer, '\n');
    // Iterate over each line
    var result: u32 = 0;
    while (line_it.next()) |line| {
        // Iterate over each character in the line
        // Optional first digit
        var two_digits: [2]u32 = .{ 0, 0 };
        for (line) |char| {
            if (char == '\n' or char == '\r') {
                continue;
            }
            // Is the character a digit?
            if (char >= '0' and char <= '9') {
                // Convert the character to a digit
                const digit = char - '0';
                if (two_digits[0] == 0) {
                    two_digits[0] = digit;
                }
                two_digits[1] = digit;
            }
        }
        // Add the two digits to the sum
        result += 10 * two_digits[0] + two_digits[1];
    }

    return result;
}

fn calculateCalibrationValueSumWithLetters(buffer: []const u8, valid_digit_strings: [9][]const u8) u32 {
    var line_it = std.mem.splitScalar(u8, buffer, '\n');
    // Iterate over each line
    var result: u32 = 0;
    while (line_it.next()) |line| {
        // Iterate over each character in the line
        // Optional first digit
        var two_digits: [2]u32 = .{ 0, 0 };
        var char_idx: usize = 0;
        for (line) |char| {
            defer char_idx += 1;
            if (char == '\n' or char == '\r') {
                continue;
            }
            // Is the character a digit?
            if (char >= '0' and char <= '9') {
                // Convert the character to a digit
                const digit = char - '0';
                if (two_digits[0] == 0) {
                    two_digits[0] = digit;
                }
                two_digits[1] = digit;
            }
            // Is it a letter?
            else if (char >= 'a' and char <= 'z') {
                // Iterate over each valid digit string
                for (valid_digit_strings, 0..) |digit_string, digit_idx| {
                    const digit_string_len = digit_string.len;
                    const end_idx = char_idx + digit_string_len;
                    if (end_idx > line.len) {
                        continue;
                    }
                    // Slice the line
                    const line_slice = line[char_idx..end_idx];
                    // Is the digit string a match?
                    if (std.mem.eql(u8, line_slice, digit_string)) {
                        // Add the digit to the sum
                        const value: u32 = @intCast(digit_idx + 1);
                        if (two_digits[0] == 0) {
                            two_digits[0] = value;
                        }
                        two_digits[1] = value;
                        break;
                    }
                }
            }
        }
        // Add the two digits to the sum
        result += 10 * two_digits[0] + two_digits[1];
    }

    return result;
}

fn format_execution_time(time_taken_ns: u64) BenchmarkTime {
    const time_taken_s = @divFloor(time_taken_ns, 1_000_000_000);
    const time_taken_ms = @divFloor(time_taken_ns - 1_000_000_000 * time_taken_s, 1_000_000);
    const time_taken_us = @divFloor(time_taken_ns - 1_000_000_000 * time_taken_s - 1_000_000 * time_taken_ms, 1_000);
    const time_taken_ns_remainder = time_taken_ns - 1_000_000_000 * time_taken_s - 1_000_000 * time_taken_ms - 1_000 * time_taken_us;
    return BenchmarkTime{ .seconds = time_taken_s, .milliseconds = time_taken_ms, .microseconds = time_taken_us, .nanoseconds = time_taken_ns_remainder };
}
