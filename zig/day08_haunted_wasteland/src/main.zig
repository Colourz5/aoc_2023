const std = @import("std");
const Allocator = std.mem.Allocator;
const ArrayList = std.ArrayList;

const BenchmarkTime = struct {
    seconds: u64,
    milliseconds: u64,
    microseconds: u64,
    nanoseconds: u64,
};

const MapDirection = struct {
    location: []const u8,
    left: []const u8,
    right: []const u8,
};

pub fn main() !void {
    var arena_allocator = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    const arena = arena_allocator.allocator();
    const args = try std.process.argsAlloc(arena);
    if (args.len < 2) {
        std.debug.print("Usage: {s} <input_path>\n", .{args[0]});
    }
    const input_path = args[1];
    const num_trials: u32 = if (args.len < 3)
        1000
    else
        try std.fmt.parseInt(u32, args[2], 10);

    // Open file
    var file = try std.fs.cwd().openFile(input_path, .{ .mode = .read_only });
    defer file.close();

    // Get the file size
    const file_size = (try file.stat()).size;
    // Allocate a buffer to store contents
    const file_buffer = try arena.alloc(u8, file_size);
    // Read the file into the buffer
    const num_read_bytes = try file.readAll(file_buffer);
    defer arena.free(file_buffer);

    // Check that we read the entire file
    if (num_read_bytes != file_size) {
        std.debug.print("Failed to read the entire file\n", .{});
        return;
    }
    std.debug.print("Running {d} trials...\n", .{num_trials});

    // Allocator for problem
    const problem_arena = arena_allocator.allocator();

    // Part 1
    var result: u32 = 0;
    var total_time: u64 = 0;
    var timer = try std.time.Timer.start();
    for (0..num_trials) |_| {
        timer.reset();
        result = try calculateNumberOfSteps(problem_arena, file_buffer);
        total_time += timer.lap();
    }
    const average_time = @divFloor(total_time, num_trials);
    const time_taken = format_execution_time(average_time);
    std.debug.print("Part 1 - Result = {d}\n", .{result});
    std.debug.print("Part 1 took {d} seconds, {d} milliseconds, {d} microseconds, {d} nanoseconds\n", .{ time_taken.seconds, time_taken.milliseconds, time_taken.microseconds, time_taken.nanoseconds });
}

fn calculateNumberOfSteps(allocator: Allocator, buffer: []const u8) !u32 {
    var line_it = std.mem.splitAny(u8, buffer, "\r\n");
    // Iterate over each line
    var result: u32 = 0;
    var directions: ?[]const u8 = null;
    var map = ArrayList(MapDirection).init(allocator);
    defer map.deinit();
    while (line_it.next()) |line| {
        if (line.len == 0) {
            continue;
        }
        if (directions == null) {
            directions = line;
            continue;
        }
        var location: ?[]const u8 = null;
        var left: ?[]const u8 = null;
        var right: ?[]const u8 = null;
        var token_it = std.mem.splitAny(u8, line, " =(,)");
        while (token_it.next()) |token| {
            if (token.len == 0) {
                continue;
            }
            if (location == null) {
                location = token;
            } else if (left == null) {
                left = token;
            } else {
                right = token;
            }
        }
        const map_direction = MapDirection{ .location = location.?, .left = left.?, .right = right.? };
        try map.append(map_direction);
    }
    std.debug.print("Directions = {s}\n", .{directions.?});
    var current_location: []const u8 = "AAA";
    outer: while (true) {
        for (directions.?) |direction| {
            std.debug.print("Going {c}\n", .{direction});
            for (map.items) |route| {
                if (std.mem.eql(u8, route.location, current_location)) {
                    if (direction == 'L') {
                        current_location = route.left;
                    } else {
                        current_location = route.right;
                    }
                    result += 1;
                    break;
                }
            }
            if (std.mem.eql(u8, current_location, "ZZZ")) {
                break :outer;
            }
        }
    }
    std.debug.print("Location = {s}\n", .{current_location});
    return result;
}

fn format_execution_time(time_taken_ns: u64) BenchmarkTime {
    const time_taken_s = @divFloor(time_taken_ns, 1_000_000_000);
    const time_taken_ms = @divFloor(time_taken_ns - 1_000_000_000 * time_taken_s, 1_000_000);
    const time_taken_us = @divFloor(time_taken_ns - 1_000_000_000 * time_taken_s - 1_000_000 * time_taken_ms, 1_000);
    const time_taken_ns_remainder = time_taken_ns - 1_000_000_000 * time_taken_s - 1_000_000 * time_taken_ms - 1_000 * time_taken_us;
    return BenchmarkTime{ .seconds = time_taken_s, .milliseconds = time_taken_ms, .microseconds = time_taken_us, .nanoseconds = time_taken_ns_remainder };
}
