"""This module contains the solution to Day 16 of Advent of Code 2023"""

# Standard libraries
import argparse
from dataclasses import dataclass
from enum import Enum
import time
from typing import Sequence

# External libraries
from rich import print as rprint

# Internal libraries
from python_aoc.common import format_execution_time


class BeamDirection(Enum):
    LEFT = 0
    UP = 1
    RIGHT = 2
    DOWN = 3


@dataclass
class Beam:
    row_idx: int
    column_idx: int
    direction: BeamDirection

    def next_cell(self) -> tuple[int, int]:
        target_row: int = self.row_idx
        target_column: int = self.column_idx
        match self.direction:
            case BeamDirection.LEFT:
                target_column -= 1
            case BeamDirection.UP:
                target_row -= 1
            case BeamDirection.RIGHT:
                target_column += 1
            case BeamDirection.DOWN:
                target_row += 1
        return (target_row, target_column)


def main(args: Sequence[str]) -> None:
    parsed_args = _parse_args(args)
    input_path: str = parsed_args.input_path
    num_trials: int = parsed_args.num_trials
    verbose_mode: bool = parsed_args.verbose

    lines: Sequence[str] = []
    with open(input_path, "r", encoding="utf-8") as input_file:
        lines = input_file.readlines()

    # Part 1
    result: int | None = None
    total_time: int = 0
    for _ in range(num_trials):
        start_time: int = time.perf_counter_ns()
        result = part1(lines, verbose_mode)
        total_time += time.perf_counter_ns() - start_time
    average_time: float = total_time / num_trials
    rprint(f"Part 1 - Result = {result}")
    rprint(f"Part 1 took {format_execution_time(average_time)}")

    # Part 2
    result: int | None = None
    total_time: int = 0
    for _ in range(num_trials):
        start_time: int = time.perf_counter_ns()
        result = part2(lines, verbose_mode)
        total_time += time.perf_counter_ns() - start_time
    average_time: float = total_time / num_trials
    rprint(f"Part 2 - Result = {result}")
    rprint(f"Part 2 took {format_execution_time(average_time)}")


def count_energised_tiles(
    grid: Sequence[str], initial_beams: list[Beam], verbose_mode: bool
) -> int:
    """Part 1 of Day XX of Advent of Code XXXX

    Parameters
    ----------
    lines : Sequence[str]
        The lines of the input file.
    verbose : bool
        Turn on verbose mode.

    Returns
    -------
    int
        The answer
    """
    width: int = len(grid[0])
    height: int = len(grid)
    beams: list[Beam] = initial_beams
    energised_tiles: set[tuple[int, int]] = set()
    seen_movements: set[tuple[int, int, BeamDirection]] = set()
    step_idx = 0
    while len(beams) > 0:
        step_idx += 1
        # Iterate over each beam and simulate
        remove_queue = []
        push_queue = []
        for beam in beams:
            target_row, target_column = beam.next_cell()
            # Remove if going to go out of bounds
            if (
                target_row < 0
                or target_row >= height
                or target_column < 0
                or target_column >= width
            ):
                remove_queue.append(beam)
                continue
            target_cell = grid[target_row][target_column]
            beam.row_idx = target_row
            beam.column_idx = target_column
            match target_cell:
                case "|" if (
                    beam.direction == BeamDirection.LEFT
                    or beam.direction == BeamDirection.RIGHT
                ):
                    remove_queue.append(beam)
                    upper_beam = Beam(target_row, target_column, BeamDirection.UP)
                    lower_beam = Beam(target_row, target_column, BeamDirection.DOWN)
                    push_queue.append(upper_beam)
                    push_queue.append(lower_beam)
                case "-" if (
                    beam.direction == BeamDirection.UP
                    or beam.direction == BeamDirection.DOWN
                ):
                    remove_queue.append(beam)
                    left_beam = Beam(target_row, target_column, BeamDirection.LEFT)
                    right_beam = Beam(target_row, target_column, BeamDirection.RIGHT)
                    push_queue.append(left_beam)
                    push_queue.append(right_beam)
                case "/":
                    match beam.direction:
                        case BeamDirection.LEFT:
                            beam.direction = BeamDirection.DOWN
                        case BeamDirection.RIGHT:
                            beam.direction = BeamDirection.UP
                        case BeamDirection.UP:
                            beam.direction = BeamDirection.RIGHT
                        case BeamDirection.DOWN:
                            beam.direction = BeamDirection.LEFT
                case "\\":
                    match beam.direction:
                        case BeamDirection.LEFT:
                            beam.direction = BeamDirection.UP
                        case BeamDirection.RIGHT:
                            beam.direction = BeamDirection.DOWN
                        case BeamDirection.UP:
                            beam.direction = BeamDirection.LEFT
                        case BeamDirection.DOWN:
                            beam.direction = BeamDirection.RIGHT
                case "." | "-" | "|":
                    pass
                case _ as target:
                    rprint(f"Unhandled target {target}")
                    break
        # Remove old beams
        for remove_beam in remove_queue:
            beams.remove(remove_beam)
        # Add new beams
        for new_beam in push_queue:
            if (
                new_beam.row_idx,
                new_beam.column_idx,
                new_beam.direction,
            ) in seen_movements:
                continue
            beams.append(new_beam)
        for beam in beams:
            if not (
                beam.row_idx < 0
                or beam.row_idx >= height
                or beam.column_idx < 0
                or beam.column_idx >= width
            ):
                energised_tiles.add((beam.row_idx, beam.column_idx))
                seen_movements.add((beam.row_idx, beam.column_idx, beam.direction))
        if verbose_mode:
            rprint(f"Simulation step: {step_idx}")
            print_grid(grid, energised_tiles)
            rprint("Beams")
            rprint(beams)
            rprint(f"Number of energised beams {len(energised_tiles)}")
    if verbose_mode:
        print_grid(grid, energised_tiles)
    return len(energised_tiles)


def part1(lines: Sequence[str], verbose_mode: bool) -> int:
    """Calculates the number of energised tiles for a single initial beam starting in the top left corner and going to the right.
    Solves part 1 of Day 16 of Advent of Code 2023.

    Parameters
    ----------
    lines : Sequence[str]
        The lines of the input file.
    verbose_mode : bool
        Turn on verbose mode.

    Returns
    -------
    int
        The answer
    """
    grid = parse_grid(lines)
    # Set up the initial beam
    beams = [Beam(0, -1, BeamDirection.RIGHT)]
    result = count_energised_tiles(grid, beams, verbose_mode)
    return result


def part2(lines: Sequence[str], verbose_mode: bool) -> int:
    """Calculates the maximum number of energised tiles by going through all possible initial beam configurations.
    Solves part 2 of Day 16 of Advent of Code 2023.

    Parameters
    ----------
    lines : Sequence[str]
        The lines of the input file.
    verbose_mode : bool
        Turn on verbose mode.

    Returns
    -------
    int
        The answer
    """
    grid = parse_grid(lines)
    width: int = len(grid[0])
    height: int = len(grid)
    max_tiles: int = 0
    # Iterate over the rows
    for row_idx in range(height):
        if verbose_mode:
            rprint(f"Calculating row {row_idx}...")
        # Left side going to the right
        left_beams = [Beam(row_idx, -1, BeamDirection.RIGHT)]
        left_result = count_energised_tiles(grid, left_beams, False)
        # Right side going to the right
        right_beams = [Beam(row_idx, width, BeamDirection.LEFT)]
        right_result = count_energised_tiles(grid, right_beams, False)
        # Update the max tiles
        max_tiles = max(max_tiles, left_result, right_result)
    # Iterate over the columns
    for column_idx in range(width):
        if verbose_mode:
            rprint(f"Calculating column {column_idx}...")
        # Top side going down
        down_beams = [Beam(-1, column_idx, BeamDirection.DOWN)]
        down_result = count_energised_tiles(grid, down_beams, False)
        # Bottom side going up
        up_beams = [Beam(height, column_idx, BeamDirection.UP)]
        up_result = count_energised_tiles(grid, up_beams, False)
        # Update the max tiles
        max_tiles = max(max_tiles, down_result, up_result)
    return max_tiles


def parse_grid(lines: Sequence[str]) -> Sequence[str]:
    """Parses the grid from the lines given.

    Parameters
    ----------
    lines : Sequence[str]
        The grid given as a sequence of lines.

    Returns
    -------
    grid : Sequence[str]
        The grid given as a sequence of lines. This is same as `lines` but with the newline character removed.
    """
    grid: list[str] = []
    for line in lines:
        line = line.rstrip("\n")
        grid.append(line)
    assert len(grid) > 0
    return grid


def print_grid(grid: Sequence[str], energised_tiles: set[tuple[int, int]]) -> None:
    """Prints the grid in a rich format.

    Parameters
    ----------
    grid : Sequence[str]
        The grid to print.
    energised_tiles : set[tuple[int, int]]
        A set of tiles which have been energised.
        Each item contains the row index as the first element and the column index as the second.
    """
    width: int = len(grid[0])
    height_digits: int = len(str(len(grid)))
    rprint(height_digits * " " + "|", end="")
    for column_idx in range(width):
        rprint(f"[bold white]{str(column_idx)[-1]}[/bold white]", end="|")
    rprint()
    for row_idx, row in enumerate(grid):
        rprint(f"[bold white]{str(row_idx).rjust(height_digits)}[/bold white]", end="|")
        for column_idx, cell in enumerate(row):
            background = "cyan" if (row_idx, column_idx) in energised_tiles else "black"
            match cell:
                case ".":
                    cell = " "
                    style = f"white on {background}"
                case "/":
                    style = f"bold red on {background}"
                case "\\":
                    cell = "\\\\"
                    style = f"bold yellow on {background}"
                case "|":
                    style = f"bold magenta on {background}"
                case "-":
                    style = f"bold green on {background}"
                case _:
                    cell = "?"
                    style = "white"
            rprint(f"[{style}]{cell}[/{style}]", end="|")
        rprint()
    rprint()


def _parse_args(args: Sequence[str]) -> argparse.Namespace:
    """Parses the command line arguments

    Parameters
    ----------
    args : Sequence[str]
        The command line arguments.

    Returns
    -------
    parsed_args : argparse.Namespace
        The parsed command line arguments.
    """
    parser = argparse.ArgumentParser(
        prog="aoc 2023 day 16",
        description="Program to solve Day 16 of Advent of Code 2023",
    )
    parser.add_argument("input_path", type=str, help="Path to input file")
    parser.add_argument(
        "-num_trials",
        "--num_trials",
        type=int,
        default=100,
        help="Number of trials to use when measuring run time",
    )
    parser.add_argument(
        "-v",
        "--v",
        "-verbose",
        "--verbose",
        action="store_true",
        dest="verbose",
        help="Turn on verbose mode",
    )
    return parser.parse_args(args)


if __name__ == "__main__":
    import sys

    main(sys.argv[1:])
