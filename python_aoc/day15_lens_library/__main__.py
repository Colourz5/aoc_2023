"""This module contains the solution to Day 15 of Advent of Code 2023"""

# Standard libraries
import argparse
import time
from typing import Sequence

# External libraries
from rich import print as rprint

# Internal libraries
from python_aoc.common import format_execution_time


def main(args: Sequence[str]) -> None:
    parsed_args = _parse_args(args)
    input_path: str = parsed_args.input_path
    num_trials: int = parsed_args.num_trials
    verbose_mode: bool = parsed_args.verbose

    lines: Sequence[str] = []
    with open(input_path, "r", encoding="utf-8") as input_file:
        lines = input_file.readlines()

    # Part 1
    result: int | None = None
    total_time: int = 0
    for _ in range(num_trials):
        start_time: int = time.perf_counter_ns()
        result = calculate_hash_sum(lines, verbose_mode)
        total_time += time.perf_counter_ns() - start_time
    average_time: float = total_time / num_trials
    rprint(f"Part 1 - Result = {result}")
    rprint(f"Part 1 took {format_execution_time(average_time)}")

    # Part 2
    result: int | None = None
    total_time: int = 0
    for _ in range(num_trials):
        start_time: int = time.perf_counter_ns()
        result = calculate_total_focusing_power(lines, verbose_mode)
        total_time += time.perf_counter_ns() - start_time
    average_time: float = total_time / num_trials
    rprint(f"Part 2 - Result = {result}")
    rprint(f"Part 2 took {format_execution_time(average_time)}")


def calculate_hash_sum(lines: Sequence[str], verbose_mode: bool) -> int:
    """Calculates the total sum of hashes of each token from a comma separated list.
    Solves part 1 of Day 15 of Advent of Code 2023.

    Parameters
    ----------
    lines : Sequence[str]
        The lines of the input file.
    verbose : bool
        Turn on verbose mode.

    Returns
    -------
    int
        The answer
    """
    assert len(lines) == 1
    line = lines[0].rstrip("\n")
    tokens = line.split(",")
    result: int = 0
    for token in tokens:
        token_hash = hash_token(token)
        if verbose_mode:
            rprint(f"Token {token} -> {token_hash}")
        result += token_hash
    return result


def calculate_total_focusing_power(lines: Sequence[str], verbose_mode: bool) -> int:
    """Calculates the toal focusing power after completing a series of instructions.
    Solves part 2 of Day 15 of Advent of Code 2023.

    Parameters
    ----------
    lines : Sequence[str]
        The lines of the input file.
    verbose_mode : bool
        Turn on verbose mode.

    Returns
    -------
    int
        The answer
    """
    assert len(lines) == 1
    line = lines[0].rstrip("\n")
    tokens = line.split(",")
    boxes: dict[int, list[tuple[str, int]]] = {}
    for token_idx, token in enumerate(tokens):
        token_idx += 1
        if verbose_mode:
            rprint(f"Processing step {token_idx}")
        if "=" in token:
            sub_tokens = token.split("=")
            label = sub_tokens[0]
            lens = int(sub_tokens[1])
            label_hash = hash_token(label)
            if verbose_mode:
                rprint(
                    f"Add lens with focal length {lens} with {label} to box {label_hash}"
                )
            # There is already something in the box
            if label_hash in boxes:
                slots = boxes[label_hash]
                found = False
                for slot_idx, (slot_label, _) in enumerate(slots):
                    # Found our label
                    if slot_label == label:
                        found = True
                        slots[slot_idx] = (label, lens)
                if not found:
                    slots.append((label, lens))
            # Create new slots
            else:
                boxes[label_hash] = [(label, lens)]
        elif "-" in token:
            sub_tokens = token.split("-")
            label = sub_tokens[0]
            label_hash = hash_token(label)
            if verbose_mode:
                rprint(f"Remove lens with {label} from box {label_hash}")
            # Check if there are slots in the box already
            if label_hash in boxes:
                slots = boxes[label_hash]
                slots = [
                    (slot_label, slot_lens)
                    for (slot_label, slot_lens) in slots
                    if slot_label != label
                ]
                boxes[label_hash] = slots
        else:
            raise ValueError(f"Expected either - or = in the token! Token: {token}")
        if verbose_mode and token_idx % 500 == 0:
            print_boxes(boxes)
    result: int = calculate_focusing_power(boxes)
    return result


def hash_token(token: str) -> int:
    """Hash a token of ASCII characters into a byte long hash.

    Parameters
    ----------
    token : str
         A series of characters.

    Returns
    -------
    int
        The hash value.
    """
    value: int = 0
    for character in token:
        code = ord(character)
        value += code
        value *= 17
        value %= 256
    return value


def calculate_focusing_power(boxes: dict[int, list[tuple[str, int]]]) -> int:
    """Calculates the focusing power of the series of focusing boxes.

    Parameters
    ----------
    boxes : dict[int, list[tuple[str, int]]]
        The boxes of lens to calculate.

    Returns
    -------
    int
        The focusing power of the boxes.
    """
    power: int = 0
    for box_number, slots in boxes.items():
        box_power = box_number + 1
        for slot_idx, (_, lens) in enumerate(slots):
            power += box_power * (slot_idx + 1) * lens
    return power


def print_boxes(boxes: dict[int, list[tuple[str, int]]]) -> None:
    """Prints the contents of each box.

    Parameters
    ----------
    boxes : dict[int, list[tuple[str, int]]]
        The boxes to print.
    """
    for box_number, slots in boxes.items():
        if len(slots) == 0:
            continue
        rprint(f"Box {box_number} [", end="")
        for slot_idx, (label, lens) in enumerate(slots):
            if slot_idx < len(slots) - 1:
                end = ", "
            else:
                end = "]"
            rprint(f"{label}={lens}", end=end)
        rprint()
    rprint()


def _parse_args(args: Sequence[str]) -> argparse.Namespace:
    """Parses the command line arguments

    Parameters
    ----------
    args : Sequence[str]
        The command line arguments.

    Returns
    -------
    parsed_args : argparse.Namespace
        The parsed command line arguments.
    """
    parser = argparse.ArgumentParser(
        prog="aoc 2023 day 15",
        description="Program to solve Day 15 of Advent of Code 2023",
    )
    parser.add_argument("input_path", type=str, help="Path to input file")
    parser.add_argument(
        "-num_trials",
        "--num_trials",
        type=int,
        default=100,
        help="Number of trials to use when measuring run time",
    )
    parser.add_argument(
        "-v",
        "--v",
        "-verbose",
        "--verbose",
        action="store_true",
        dest="verbose",
        help="Turn on verbose mode",
    )
    return parser.parse_args(args)


if __name__ == "__main__":
    import sys

    main(sys.argv[1:])
