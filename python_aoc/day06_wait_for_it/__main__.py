"""This module contains Day 6 of AOC 2023"""

# Standard libraries
import argparse
import math
import time
from typing import Sequence

# External libraries
from rich import print as rprint

# Internal libraries
from python_aoc.common import format_execution_time


def main(args: Sequence[str]) -> None:
    parsed_args = _parse_args(args)
    input_path: str = parsed_args.input_path
    num_trials: int = parsed_args.num_trials

    lines: Sequence[str] = []
    with open(input_path, "r", encoding="utf-8") as input_file:
        lines = input_file.readlines()

    # Part 1
    result = 0
    total_time = 0
    for _ in range(num_trials):
        start_time = time.perf_counter_ns()
        result = calculate_num_ways_product(lines)
        total_time += time.perf_counter_ns() - start_time
    average_time = total_time / num_trials
    rprint(f"Part 1 - Result {result}")
    rprint(f"Part 1 took {format_execution_time(average_time)}")

    # Part 2
    result = 0
    total_time = 0
    for _ in range(num_trials):
        start_time = time.perf_counter_ns()
        result = calculate_num_ways_total(lines)
        total_time += time.perf_counter_ns() - start_time
    average_time = total_time / num_trials
    rprint(f"Part 2 - Result {result}")
    rprint(f"Part 2 took {format_execution_time(average_time)}")


def calculate_num_ways_product(lines: Sequence[str]) -> int:
    times_given = []
    records = []
    for line in lines:
        line = line.rstrip("\n")
        tokens = line.split()
        if len(tokens) == 0:
            continue
        numbers = [int(token) for token in tokens[1:]]
        if tokens[0] == "Time:":
            times_given.extend(numbers)
        elif tokens[0] == "Distance:":
            records.extend(numbers)
        else:
            rprint(f"Unexpected token {tokens[0]}")

    assert len(times_given) > 0
    assert len(times_given) == len(records)
    result = 1
    for total_time, record in zip(times_given, records):
        num_ways = calculate_num_ways_single(total_time, record)
        result *= num_ways
    return result


def calculate_num_ways_total(lines: Sequence[str]) -> int:
    total_time = 0
    record = 0
    for line in lines:
        line = line.rstrip("\n")
        tokens = line.split()
        if len(tokens) == 0:
            continue
        number = int("".join(tokens[1:]))
        if tokens[0] == "Time:":
            total_time = number
        elif tokens[0] == "Distance:":
            record = number
        else:
            rprint(f"Unexpected token {tokens[0]}")
    num_ways = calculate_num_ways_single(total_time, record)
    return num_ways


def calculate_num_ways_single(total_time: int, record: int) -> int:
    discriminant = math.sqrt(total_time**2 - 4 * record)
    upper_bound = math.ceil(0.5 * (total_time + discriminant))
    lower_bound = math.floor(0.5 * (total_time - discriminant))
    num_ways = upper_bound - lower_bound - 1
    return num_ways


def _parse_args(args: Sequence[str]) -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        prog="day06_wait_for_it", description="Advent of Code Day 6 2023"
    )
    parser.add_argument("input_path", type=str, help="Path to the input file")
    parser.add_argument(
        "-num_trials",
        "--num_trials",
        type=int,
        default=100,
        help="The number of trials to run each part for",
    )
    return parser.parse_args(args)


if __name__ == "__main__":
    import sys

    main(sys.argv[1:])
