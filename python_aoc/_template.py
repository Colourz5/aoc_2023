"""This module contains the solution to Day X of Advent of Code XXXX"""

# Standard libraries
import argparse
import time
from typing import Sequence

# External libraries
from rich import print as rprint

# Internal libraries
from python_aoc.common import format_execution_time


def main(args: Sequence[str]) -> None:
    parsed_args = _parse_args(args)
    input_path: str = parsed_args.input_path
    num_trials: int = parsed_args.num_trials
    verbose_mode: bool = parsed_args.verbose

    lines: Sequence[str] = []
    with open(input_path, "r", encoding="utf-8") as input_file:
        lines = input_file.readlines()

    # Part 1
    result: int | None = None
    total_time: int = 0
    for _ in range(num_trials):
        start_time: int = time.perf_counter_ns()
        result = part1(lines, verbose_mode)
        total_time += time.perf_counter_ns() - start_time
    average_time: float = total_time / num_trials
    rprint(f"Part 1 - Result = {result}")
    rprint(f"Part 1 took {format_execution_time(average_time)}")

    # Part 2
    result: int | None = None
    total_time: int = 0
    for _ in range(num_trials):
        start_time: int = time.perf_counter_ns()
        result = part2(lines, verbose_mode)
        total_time += time.perf_counter_ns() - start_time
    average_time: float = total_time / num_trials
    rprint(f"Part 2 - Result = {result}")
    rprint(f"Part 2 took {format_execution_time(average_time)}")


def part1(lines: Sequence[str], verbose_mode: bool) -> int:
    """Part 1 of Day XX of Advent of Code XXXX

    Parameters
    ----------
    lines : Sequence[str]
        The lines of the input file.
    verbose : bool
        Turn on verbose mode.

    Returns
    -------
    int
        The answer
    """
    return 0


def part2(lines: Sequence[str], verbose: bool) -> int:
    """Part 2 of Day XX of Advent of Code XXXX

    Parameters
    ----------
    lines : Sequence[str]
        The lines of the input file.
    verbose : bool
        Turn on verbose mode.

    Returns
    -------
    int
        The answer
    """
    return 0


def _parse_args(args: Sequence[str]) -> argparse.Namespace:
    """Parses the command line arguments

    Parameters
    ----------
    args : Sequence[str]
        The command line arguments.

    Returns
    -------
    parsed_args : argparse.Namespace
        The parsed command line arguments.
    """
    parser = argparse.ArgumentParser(
        prog="aoc XXXX day XX",
        description="Program to solve Day XX of Advent of Code XXXX",
    )
    parser.add_argument("input_path", type=str, help="Path to input file")
    parser.add_argument(
        "-num_trials",
        "--num_trials",
        type=int,
        default=100,
        help="Number of trials to use when measuring run time",
    )
    parser.add_argument(
        "-v",
        "--v",
        "-verbose",
        "--verbose",
        action="store_true",
        dest="verbose",
        help="Turn on verbose mode",
    )
    return parser.parse_args(args)


if __name__ == "__main__":
    import sys

    main(sys.argv[1:])
