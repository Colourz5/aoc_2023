"""This module contains the solution to Day 24 of Advent of Code 2023"""

# Standard libraries
import argparse
import time
from typing import Sequence

# External libraries
from rich import print as rprint
import sympy

# Internal libraries
from python_aoc.common import format_execution_time


def main(args: Sequence[str]) -> None:
    parsed_args = _parse_args(args)
    input_path: str = parsed_args.input_path
    num_trials: int = parsed_args.num_trials
    verbose_mode: bool = parsed_args.verbose

    lines: Sequence[str] = []
    with open(input_path, "r", encoding="utf-8") as input_file:
        lines = input_file.readlines()

    # Part 1
    result: int | None = None
    total_time: int = 0
    for _ in range(num_trials):
        start_time: int = time.perf_counter_ns()
        result = part1(lines, verbose_mode)
        total_time += time.perf_counter_ns() - start_time
    average_time: float = total_time / num_trials
    rprint(f"Part 1 - Result = {result}")
    rprint(f"Part 1 took {format_execution_time(average_time)}")

    # Part 2
    result: int | None = None
    total_time: int = 0
    for _ in range(num_trials):
        start_time: int = time.perf_counter_ns()
        result = part2(lines, verbose_mode)
        total_time += time.perf_counter_ns() - start_time
    average_time: float = total_time / num_trials
    rprint(f"Part 2 - Result = {result}")
    rprint(f"Part 2 took {format_execution_time(average_time)}")


def part1(lines: Sequence[str], verbose_mode: bool) -> int:
    """Part 1 of Day XX of Advent of Code XXXX

    Parameters
    ----------
    lines : Sequence[str]
        The lines of the input file.
    verbose : bool
        Turn on verbose mode.

    Returns
    -------
    int
        The answer
    """
    hailstones: list[tuple[tuple[int, int, int], tuple[int, int, int]]] = []
    for line in lines:
        line = line.rstrip("\n")
        position_tokens, velocity_tokens = line.split(" @ ")
        position_values = [int(position) for position in position_tokens.split(", ")]
        velocity_values = [int(velocity) for velocity in velocity_tokens.split(", ")]
        assert len(position_values) == 3
        assert len(velocity_values) == 3
        hailstones.append(
            (
                (position_values[0], position_values[1], position_values[2]),
                (velocity_values[0], velocity_values[1], velocity_values[2]),
            )
        )
    # x_range = (7, 27)
    # y_range = (7, 27)
    x_range = (200000000000000, 400000000000000)
    y_range = (200000000000000, 400000000000000)
    total_hits: int = 0
    for hail_idx, hailstone in enumerate(hailstones[:-1]):
        for other_hail_idx, other_hailstone in enumerate(hailstones[hail_idx + 1 :]):
            other_hail_idx += hail_idx + 1
            this_position, this_velocity = hailstone
            other_position, other_velocity = other_hailstone
            discriminant = (
                other_velocity[1] * this_velocity[0]
                - other_velocity[0] * this_velocity[1]
            )
            if discriminant == 0:
                continue
            hit_time = (
                other_velocity[0] * (this_position[1] - other_position[1])
                - other_velocity[1] * (this_position[0] - other_position[0])
            ) / discriminant
            other_hit_time = (
                this_velocity[0] * hit_time + (this_position[0] - other_position[0])
            ) / other_velocity[0]
            hit_x = this_velocity[0] * hit_time + this_position[0]
            hit_y = this_velocity[1] * hit_time + this_position[1]
            if hit_time < 0 or other_hit_time < 0:
                continue
            if hit_x < x_range[0] or hit_x > x_range[1]:
                continue
            if hit_y < y_range[0] or hit_y > y_range[1]:
                continue
            total_hits += 1
    return total_hits


def part2(lines: Sequence[str], verbose: bool) -> int:
    """Part 2 of Day XX of Advent of Code XXXX

    Parameters
    ----------
    lines : Sequence[str]
        The lines of the input file.
    verbose : bool
        Turn on verbose mode.

    Returns
    -------
    int
        The answer
    """
    hailstones: list[tuple[tuple[int, int, int], tuple[int, int, int]]] = []
    for line in lines:
        line = line.rstrip("\n")
        position_tokens, velocity_tokens = line.split(" @ ")
        position_values = [int(position) for position in position_tokens.split(", ")]
        velocity_values = [int(velocity) for velocity in velocity_tokens.split(", ")]
        assert len(position_values) == 3
        assert len(velocity_values) == 3
        hailstones.append(
            (
                (position_values[0], position_values[1], position_values[2]),
                (velocity_values[0], velocity_values[1], velocity_values[2]),
            )
        )

    pos1, vel1 = hailstones[0]
    pos2, vel2 = hailstones[1]
    pos3, vel3 = hailstones[2]
    pos_x, pos_y, pos_z, vel_x, vel_y, vel_z, t1, t2, t3 = sympy.symbols(
        "pos_x pos_y pos_z vel_x vel_y vel_z t1 t2 t3", real=True
    )
    equations = [
        sympy.Eq(pos_x + vel_x * t1, pos1[0] + vel1[0] * t1),
        sympy.Eq(pos_y + vel_y * t1, pos1[1] + vel1[1] * t1),
        sympy.Eq(pos_z + vel_z * t1, pos1[2] + vel1[2] * t1),
        sympy.Eq(pos_x + vel_x * t2, pos2[0] + vel2[0] * t2),
        sympy.Eq(pos_y + vel_y * t2, pos2[1] + vel2[1] * t2),
        sympy.Eq(pos_z + vel_z * t2, pos2[2] + vel2[2] * t2),
        sympy.Eq(pos_x + vel_x * t3, pos3[0] + vel3[0] * t3),
        sympy.Eq(pos_y + vel_y * t3, pos3[1] + vel3[1] * t3),
        sympy.Eq(pos_z + vel_z * t3, pos3[2] + vel3[2] * t3),
    ]
    solution = sympy.solve(equations)[0]
    assert solution[t1] > 0
    assert solution[t2] > 0
    assert solution[t3] > 0
    answer: int = solution[pos_x] + solution[pos_y] + solution[pos_z]
    return answer


def _parse_args(args: Sequence[str]) -> argparse.Namespace:
    """Parses the command line arguments

    Parameters
    ----------
    args : Sequence[str]
        The command line arguments.

    Returns
    -------
    parsed_args : argparse.Namespace
        The parsed command line arguments.
    """
    parser = argparse.ArgumentParser(
        prog="aoc XXXX day XX",
        description="Program to solve Day XX of Advent of Code XXXX",
    )
    parser.add_argument("input_path", type=str, help="Path to input file")
    parser.add_argument(
        "-num_trials",
        "--num_trials",
        type=int,
        default=100,
        help="Number of trials to use when measuring run time",
    )
    parser.add_argument(
        "-v",
        "--v",
        "-verbose",
        "--verbose",
        action="store_true",
        dest="verbose",
        help="Turn on verbose mode",
    )
    return parser.parse_args(args)


if __name__ == "__main__":
    import sys

    main(sys.argv[1:])
