"""This module contains the solution to Day 25 of Advent of Code 2023"""

# Standard libraries
import argparse
import math
import time
from typing import Sequence

# External libraries
import networkx as nx
from rich import print as rprint

# Internal libraries
from python_aoc.common import format_execution_time


def main(args: Sequence[str]) -> None:
    parsed_args = _parse_args(args)
    input_path: str = parsed_args.input_path
    num_trials: int = parsed_args.num_trials
    verbose_mode: bool = parsed_args.verbose

    lines: Sequence[str] = []
    with open(input_path, "r", encoding="utf-8") as input_file:
        lines = input_file.readlines()

    # Part 1
    result: int | None = None
    total_time: int = 0
    for _ in range(num_trials):
        start_time: int = time.perf_counter_ns()
        result = find_minimal_cut(lines, verbose_mode)
        total_time += time.perf_counter_ns() - start_time
    average_time: float = total_time / num_trials
    rprint(f"Part 1 - Result = {result}")
    rprint(f"Part 1 took {format_execution_time(average_time)}")


def find_minimal_cut(lines: Sequence[str], verbose_mode: bool) -> int:
    """Finds the product of the number of components in the two disconnected components resulting from cutting three edges.
    Solves part 1 of Day 25 of Advent of Code 2023.

    Parameters
    ----------
    lines : Sequence[str]
        The lines of the input file.
    verbose : bool
        Turn on verbose mode.

    Returns
    -------
    int
        The answer
    """
    graph: dict[str, set[str]] = {}
    for line in lines:
        line = line.rstrip("\n")
        source, destination_tokens = line.split(":")
        destinations = destination_tokens.strip().split(" ")
        destination_set = set(destinations)
        if source not in graph:
            graph[source] = destination_set
        else:
            graph[source] |= destination_set
        for destination in destinations:
            if destination not in graph:
                graph[destination] = {source}
            else:
                graph[destination] |= {source}

    # List all edges
    edges: list[tuple[str, str]] = []
    for node in graph:
        for neighbour in graph[node]:
            if (node, neighbour) in edges or (neighbour, node) in edges:
                continue
            else:
                edges.append((node, neighbour))

    # Find the minimum edge cut assuming that it will take 3 cuts
    nx_graph: nx.Graph = nx.Graph()
    for node in graph:
        nx_graph.add_node(node)
    for source, dest in edges:
        nx_graph.add_edge(source, dest)
        nx_graph.add_edge(dest, source)
    edge_cut_set = nx.minimum_edge_cut(nx_graph)

    # Remove the edges
    edges_to_remove = tuple(edge_cut_set)
    assert (
        len(edges_to_remove) == 3
    ), f"Could not find three edges that split the graph! Instead found {edges_to_remove}"
    new_graph = {node: neighbours.copy() for node, neighbours in graph.items()}
    remove_edges(new_graph, edges_to_remove)
    # Count the component counts
    num_components, component_count = count_disconnected_components(new_graph)
    assert (
        num_components == 2
    ), f"Could not split the graph into two! Instead split into {num_components}"
    if verbose_mode:
        rprint(f"Needed to remove {edges_to_remove}")
    # Calculate the product
    return math.prod(component_count)


def remove_edges(graph: dict[str, set[str]], edges: Sequence[tuple[str, str]]) -> None:
    """Remove edges from the graph.

    Parameters
    ----------
    graph : dict[str, set[str]]
        The graph.
    edges : Sequence[tuple[str, str]]
        The edges to remove.
    """
    for source, dest in edges:
        assert source in graph
        assert dest in graph
        graph[source].remove(dest)
        graph[dest].remove(source)


def count_disconnected_components(
    graph: dict[str, set[str]],
) -> tuple[int, Sequence[int]]:
    """Counts the number of disconnected components.

    Parameters
    ----------
    graph : dict[str, set[str]]
        The graph.

    Returns
    -------
    num_components : int
        The number of disconnected components.
    component_count : Sequence[int]
        The number of nodes in each disconnected component.
    """
    visited: set[str] = set()
    num_components: int = 0
    component_count: list[int] = []
    for vertex in graph:
        if vertex not in visited:
            num_components += 1
            new_visited = breadth_first_search(graph, vertex)
            component_count.append(len(new_visited))
            visited.update(new_visited)
    return (num_components, component_count)


def breadth_first_search(graph: dict[str, set[str]], source: str) -> set[str]:
    """Breadth first search algorithm with a source but no sink.

    Parameters
    ----------
    graph : dict[str, set[str]]
        The graph.
    source : str
        The source node.

    Returns
    -------
    visited : set[str]
        The nodes that can be visited by the source node.
    """
    assert source in graph
    visited: set[str] = set()
    work_queue: list[str] = [source]
    while len(work_queue) > 0:
        current_node = work_queue.pop(0)
        if current_node in visited:
            continue
        visited.add(current_node)
        # Get neighbours
        for neighbour in graph[current_node]:
            work_queue.append(neighbour)

    return visited


def _parse_args(args: Sequence[str]) -> argparse.Namespace:
    """Parses the command line arguments

    Parameters
    ----------
    args : Sequence[str]
        The command line arguments.

    Returns
    -------
    parsed_args : argparse.Namespace
        The parsed command line arguments.
    """
    parser = argparse.ArgumentParser(
        prog="aoc 2023 day 25",
        description="Program to solve Day 25 of Advent of Code 2023",
    )
    parser.add_argument("input_path", type=str, help="Path to input file")
    parser.add_argument(
        "-num_trials",
        "--num_trials",
        type=int,
        default=100,
        help="Number of trials to use when measuring run time",
    )
    parser.add_argument(
        "-v",
        "--v",
        "-verbose",
        "--verbose",
        action="store_true",
        dest="verbose",
        help="Turn on verbose mode",
    )
    return parser.parse_args(args)


if __name__ == "__main__":
    import sys

    main(sys.argv[1:])
