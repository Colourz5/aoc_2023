"""Main module for Day 1 of AOC 2023."""

# Standard libraries
import argparse
import time
from typing import Sequence

# External libraries
from rich import print as rprint


valid_digit_strings: dict[str, str] = {
    "one": "1",
    "two": "2",
    "three": "3",
    "four": "4",
    "five": "5",
    "six": "6",
    "seven": "7",
    "eight": "8",
    "nine": "9",
}
"""dict[str, str]: A dictionary mapping word digits to their corresponding numerical digit as a character."""


def main(args: Sequence[str]) -> None:
    """Main function for Day 1 of AOC 2023.

    Parameters
    ----------
    args : Sequence[str]
        The command line arguments.
    """
    # Parse command line arguments
    parsed_args = _parse_args(args)
    input_path: str = parsed_args.input_path
    num_trials: int = parsed_args.num_trials

    # Read input file
    lines: Sequence[str] = []
    with open(input_path, "r", encoding="utf-8") as input_file:
        lines = input_file.readlines()

    rprint(f"Running {num_trials} trials...")

    # Part 1
    total_time = 0
    result = 0
    for _ in range(num_trials):
        start_time = time.perf_counter_ns()
        result = calculate_calibration_value_sum(lines)
        elapsed_time = time.perf_counter_ns() - start_time
        total_time += elapsed_time
    rprint(f"Part 1 - Result = {result}")
    average_time = total_time / num_trials
    rprint(f"Part 1 took {format_execution_time(average_time)}")

    # Part 2
    total_time = 0
    result = 0
    for _ in range(num_trials):
        start_time = time.perf_counter_ns()
        result = calculate_calibration_value_sum_with_letters(lines)
        elapsed_time = time.perf_counter_ns() - start_time
        total_time += elapsed_time
    rprint(f"Part 2 - Result = {result}")
    average_time = total_time / num_trials
    rprint(f"Part 2 took {format_execution_time(average_time)}")


def calculate_calibration_value_sum(lines: Sequence[str]) -> int:
    """Calculates the calibration value sum. This is the solution to Part 1 of AOC 2023 Day 1.
    The calibration value sum is the total sum of all calibration values. A calibration value is defined as the number whose
    first digit appears first in a line, and whose second digit appears last in the same line, given a calibration document.

    Parameters
    ----------
    lines : Sequence[str]
        The lines of the calibration document.

    Returns
    -------
    int
        The calibration value sum.
    """
    result = 0
    for _idx, line in enumerate(lines):
        first_digit: str | None = None
        second_digit: str | None = None
        for _char_idx, char in enumerate(line):
            if char.isdigit():
                if first_digit is None:
                    first_digit = char
                    second_digit = char
                else:
                    second_digit = char
        if first_digit is None or second_digit is None:
            continue
        number = int(f"{first_digit}{second_digit}")
        result += number
    return result


def calculate_calibration_value_sum_with_letters(lines: Sequence[str]) -> int:
    """Calculates the calibration value sum also including word digits. This is the solution to Part 2 of AOC 2023 Day 1.
    The calibration value sum is the total sum of all calibration values. A calibration value is defined as the number whose
    first digit appears first in a line, and whose second digit appears last in the same line, given a calibration document.
    The difference from Part 1 is that the calibration document may contain word digits, which are the spelled out version of
    digits 1 to 9.

    Parameters
    ----------
    lines : Sequence[str]
        The lines of the calibration document.

    Returns
    -------
    int
        The calibration value sum.
    """
    result = 0
    for _idx, line in enumerate(lines):
        first_digit: str | None = None
        second_digit: str | None = None
        for char_idx, char in enumerate(line):
            # Char is a digit
            if char.isdigit():
                if first_digit is None:
                    first_digit = char
                    second_digit = char
                else:
                    second_digit = char
            # Assuming we have some alpha
            elif char.isalpha():
                for digit_string, digit_char in valid_digit_strings.items():
                    end_idx = char_idx + len(digit_string)
                    if end_idx > len(line):
                        continue
                    current_slice = line[char_idx : char_idx + len(digit_string)]
                    if current_slice == digit_string:
                        char = digit_char
                        if first_digit is None:
                            first_digit = char
                            second_digit = char
                        else:
                            second_digit = char
                        break
        if first_digit is None or second_digit is None:
            continue
        number = int(f"{first_digit}{second_digit}")
        result += number
    return result


def format_execution_time(time_taken: float) -> str:
    """Formats the execution time into a human readable string.

    Parameters
    ----------
    time_taken : float
        The time taken in seconds.

    Returns
    -------
    str
        The formatted string.
    """
    seconds = time_taken // 1_000_000_000
    milliseconds = (time_taken - seconds * 1_000_000_000) // 1_000_000
    microseconds = (
        time_taken - seconds * 1_000_000_000 - milliseconds * 1_000_000
    ) // 1_000
    nanoseconds = (
        time_taken
        - seconds * 1_000_000_000
        - milliseconds * 1_000_000
        - microseconds * 1_000
    )
    return f"{seconds} seconds, {milliseconds} milliseconds, {microseconds} microseconds, {nanoseconds} nanoseconds"


def _parse_args(args: Sequence[str]) -> argparse.Namespace:
    """Parses the command line arguments.

    Parameters
    ----------
    args : Sequence[str]
        The command line arguments.

    Returns
    -------
    argparse.Namespace
        The parsed arguments.
    """
    parser = argparse.ArgumentParser(
        prog="AOC 2023 Day 1", description="Day 1 of Advent of Code 2023"
    )
    parser.add_argument("input_path", help="File path to input file")
    parser.add_argument(
        "-num_trials",
        "--num_trials",
        type=int,
        default=1000,
        help="Number of trials for benchmarking",
    )
    return parser.parse_args(args)


if __name__ == "__main__":
    import sys

    main(sys.argv[1:])
