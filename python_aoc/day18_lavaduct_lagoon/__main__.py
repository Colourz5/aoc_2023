"""This module contains the solution to Day 18 of Advent of Code 2023"""

# Standard libraries
import argparse
import time
from typing import Sequence

# External libraries
from matplotlib import pyplot as plt
from PIL import Image
from rich import print as rprint

# Internal libraries
from python_aoc.common import format_execution_time


def main(args: Sequence[str]) -> None:
    parsed_args = _parse_args(args)
    input_path: str = parsed_args.input_path
    num_trials: int = parsed_args.num_trials
    verbose_mode: bool = parsed_args.verbose

    lines: Sequence[str] = []
    with open(input_path, "r", encoding="utf-8") as input_file:
        lines = input_file.readlines()

    # Part 1
    result: int | None = None
    total_time: int = 0
    for _ in range(num_trials):
        start_time: int = time.perf_counter_ns()
        result = part1(lines, verbose_mode)
        total_time += time.perf_counter_ns() - start_time
    average_time: float = total_time / num_trials
    rprint(f"Part 1 - Result = {result}")
    rprint(f"Part 1 took {format_execution_time(average_time)}")

    # Part 2
    result: int | None = None
    total_time: int = 0
    for _ in range(num_trials):
        start_time: int = time.perf_counter_ns()
        result = part2(lines, verbose_mode)
        total_time += time.perf_counter_ns() - start_time
    average_time: float = total_time / num_trials
    rprint(f"Part 2 - Result = {result}")
    rprint(f"Part 2 took {format_execution_time(average_time)}")


def part1(lines: Sequence[str], verbose_mode: bool) -> int:
    """Calculates the volume of the lagoon dug out by the parsed instructions.
    Solves part 1 of Day 18 of Advent of Code 2023.

    Parameters
    ----------
    lines : Sequence[str]
        The lines of the input file.
    verbose : bool
        Turn on verbose mode.

    Returns
    -------
    int
        The volume of the lagoon following the wrong instructions.
    """
    directions_to_offsets: dict[str, tuple[int, int]] = {
        "U": (-1, 0),
        "R": (0, 1),
        "L": (0, -1),
        "D": (1, 0),
    }
    current_location: tuple[int, int] = (0, 0)
    grid: dict[tuple[int, int], str] = {current_location: "ffffff"}
    topmost_idx = current_location[0]
    bottommost_idx = current_location[0]
    leftmost_idx = current_location[1]
    rightmost_idx = current_location[1]
    for line in lines:
        line = line.rstrip("\n")
        tokens = line.split()
        if len(tokens) == 0:
            continue
        direction = tokens[0]
        num_steps = int(tokens[1])
        color = tokens[2].strip("()#")
        row_idx, column_idx = current_location
        assert direction in directions_to_offsets
        row_offset, column_offset = directions_to_offsets[direction]
        new_location = current_location
        for step in range(num_steps):
            target_row = row_idx + (step + 1) * row_offset
            target_column = column_idx + (step + 1) * column_offset
            leftmost_idx = min(leftmost_idx, target_column)
            rightmost_idx = max(rightmost_idx, target_column)
            topmost_idx = min(topmost_idx, target_row)
            bottommost_idx = max(bottommost_idx, target_row)
            grid[(target_row, target_column)] = color
            new_location = (target_row, target_column)
        current_location = new_location
    width: int = rightmost_idx - leftmost_idx + 1
    height: int = bottommost_idx - topmost_idx + 1
    # Recentre grid so that top left cell is (0, 0)
    grid = {
        (row_idx - topmost_idx, column_idx - leftmost_idx): color
        for (row_idx, column_idx), color in grid.items()
    }
    if verbose_mode:
        rprint(f"Width = {width}")
        rprint(f"Height = {height}")
        rprint(f"Total size = {width * height}")
        rprint(f"Boundary points = {len(grid)}")

    rasterised: dict[tuple[int, int], str] = {}
    for row_idx in range(height):
        parity = False
        for column_idx in range(width):
            current_location = (row_idx, column_idx)
            if current_location in grid:
                above_exists = (row_idx - 1, column_idx) in grid
                below_exists = (row_idx + 1, column_idx) in grid
                match (above_exists, below_exists):
                    case (True, True):
                        parity ^= True
                    case (True, False):
                        parity ^= True
                    case (False, True):
                        pass
                    case (False, False):
                        pass
            elif parity:
                rasterised[current_location] = "ff0000"
    grid.update(rasterised)

    # Rasterise
    if verbose_mode:
        buffer = generate_image_bytes(grid, width, height)
        buffer_image = Image.frombytes("RGB", (width, height), buffer)
        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.imshow(buffer_image)

        plt.show()
        plt.close(fig)

    return len(grid)


def part2(lines: Sequence[str], verbose_mode: bool) -> int:
    """Calculates the volume of the lagoon dug out by the parsed instructions.
    Solves part 2 of Day 18 of Advent of Code 2023.

    Parameters
    ----------
    lines : Sequence[str]
        The lines of the input file.
    verbose : bool
        Turn on verbose mode.

    Returns
    -------
    int
        The volume of the lagoon following the correct instructions.
    """
    direction_decoder: str = "RDLU"
    directions_to_offsets: dict[str, tuple[int, int]] = {
        "U": (-1, 0),
        "R": (0, 1),
        "L": (0, -1),
        "D": (1, 0),
    }
    current_location: tuple[int, int] = (0, 0)
    edges: list[tuple[int, int]] = [current_location]
    boundary_count: int = 0
    for _, line in enumerate(lines):
        line = line.rstrip("\n")
        tokens = line.split()
        command = tokens[2].strip("()#")
        direction = direction_decoder[int(command[-1])]
        num_steps = int(command[:-1], base=16)
        row_idx, column_idx = current_location
        assert direction in directions_to_offsets
        row_offset, column_offset = directions_to_offsets[direction]
        target_row = row_idx + num_steps * row_offset
        target_column = column_idx + num_steps * column_offset
        current_location = (target_row, target_column)
        distance = abs(target_row - row_idx) + abs(target_column - column_idx)
        boundary_count += distance
        edges.append(current_location)
    edges = edges + [edges[0]]
    total = 0
    for _, (start_point, end_point) in enumerate(zip(edges[:-1], edges[1:])):
        current_x = start_point[1]
        current_y = start_point[0]
        next_x = end_point[1]
        next_y = end_point[0]
        current_area = current_x * next_y - next_x * current_y
        total += current_area
    area = abs(total) // 2
    volume = (2 * (area + 1) + boundary_count) // 2
    if verbose_mode:
        rprint(f"The area is {area}")
        rprint(f"There are {boundary_count} tiles on the boundary")
    return volume


def generate_image_bytes(
    grid: dict[tuple[int, int], str],
    width: int,
    height: int,
) -> bytes:
    """Encodes the grid into a byte array of RGB data.

    Parameters
    ----------
    grid : dict[tuple[int, int], str]
        The grid to encode.
        The keys are coordinates and the value is a hexadecimal code for the color.
    width : int
        The width of the grid.
    height : int
        The height of the grid.

    Returns
    -------
    bytes
        The RGB data.
    """
    buffer: bytearray = bytearray()
    for row_idx in range(height):
        for column_idx in range(width):
            current_location = (row_idx, column_idx)
            if current_location in grid:
                color = grid[current_location]
            else:
                color = "ffffff"
            buffer.extend(bytearray.fromhex(color))
    return bytes(buffer)


def _parse_args(args: Sequence[str]) -> argparse.Namespace:
    """Parses the command line arguments

    Parameters
    ----------
    args : Sequence[str]
        The command line arguments.

    Returns
    -------
    parsed_args : argparse.Namespace
        The parsed command line arguments.
    """
    parser = argparse.ArgumentParser(
        prog="aoc 2023 day 18",
        description="Program to solve Day 18 of Advent of Code 2023",
    )
    parser.add_argument("input_path", type=str, help="Path to input file")
    parser.add_argument(
        "-num_trials",
        "--num_trials",
        type=int,
        default=100,
        help="Number of trials to use when measuring run time",
    )
    parser.add_argument(
        "-v",
        "--v",
        "-verbose",
        "--verbose",
        action="store_true",
        dest="verbose",
        help="Turn on verbose mode",
    )
    return parser.parse_args(args)


if __name__ == "__main__":
    import sys

    main(sys.argv[1:])
