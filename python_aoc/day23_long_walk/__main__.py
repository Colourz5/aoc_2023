"""This module contains the solution to Day 23 of Advent of Code 2023"""
from __future__ import annotations

# Standard libraries
import argparse
from dataclasses import dataclass
import time
from typing import Sequence

# External libraries
from rich import print as rprint

# Internal libraries
from python_aoc.common import format_execution_time


@dataclass
class Node:
    row_idx: int
    column_idx: int
    neighbours: set[tuple[int, int, int]]


def main(args: Sequence[str]) -> None:
    parsed_args = _parse_args(args)
    input_path: str = parsed_args.input_path
    num_trials: int = parsed_args.num_trials
    verbose_mode: bool = parsed_args.verbose

    lines: Sequence[str] = []
    with open(input_path, "r", encoding="utf-8") as input_file:
        lines = input_file.readlines()

    # Part 1
    result: int | None = None
    total_time: int = 0
    for _ in range(num_trials):
        start_time: int = time.perf_counter_ns()
        result = find_most_scenic_path(lines, verbose_mode, True)
        total_time += time.perf_counter_ns() - start_time
    average_time: float = total_time / num_trials
    rprint(f"Part 1 - Result = {result}")
    rprint(f"Part 1 took {format_execution_time(average_time)}")

    # Part 2
    result: int | None = None
    total_time: int = 0
    for _ in range(num_trials):
        start_time: int = time.perf_counter_ns()
        result = find_most_scenic_path_contract(lines, verbose_mode)
        total_time += time.perf_counter_ns() - start_time
    average_time: float = total_time / num_trials
    rprint(f"Part 2 - Result = {result}")
    rprint(f"Part 2 took {format_execution_time(average_time)}")


def find_most_scenic_path(
    lines: Sequence[str], verbose_mode: bool, slopes: bool
) -> int:
    """Part 1 of Day XX of Advent of Code XXXX

    Parameters
    ----------
    lines : Sequence[str]
        The lines of the input file.
    verbose : bool
        Turn on verbose mode.

    Returns
    -------
    int
        The answer
    """
    return 0
    grid: Sequence[str] = []
    start_pos: tuple[int, int] | None = None
    end_pos: tuple[int, int] | None = None
    for line_idx, line in enumerate(lines):
        line = line.rstrip("\n")
        if start_pos is None:
            start_pos = (0, line.index("."))
        end_pos = (line_idx, line.index("."))
        grid.append(line)
    assert start_pos is not None
    assert end_pos is not None
    if verbose_mode:
        print_grid(grid, start_pos, end_pos, [])
    work_queue: list[tuple[int, int, int, set[tuple[int, int]]]] = [
        (start_pos[0], start_pos[1], 0, set())
    ]
    seen: set[tuple[int, int, Sequence[tuple[int, int]]]] = set()
    max_steps: int = 0
    true_visited: Sequence[tuple[int, int]] = []
    while len(work_queue) > 0:
        row_idx, column_idx, steps, visited = work_queue.pop(0)

        if (row_idx, column_idx, tuple(visited)) in seen:
            continue

        seen.add((row_idx, column_idx, tuple(visited)))

        if (row_idx, column_idx) == end_pos:
            if max_steps < steps:
                max_steps = steps
                true_visited = list(visited)
            rprint(f"Reached the end using {steps} steps")
            continue

        # Get the next nodes
        destinations = get_destinations(grid, row_idx, column_idx, slopes)
        for target_row, target_column in destinations:
            if (target_row, target_column) in visited:
                continue
            else:
                new_visited = visited.copy()
                new_visited.add((target_row, target_column))
                work_queue.append((target_row, target_column, steps + 1, new_visited))

    if verbose_mode:
        print_grid(grid, start_pos, end_pos, true_visited)
    return max_steps


def find_most_scenic_path_contract(lines: Sequence[str], verbose_mode: bool) -> int:
    grid, graph, start_pos, end_pos = construct_graph(lines, False)
    rprint(graph)
    plot_grid(grid, start_pos, end_pos, tuple(graph.keys()))

    seen: set[tuple[int, int]] = set()

    def depth_first_search(row_idx: int, column_idx: int) -> float:
        nonlocal graph
        nonlocal seen
        if (row_idx, column_idx) == end_pos:
            return 0
        steps = -float("inf")
        current_node = graph[(row_idx, column_idx)]
        seen.add((row_idx, column_idx))
        for next_row, next_column, cost in current_node.neighbours:
            if (next_row, next_column) in seen:
                continue
            steps = max(
                steps,
                depth_first_search(next_row, next_column) + cost,
            )
        seen.remove((row_idx, column_idx))
        return steps

    max_steps = depth_first_search(start_pos[0], start_pos[1])
    return int(max_steps)


def construct_graph(
    lines: Sequence[str], slopes: bool
) -> tuple[
    Sequence[str], dict[tuple[int, int], Node], tuple[int, int], tuple[int, int]
]:
    grid: Sequence[str] = []
    start_pos: tuple[int, int] | None = None
    end_pos: tuple[int, int] | None = None
    for line_idx, line in enumerate(lines):
        line = line.rstrip("\n")
        if start_pos is None:
            start_pos = (0, line.index("."))
        end_pos = (line_idx, line.index("."))
        grid.append(line)
    assert start_pos is not None
    assert end_pos is not None
    # Construct node list
    nodes: dict[tuple[int, int], Node] = {}
    for row_idx, row in enumerate(grid):
        for column_idx, cell in enumerate(row):
            if cell == "#":
                continue
            neighbours = get_destinations(grid, row_idx, column_idx, slopes)
            if len(neighbours) > 2 or len(neighbours) == 1:
                nodes[(row_idx, column_idx)] = Node(
                    row_idx=row_idx, column_idx=column_idx, neighbours=set()
                )
    # Construct adjacency list
    for (row_idx, column_idx), node in nodes.items():
        work_queue: list[tuple[int, int, int]] = [(row_idx, column_idx, 0)]
        seen: set[tuple[int, int]] = set()
        while len(work_queue) > 0:
            current_row, current_column, steps = work_queue.pop()
            if (current_row, current_column) in seen:
                continue
            seen.add((current_row, current_column))
            if steps > 0 and (current_row, current_column) in nodes:
                node.neighbours.add((current_row, current_column, steps))
                continue

            for target_row, target_column in get_destinations(
                grid, current_row, current_column, slopes
            ):
                work_queue.append((target_row, target_column, steps + 1))

    return (grid, nodes, start_pos, end_pos)


def get_destinations(
    grid: Sequence[str], row_idx: int, column_idx: int, slopes: bool
) -> Sequence[tuple[int, int]]:
    assert len(grid) > 0
    height: int = len(grid)
    width: int = len(grid[0])
    cell = grid[row_idx][column_idx]
    directions: Sequence[tuple[int, int]] = []
    match cell:
        case ">" if slopes:
            directions = ((0, 1),)
        case "<" if slopes:
            directions = ((0, -1),)
        case "v" if slopes:
            directions = ((1, 0),)
        case "^" if slopes:
            directions = ((-1, 0),)
        case _ as cell:
            directions = ((-1, 0), (1, 0), (0, -1), (0, 1))
    destinations: list[tuple[int, int]] = []
    for row_offset, column_offset in directions:
        target_row = row_idx + row_offset
        target_column = column_idx + column_offset
        if (
            target_row < 0
            or target_row >= height
            or target_column < 0
            or target_column >= width
        ):
            continue
        if grid[target_row][target_column] == "#":
            continue
        destinations.append((target_row, target_column))
    return destinations


def print_grid(
    grid: Sequence[str],
    start_pos: tuple[int, int],
    end_pos: tuple[int, int],
    visited: Sequence[tuple[int, int]],
) -> None:
    assert len(grid) > 0
    width: int = len(grid[0])
    rprint(" |", end="")
    for column_idx in range(width):
        rprint(f"[bold white]{str(column_idx)[-1]}[/bold white]", end="|")
    rprint()
    for row_idx, row in enumerate(grid):
        rprint(f"[bold white]{str(row_idx)[-1]}[/bold white]", end="|")
        for column_idx, cell in enumerate(row):
            match cell:
                case "#":
                    cell = " "
                    style = "on black"
                case ".":
                    cell = " "
                    style = "on white"
                case ">":
                    cell = "\u2192"
                    style = "on cyan"
                case "<":
                    cell = "\u2190"
                    style = "on yellow"
                case "v":
                    cell = "\u2193"
                    style = "on green"
                case "^":
                    cell = "\u2191"
                    style = "on blue"
                case _:
                    style = "on magenta"
            if (row_idx, column_idx) == start_pos:
                cell = "S"
                style = "on red"
            elif (row_idx, column_idx) == end_pos:
                cell = "E"
                style = "on red"
            elif (row_idx, column_idx) in visited:
                cell = chr(0x130C0)
                style = "on magenta"
            else:
                pass
            rprint(2 * f"[{style}]{cell}[/{style}]", end="")
        rprint()
    rprint()


def plot_grid(
    grid: Sequence[str],
    start_pos: tuple[int, int],
    end_pos: tuple[int, int],
    visited: Sequence[tuple[int, int]],
) -> None:
    assert len(grid) > 0
    width: int = len(grid[0])
    height: int = len(grid)
    from matplotlib import pyplot as plt
    import numpy as np

    data = np.zeros((height, width, 3), dtype=int)
    for row_idx, row in enumerate(grid):
        for column_idx, cell in enumerate(row):
            match cell:
                case "#":
                    color = (0, 0, 0)
                case ".":
                    color = (255, 255, 255)
                case ">":
                    color = (0, 255, 255)
                case "<":
                    color = (255, 255, 0)
                case "v":
                    color = (0, 255, 0)
                case "^":
                    color = (0, 0, 255)
                case _:
                    color = (255, 0, 255)
            if (row_idx, column_idx) == start_pos:
                color = (255, 0, 0)
            elif (row_idx, column_idx) == end_pos:
                color = (255, 0, 0)
            elif (row_idx, column_idx) in visited:
                color = (140, 24, 41)
            data[row_idx, column_idx] = color

    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.imshow(data)
    plt.show()
    plt.close(fig)


def _parse_args(args: Sequence[str]) -> argparse.Namespace:
    """Parses the command line arguments

    Parameters
    ----------
    args : Sequence[str]
        The command line arguments.

    Returns
    -------
    parsed_args : argparse.Namespace
        The parsed command line arguments.
    """
    parser = argparse.ArgumentParser(
        prog="aoc XXXX day XX",
        description="Program to solve Day XX of Advent of Code XXXX",
    )
    parser.add_argument("input_path", type=str, help="Path to input file")
    parser.add_argument(
        "-num_trials",
        "--num_trials",
        type=int,
        default=100,
        help="Number of trials to use when measuring run time",
    )
    parser.add_argument(
        "-v",
        "--v",
        "-verbose",
        "--verbose",
        action="store_true",
        dest="verbose",
        help="Turn on verbose mode",
    )
    return parser.parse_args(args)


if __name__ == "__main__":
    import sys

    main(sys.argv[1:])
