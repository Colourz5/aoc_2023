"""This module contains the solution to Day X of Advent of Code XXXX"""

# Standard libraries
import argparse
from functools import cache
import time
from typing import Sequence

# External libraries
from rich import print as rprint

# Internal libraries
from python_aoc.common import format_execution_time


def main(args: Sequence[str]) -> None:
    parsed_args = _parse_args(args)
    input_path: str = parsed_args.input_path
    num_trials: int = parsed_args.num_trials
    verbose_mode: bool = parsed_args.verbose

    lines: Sequence[str] = []
    with open(input_path, "r", encoding="utf-8") as input_file:
        lines = input_file.readlines()

    # Part 1
    result: int | None = None
    total_time: int = 0
    for _ in range(num_trials):
        start_time: int = time.perf_counter_ns()
        result = calculate_num_possibilities_folded(lines, verbose_mode)
        total_time += time.perf_counter_ns() - start_time
    average_time: float = total_time / num_trials
    rprint(f"Part 1 - Result = {result}")
    rprint(f"Part 1 took {format_execution_time(average_time)}")

    # Part 2
    result: int | None = None
    total_time: int = 0
    for _ in range(num_trials):
        start_time: int = time.perf_counter_ns()
        result = calculate_num_possibilities_unfolded(lines, verbose_mode)
        total_time += time.perf_counter_ns() - start_time
    average_time: float = total_time / num_trials
    rprint(f"Part 2 - Result = {result}")
    rprint(f"Part 2 took {format_execution_time(average_time)}")


def calculate_num_possibilities_folded(lines: Sequence[str], verbose_mode: bool) -> int:
    """Calculates the number of possible configurations of broken springs. Solves part 1 of Day 12 of Advent of Code 2023.

    Parameters
    ----------
    lines : Sequence[str]
        The lines of the input file.
    verbose : bool
        Turn on verbose mode.

    Returns
    -------
    int
        The number of possible broken spring configurations.
    """
    result: int = 0
    for line_idx, line in enumerate(lines):
        line = line.rstrip("\n")
        # Get the hints
        tokens = line.split()
        hints_str = tokens[1].split(",")
        hints: Sequence[int] = tuple([int(hint) for hint in hints_str])
        puzzle = tokens[0]
        if verbose_mode:
            rprint(f"Line {line_idx}")
        num_ways = calculate_num_ways_recursion(puzzle, hints)
        if verbose_mode:
            rprint(f"There are {num_ways} possible arrangements")
        result += num_ways
    return result


def calculate_num_possibilities_unfolded(
    lines: Sequence[str], verbose_mode: bool
) -> int:
    """Calculates the number of possible configurations of broken springs after unfolding.
    Solves part 2 of Day 12 of Advent of Code 2023.

    Parameters
    ----------
    lines : Sequence[str]
        The lines of the input file.
    verbose : bool
        Turn on verbose mode.

    Returns
    -------
    int
        The number of possible broken spring configurations.
    """
    result: int = 0
    for line_idx, line in enumerate(lines):
        line = line.rstrip("\n")
        # Get the hints
        tokens = line.split()
        hints_str = tokens[1].split(",")
        hints: Sequence[int] = [int(hint) for hint in hints_str]
        # Unfold the hints
        hints = tuple(5 * hints)
        puzzle = tokens[0]
        # Unfold the puzzle
        puzzle = "?".join(5 * [puzzle])
        if verbose_mode:
            rprint(f"Processing line {line_idx}...")
        num_ways = calculate_num_ways_recursion(puzzle, hints)
        if verbose_mode:
            rprint(f"There are {num_ways} possible arrangements")
        result += num_ways
    return result


@cache
def calculate_num_ways_recursion(
    puzzle: str,
    hints: tuple[int] | tuple[()],
) -> int:
    """Counts the number of possible valid configurations of a nonogram line via recursion.

    Parameters
    ----------
    puzzle : str
        The nonogram line.
    hints : tuple[int] | tuple[()]
        A current set of hints for the nonogram line.
    """
    # Base case: no cells left
    if len(puzzle) == 0:
        # Only solved if there are no more hints left
        return 1 if len(hints) == 0 else 0
    # Base case: no hints left
    elif len(hints) == 0:
        # Only solved if there are no other blocks left in the line
        return 1 if "#" not in puzzle else 0

    count: int = 0
    # . or ? can just be consumed
    if puzzle[0] in ".?":
        count += calculate_num_ways_recursion(puzzle[1:], hints)

    # Just #
    if puzzle[0] in "#?":
        # 1. The current block can fit in the remaining string
        # 2. Are the next hints[0] cells free from `.`?
        # 3. Is the block fenced off from another block or will the block extend to the end
        if (
            hints[0] <= len(puzzle)
            and "." not in puzzle[: hints[0]]
            and (hints[0] == len(puzzle) or puzzle[hints[0]] != "#")
        ):
            # Jump to next non #
            count += calculate_num_ways_recursion(puzzle[hints[0] + 1 :], hints[1:])

    return count


def _parse_args(args: Sequence[str]) -> argparse.Namespace:
    """Parses the command line arguments

    Parameters
    ----------
    args : Sequence[str]
        The command line arguments.

    Returns
    -------
    parsed_args : argparse.Namespace
        The parsed command line arguments.
    """
    parser = argparse.ArgumentParser(
        prog="aoc 2023 day 12",
        description="Program to solve Day 12 of Advent of Code 2023",
    )
    parser.add_argument("input_path", type=str, help="Path to input file")
    parser.add_argument(
        "-num_trials",
        "--num_trials",
        type=int,
        default=100,
        help="Number of trials to use when measuring run time",
    )
    parser.add_argument(
        "-v",
        "--v",
        "-verbose",
        "--verbose",
        action="store_true",
        dest="verbose",
        help="Turn on verbose mode",
    )
    return parser.parse_args(args)


if __name__ == "__main__":
    import sys

    main(sys.argv[1:])
