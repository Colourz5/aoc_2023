"""This module contains the solution to Day 22 of Advent of Code 2023"""

# Standard libraries
import argparse
from copy import deepcopy
import time
from typing import Sequence

# External libraries
from matplotlib import pyplot as plt
import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
from rich import print as rprint

# Internal libraries
from python_aoc.common import format_execution_time


LETTERS: str = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
COLORS: Sequence[str] = ["red", "blue", "green"]


class Stack:
    def __init__(self):
        self._grid: dict[tuple[int, int, int], int] = {}
        self._bricks: dict[int, list[tuple[int, int, int]]] = {}
        self._brick_idx: int = 0
        self._width: int = 0
        self._breadth: int = 0
        self._height: int = 0
        self._above_bricks: dict[int, set[int]] | None = None
        self._below_bricks: dict[int, set[int]] | None = None
        self._counts: dict[int, int] = {}

    def add_brick(self, start: tuple[int, int, int], end: tuple[int, int, int]) -> None:
        x_start = min(start[0], end[0])
        y_start = min(start[1], end[1])
        z_start = min(start[2], end[2])
        x_end = max(start[0], end[0]) + 1
        y_end = max(start[1], end[1]) + 1
        z_end = max(start[2], end[2]) + 1
        points: list[tuple[int, int, int]] = []
        for x in range(x_start, x_end):
            for y in range(y_start, y_end):
                for z in range(z_start, z_end):
                    assert (x, y, z) not in self._grid
                    self._grid[(x, y, z)] = self._brick_idx
                    points.append((x, y, z))
        self._width = max(self._width, x_end)
        self._breadth = max(self._breadth, y_end)
        self._height = max(self._height, z_end)
        self._bricks[self._brick_idx] = points
        self._brick_idx += 1

    def remove_brick(self, brick_idx: int) -> None:
        for x, y, z in self._bricks[brick_idx]:
            del self._grid[(x, y, z)]
        del self._bricks[brick_idx]
        self._above_bricks = None
        self._below_bricks = None
        self._counts = {}
        while self.simulate():
            pass

    def simulate(self) -> bool:
        changed: bool = False
        new_grid: dict[tuple[int, int, int], int] = self._grid.copy()
        for brick_idx, brick_points in self._bricks.items():
            # For each point check the grid cell below it
            can_fall: bool = True
            for x, y, z in brick_points:
                if z - 1 == 0:
                    can_fall = False
                    break
                # If in that means there is a brick there
                if (x, y, z - 1) in self._grid:
                    hit_brick = self._grid[(x, y, z - 1)]
                    if hit_brick == brick_idx:
                        continue
                    else:
                        can_fall = False
                        break
            if can_fall:
                if brick_idx in self._counts:
                    self._counts[brick_idx] += 1
                else:
                    self._counts[brick_idx] = 0
                changed = True
                new_points: list[tuple[int, int, int]] = []
                for x, y, z in brick_points:
                    del new_grid[(x, y, z)]
                    new_grid[(x, y, z - 1)] = brick_idx
                    new_points.append((x, y, z - 1))
                self._bricks[brick_idx] = new_points
        self._grid = new_grid
        return changed

    def get_counts(self) -> dict[int, int]:
        return self._counts

    def reset_counts(self) -> None:
        self._counts = {}

    def construct_dependency_graph(self) -> None:
        # Bricks that you are supporting
        above_bricks: dict[int, set[int]] = {}
        # Bricks that are supporting you
        below_bricks: dict[int, set[int]] = {}

        for brick_idx, brick_points in self._bricks.items():
            # For each grid point check points above it
            supporting: set[int] = set()
            depending: set[int] = set()
            for x, y, z in brick_points:
                if (x, y, z + 1) in self._grid:
                    hit_brick = self._grid[(x, y, z + 1)]
                    if hit_brick != brick_idx:
                        supporting.add(hit_brick)
                if (x, y, z - 1) in self._grid:
                    hit_brick = self._grid[(x, y, z - 1)]
                    if hit_brick != brick_idx:
                        depending.add(hit_brick)
            above_bricks[brick_idx] = supporting
            below_bricks[brick_idx] = depending
        assert len(above_bricks) == len(below_bricks)
        self._above_bricks = above_bricks
        self._below_bricks = below_bricks

    def find_safe_removals(self) -> Sequence[int]:
        if self._above_bricks is None or self._below_bricks is None:
            self.construct_dependency_graph()
        assert self._above_bricks is not None
        assert self._below_bricks is not None
        above_bricks = self._above_bricks
        below_bricks = self._below_bricks
        not_safe_removals: set[int] = set()
        for brick_idx in above_bricks:
            depending = list(below_bricks[brick_idx])
            if len(depending) == 1:
                not_safe_removals.add(depending[0])
        safe_removals = [
            brick_idx
            for brick_idx in range(len(self._bricks))
            if brick_idx not in not_safe_removals
        ]

        return safe_removals

    def draw_xz(self) -> None:
        for z in range(self._height)[::-1]:
            rprint(f"[bold white]{str(z)[-1]}[/bold white]", end="|")
            for x in range(self._width):
                if z == 0:
                    cell = "-"
                    style = "on cyan"
                else:
                    brick_idx: int | None = None
                    for y in range(self._breadth):
                        if (x, y, z) in self._grid:
                            brick_idx = self._grid[(x, y, z)]
                            break
                    if brick_idx is None:
                        cell = " "
                        style = "on black"
                    else:
                        cell = LETTERS[brick_idx % len(LETTERS)]
                        style = f"on {COLORS[brick_idx % len(COLORS)]}"
                rprint(f"[{style}]{cell}[/{style}]", end="|")
            rprint()
        rprint("[bold yellow]x[/bold yellow]", end="|")
        for x in range(self._width):
            rprint(f"[bold white]{str(x)[-1]}[/bold white]", end="|")
        rprint()

    def draw_yz(self) -> None:
        for z in range(self._height)[::-1]:
            rprint(f"[bold white]{str(z)[-1]}[/bold white]", end="|")
            for y in range(self._breadth):
                if z == 0:
                    cell = "-"
                    style = "on cyan"
                else:
                    brick_idx: int | None = None
                    for x in range(self._width):
                        if (x, y, z) in self._grid:
                            brick_idx = self._grid[(x, y, z)]
                            break
                    if brick_idx is None:
                        cell = " "
                        style = "on black"
                    else:
                        cell = LETTERS[brick_idx % len(LETTERS)]
                        style = f"on {COLORS[brick_idx % len(COLORS)]}"
                rprint(f"[{style}]{cell}[/{style}]", end="|")
            rprint()
        rprint("[bold yellow]y[/bold yellow]", end="|")
        for y in range(self._breadth):
            rprint(f"[bold white]{str(y)[-1]}[/bold white]", end="|")
        rprint()

    def draw_plot(self) -> None:
        fig = plt.figure()
        ax = fig.add_subplot(111, projection="3d")
        ax.set_xlabel("x")
        ax.set_ylabel("y")
        ax.set_zlabel("z")
        cmap = mpl.colormaps["tab20c"]
        data = np.zeros((self._width, self._breadth, self._height), dtype=bool)
        colors = np.zeros((self._width, self._breadth, self._height, 4))
        for brick_idx, brick_points in self._bricks.items():
            color = cmap(brick_idx / (len(self._bricks) - 1))
            for x, y, z in brick_points:
                data[x][y][z] = True
                colors[x][y][z][:] = color

        ax.voxels(data, facecolors=colors, edgecolors="black")
        plt.show()
        plt.close(fig)


def main(args: Sequence[str]) -> None:
    parsed_args = _parse_args(args)
    input_path: str = parsed_args.input_path
    num_trials: int = parsed_args.num_trials
    verbose_mode: bool = parsed_args.verbose

    lines: Sequence[str] = []
    with open(input_path, "r", encoding="utf-8") as input_file:
        lines = input_file.readlines()

    # Part 1
    result: int | None = None
    total_time: int = 0
    for _ in range(num_trials):
        start_time: int = time.perf_counter_ns()
        result = part1(lines, verbose_mode)
        total_time += time.perf_counter_ns() - start_time
    average_time: float = total_time / num_trials
    rprint(f"Part 1 - Result = {result}")
    rprint(f"Part 1 took {format_execution_time(average_time)}")

    # Part 2
    result: int | None = None
    total_time: int = 0
    for _ in range(num_trials):
        start_time: int = time.perf_counter_ns()
        result = part2(lines, verbose_mode)
        total_time += time.perf_counter_ns() - start_time
    average_time: float = total_time / num_trials
    rprint(f"Part 2 - Result = {result}")
    rprint(f"Part 2 took {format_execution_time(average_time)}")


def part1(lines: Sequence[str], verbose_mode: bool) -> int:
    """Part 1 of Day XX of Advent of Code XXXX

    Parameters
    ----------
    lines : Sequence[str]
        The lines of the input file.
    verbose : bool
        Turn on verbose mode.

    Returns
    -------
    int
        The answer
    """
    world = Stack()
    for line in lines:
        line = line.rstrip("\n")
        tokens = line.split("~")
        start = tuple([int(num) for num in tokens[0].split(",")])
        end = tuple([int(num) for num in tokens[1].split(",")])
        world.add_brick((start[0], start[1], start[2]), (end[0], end[1], end[2]))
    # 3D grid
    if verbose_mode:
        world.draw_plot()
    while world.simulate():
        pass
    if verbose_mode:
        world.draw_plot()
    safe_removals = world.find_safe_removals()
    return len(safe_removals)


def part2(lines: Sequence[str], verbose_mode: bool) -> int:
    """Part 2 of Day XX of Advent of Code XXXX

    Parameters
    ----------
    lines : Sequence[str]
        The lines of the input file.
    verbose_mode : bool
        Turn on verbose mode.

    Returns
    -------
    int
        The answer
    """
    world = Stack()
    for line in lines:
        line = line.rstrip("\n")
        tokens = line.split("~")
        start = tuple([int(num) for num in tokens[0].split(",")])
        end = tuple([int(num) for num in tokens[1].split(",")])
        world.add_brick((start[0], start[1], start[2]), (end[0], end[1], end[2]))
    # 3D grid
    if verbose_mode:
        world.draw_plot()
    while world.simulate():
        pass
    total: int = 0
    for brick_idx in world._bricks:
        rprint(f"Removing brick {brick_idx:04}", end="\r")
        new_world = deepcopy(world)
        new_world.remove_brick(brick_idx)
        total += len(new_world.get_counts())
    rprint()
    if verbose_mode:
        world.draw_plot()
    return total


def _parse_args(args: Sequence[str]) -> argparse.Namespace:
    """Parses the command line arguments

    Parameters
    ----------
    args : Sequence[str]
        The command line arguments.

    Returns
    -------
    parsed_args : argparse.Namespace
        The parsed command line arguments.
    """
    parser = argparse.ArgumentParser(
        prog="aoc XXXX day XX",
        description="Program to solve Day XX of Advent of Code XXXX",
    )
    parser.add_argument("input_path", type=str, help="Path to input file")
    parser.add_argument(
        "-num_trials",
        "--num_trials",
        type=int,
        default=100,
        help="Number of trials to use when measuring run time",
    )
    parser.add_argument(
        "-v",
        "--v",
        "-verbose",
        "--verbose",
        action="store_true",
        dest="verbose",
        help="Turn on verbose mode",
    )
    return parser.parse_args(args)


if __name__ == "__main__":
    import sys

    main(sys.argv[1:])
