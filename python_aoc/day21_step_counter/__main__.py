"""This module contains the solution to Day 21 of Advent of Code 2023"""

# Standard libraries
import argparse
import math
import os
import time
from typing import Sequence

# External libraries
from rich import print as rprint

# Internal libraries
from python_aoc.common import format_execution_time

type Landmarks = dict[tuple[int, int], int]


def main(args: Sequence[str]) -> None:
    parsed_args = _parse_args(args)
    input_path: str = parsed_args.input_path
    num_trials: int = parsed_args.num_trials
    verbose_mode: bool = parsed_args.verbose

    lines: Sequence[str] = []
    with open(input_path, "r", encoding="utf-8") as input_file:
        lines = input_file.readlines()

    # Part 1
    result: int | None = None
    total_time: int = 0
    for _ in range(num_trials):
        start_time: int = time.perf_counter_ns()
        result = part1(lines, verbose_mode)
        total_time += time.perf_counter_ns() - start_time
    average_time: float = total_time / num_trials
    rprint(f"Part 1 - Result = {result}")
    rprint(f"Part 1 took {format_execution_time(average_time)}")

    # Part 2
    result: int | None = None
    total_time: int = 0
    for _ in range(num_trials):
        start_time: int = time.perf_counter_ns()
        result = part2(lines, verbose_mode)
        total_time += time.perf_counter_ns() - start_time
    average_time: float = total_time / num_trials
    rprint(f"Part 2 - Result = {result}")
    rprint(f"Part 2 took {format_execution_time(average_time)}")


def part1(lines: Sequence[str], verbose_mode: bool) -> int:
    """Part 1 of Day XX of Advent of Code XXXX

    Parameters
    ----------
    lines : Sequence[str]
        The lines of the input file.
    verbose : bool
        Turn on verbose mode.

    Returns
    -------
    int
        The answer
    """
    grid: Sequence[Sequence[str]] = []
    start_pos: tuple[int, int] | None = None
    for row_idx, line in enumerate(lines):
        line = line.rstrip("\n")
        if "S" in line:
            column_idx = line.index("S")
            start_pos = (row_idx, column_idx)
        grid.append(list(line))
    assert len(grid) > 0
    width: int = len(grid[0])
    height: int = len(grid)
    start_pos: tuple[int, int] | None = None
    for row_idx, row in enumerate(grid):
        found: bool = False
        for column_idx, cell in enumerate(row):
            if cell == "S":
                start_pos = (row_idx, column_idx)
                found = True
                break
        if found:
            break
    assert start_pos is not None
    total_area: int = width * height
    max_step = 6 if total_area < 200 else 64
    if verbose_mode:
        print_grid(grid, [])
    final_positions = depth_first_search(grid, max_step, start_pos)
    if verbose_mode:
        print_grid(grid, final_positions)
    return len(final_positions)


def part2(lines: Sequence[str], verbose_mode: bool) -> int:
    """Part 2 of Day XX of Advent of Code XXXX

    Parameters
    ----------
    lines : Sequence[str]
        The lines of the input file.
    verbose_mode : bool
        Turn on verbose mode.

    Returns
    -------
    int
        The answer
    """
    grid: Sequence[Sequence[str]] = []
    for line in lines:
        line = line.rstrip("\n")
        grid.append(list(line))
    assert len(grid) > 0
    width: int = len(grid[0])
    height: int = len(grid)
    half_width: int = (width - 1) // 2
    total_area: int = width * height
    factor = 10
    factor = 202300
    max_steps = 50 if total_area < 200 else width * factor + half_width
    if verbose_mode:
        print_grid(grid, [])
    final_positions = depth_first_search_periodic(grid, max_steps)
    if verbose_mode:
        print_grid(grid, final_positions)
    return len(final_positions)


def depth_first_search(
    grid: Sequence[Sequence[str]], max_steps: int, start_pos: tuple[int, int]
) -> Sequence[tuple[int, int]]:
    assert len(grid) > 0
    width: int = len(grid[0])
    height: int = len(grid)
    CARDINAL_DIRECTIONS: tuple[
        tuple[int, int], tuple[int, int], tuple[int, int], tuple[int, int]
    ] = ((-1, 0), (1, 0), (0, -1), (0, 1))
    work_queue: list[tuple[int, int, int]] = [(start_pos[0], start_pos[1], 0)]
    seen: set[tuple[int, int]] = set()
    final_positions: set[tuple[int, int]] = set()
    while len(work_queue) > 0:
        row_idx, column_idx, steps = work_queue.pop(0)
        if (row_idx, column_idx) in seen:
            continue
        seen.add((row_idx, column_idx))
        # Calculate if it is possible
        leftover_steps = max_steps - steps
        if leftover_steps % 2 == 0:
            final_positions.add((row_idx, column_idx))
        next_steps = steps + 1
        if next_steps > max_steps:
            continue
        for row_offset, column_offset in CARDINAL_DIRECTIONS:
            target_row = row_idx + row_offset
            target_column = column_idx + column_offset
            if (
                target_row < 0
                or target_row >= height
                or target_column < 0
                or target_column >= width
            ):
                continue
            target_cell = grid[target_row][target_column]
            if target_cell == "#":
                continue
            # Add node
            work_queue.append((target_row, target_column, next_steps))
    return list(final_positions)


def depth_first_search_periodic(
    grid: Sequence[Sequence[str]], max_steps: int
) -> Sequence[tuple[int, int]]:
    assert len(grid) > 0
    width: int = len(grid[0])
    height: int = len(grid)
    rprint(f"Width = {width}, height = {height}")
    grid_width = max_steps // width - 1
    num_odd_grids = (2 * (grid_width // 2) + 1) ** 2
    rprint(f"There are {num_odd_grids} odd grids")
    num_even_grids = ((grid_width + 1) // 2 * 2) ** 2
    rprint(f"There are {num_even_grids} even grids")
    CARDINAL_DIRECTIONS: tuple[
        tuple[int, int], tuple[int, int], tuple[int, int], tuple[int, int]
    ] = ((-1, 0), (1, 0), (0, -1), (0, 1))
    start_pos: tuple[int, int] | None = None
    for row_idx, row in enumerate(grid):
        found: bool = False
        for column_idx, cell in enumerate(row):
            if cell == "S":
                start_pos = (row_idx, column_idx)
                found = True
                break
        if found:
            break
    assert start_pos is not None
    work_queue: list[tuple[int, int, int]] = [(start_pos[0], start_pos[1], 0)]
    seen: set[tuple[int, int]] = set()
    final_positions: set[tuple[int, int]] = set()
    counts: dict[tuple[int, int], int] = {}

    # while len(work_queue) > 0:
    while False:
        row_idx, column_idx, steps = work_queue.pop(0)
        if (row_idx, column_idx) in seen:
            continue
        seen.add((row_idx, column_idx))
        # Calculate if it is possible
        leftover_steps = max_steps - steps
        if leftover_steps % 2 == 0:
            # Dimension number
            row_dimension = row_idx // height
            column_dimension = column_idx // width
            if (row_dimension, column_dimension) in counts:
                counts[(row_dimension, column_dimension)] += 1
            else:
                counts[(row_dimension, column_dimension)] = 1
            final_positions.add((row_idx, column_idx))
        next_steps = steps + 1
        if next_steps > max_steps:
            continue
        for row_offset, column_offset in CARDINAL_DIRECTIONS:
            target_row = row_idx + row_offset
            target_column = column_idx + column_offset
            target_cell = grid[target_row % height][target_column % width]
            if target_cell == "#":
                continue
            # Add node
            work_queue.append((target_row, target_column, next_steps))
    from matplotlib import pyplot as plt

    num_final_cells_odd = len(
        depth_first_search(grid, 2 * (width + height) + 1, start_pos)
    )
    num_final_cells_even = len(
        depth_first_search(grid, 2 * (width + height), start_pos)
    )
    traversal = grid_width * width + (width - 1) // 2 + 1
    leftover_steps = max_steps - traversal
    corner_traversal = (grid_width + 1) * width + 1
    corner_leftover = max_steps - corner_traversal
    normal_corner_traversal = grid_width * width + 1
    normal_corner_leftover = max_steps - normal_corner_traversal
    num_final_cells_rightmost = len(
        depth_first_search(grid, leftover_steps, ((height - 1) // 2, 0))
    )
    num_final_cells_leftmost = len(
        depth_first_search(grid, leftover_steps, ((height - 1) // 2, width - 1))
    )
    num_final_cells_topmost = len(
        depth_first_search(grid, leftover_steps, (height - 1, (width - 1) // 2))
    )
    num_final_cells_bottommost = len(
        depth_first_search(grid, leftover_steps, (0, (width - 1) // 2))
    )
    num_extremal_corners_topleft = len(
        depth_first_search(grid, corner_leftover, (0, 0))
    )
    num_extremal_corners_topright = len(
        depth_first_search(grid, corner_leftover, (0, width - 1))
    )
    num_extremal_corners_bottomleft = len(
        depth_first_search(grid, corner_leftover, (height - 1, 0))
    )
    num_extremal_corners_bottomright = len(
        depth_first_search(grid, corner_leftover, (height - 1, width - 1))
    )
    num_normal_corners_bottomleft = len(
        depth_first_search(grid, normal_corner_leftover, (height - 1, 0))
    )
    num_normal_corners_bottomright = len(
        depth_first_search(grid, normal_corner_leftover, (height - 1, width - 1))
    )
    num_normal_corners_topright = len(
        depth_first_search(grid, normal_corner_leftover, (0, width - 1))
    )
    num_normal_corners_topleft = len(
        depth_first_search(grid, normal_corner_leftover, (0, 0))
    )
    num_extremal_grids = grid_width + 1

    rprint(f"Leftover steps in extremal cardinal grids {leftover_steps}")
    rprint(f"Leftover steps in extremal corner grids {corner_leftover}")
    rprint(f"Width = {width}, height = {height}")
    rprint(f"nGrid width = {grid_width}")
    rprint(f"Each odd grid has {num_final_cells_odd} landable cells")
    rprint(f"Each even grid has {num_final_cells_even} landable cells")
    rprint(f"Rightmost grid has {num_final_cells_rightmost} landable cells")
    rprint(f"Leftmost grid has {num_final_cells_leftmost} landable cells")
    rprint(f"Topmost grid has {num_final_cells_topmost} landable cells")
    rprint(f"Bottommost grid has {num_final_cells_bottommost} landable cells")
    rprint(
        f"Extremal corners grid in the top right have {num_extremal_corners_bottomleft} landable cells"
    )
    rprint(
        f"Extremal corners grid in the top left have {num_extremal_corners_bottomright} landable cells"
    )
    rprint(
        f"Extremal corners grid in the bottom left have {num_extremal_corners_topright} landable cells"
    )
    rprint(
        f"Extremal corners grid in the bottom right have {num_extremal_corners_topleft} landable cells"
    )
    rprint(f"There are 4x{num_extremal_grids} corner extremal grids")
    rprint(
        f"Normal corner grid in the top right have {num_normal_corners_bottomleft} landable cells"
    )
    rprint(
        f"Normal corner grid in the top left have {num_normal_corners_bottomright} landable cells"
    )
    rprint(
        f"Normal corner grid in the bottom left have {num_normal_corners_topright} landable cells"
    )
    rprint(
        f"Normal corner grid in the bottom right have {num_normal_corners_topleft} landable cells"
    )
    inner_cells = (
        num_final_cells_odd * num_odd_grids + num_final_cells_even * num_even_grids
    )
    current_total = (
        inner_cells
        + num_final_cells_rightmost
        + num_final_cells_leftmost
        + num_final_cells_topmost
        + num_final_cells_bottommost
        + num_extremal_grids
        * (
            num_extremal_corners_topright
            + num_extremal_corners_topleft
            + num_extremal_corners_bottomright
            + num_extremal_corners_bottomleft
        )
        + grid_width
        * (
            num_normal_corners_topright
            + num_normal_corners_topleft
            + num_normal_corners_bottomright
            + num_normal_corners_bottomleft
        )
    )
    rprint(f"There are {inner_cells} cells inside")
    rprint(f"There are {current_total} cells in total")
    fig = plt.figure()
    ax = fig.add_subplot(111)
    max_column_dimension = max_row_dimension = grid_width + 1
    count_grid = [
        (2 * max_column_dimension + 1) * [0] for _ in range(2 * max_row_dimension + 1)
    ]
    for i in range(2 * max_row_dimension + 1):
        for j in range(2 * max_column_dimension + 1):
            actual_row = i - max_row_dimension
            actual_column = j - max_column_dimension
            if (actual_row, actual_column) in counts:
                count = counts[(actual_row, actual_column)]
                ax.text(
                    j,
                    i,
                    str(count),
                    color="red" if (i + j) % 2 == 0 else "blue",
                    ha="center",
                    va="center",
                )
                count_grid[i][j] = count
    ax.imshow(count_grid)
    plt.show()
    plt.close(fig)
    return list(final_positions)


def print_grid(
    grid: Sequence[Sequence[str]], final_positions: Sequence[tuple[int, int]]
) -> None:
    width: int = len(grid[0])
    height: int = len(grid)
    row_size, column_size = os.get_terminal_size()
    width = min(width, column_size)
    height = min(height, row_size)
    rprint(" ", end="|")
    for column_idx in range(width):
        rprint(f"[bold white]{str(column_idx)[-1]}[/bold white]", end="|")
    rprint()

    for row_idx in range(height):
        rprint(f"[bold white]{str(row_idx)[-1]}[/bold white]", end="|")
        for column_idx in range(width):
            cell = grid[row_idx][column_idx]
            match cell:
                case ".":
                    cell = " "
                    style = "on black"
                case "#":
                    cell = " "
                    style = "on red"
                case "S":
                    style = "on cyan"
                case _:
                    style = "on magenta"
            if (row_idx, column_idx) in final_positions:
                cell = "X"
                style = "on green"
            rprint(f"[{style}]{cell}[/{style}]", end="|")
        rprint()
    rprint()


def _parse_args(args: Sequence[str]) -> argparse.Namespace:
    """Parses the command line arguments

    Parameters
    ----------
    args : Sequence[str]
        The command line arguments.

    Returns
    -------
    parsed_args : argparse.Namespace
        The parsed command line arguments.
    """
    parser = argparse.ArgumentParser(
        prog="aoc XXXX day XX",
        description="Program to solve Day XX of Advent of Code XXXX",
    )
    parser.add_argument("input_path", type=str, help="Path to input file")
    parser.add_argument(
        "-num_trials",
        "--num_trials",
        type=int,
        default=100,
        help="Number of trials to use when measuring run time",
    )
    parser.add_argument(
        "-v",
        "--v",
        "-verbose",
        "--verbose",
        action="store_true",
        dest="verbose",
        help="Turn on verbose mode",
    )
    return parser.parse_args(args)


if __name__ == "__main__":
    import sys

    main(sys.argv[1:])
