"""This module contains the solution to Day X of Advent of Code XXXX"""

# Standard libraries
import argparse
import time
from typing import Sequence

# External libraries
from rich import print as rprint

# Internal libraries
from python_aoc.common import format_execution_time


def main(args: Sequence[str]) -> None:
    parsed_args = _parse_args(args)
    input_path: str = parsed_args.input_path
    num_trials: int = parsed_args.num_trials
    verbose_mode: bool = parsed_args.verbose

    lines: Sequence[str] = []
    with open(input_path, "r", encoding="utf-8") as input_file:
        lines = input_file.readlines()

    # Part 1
    result: int | None = None
    total_time: int = 0
    for _ in range(num_trials):
        start_time: int = time.perf_counter_ns()
        result = get_reflection_summary(lines, 0, verbose_mode)
        total_time += time.perf_counter_ns() - start_time
    average_time: float = total_time / num_trials
    rprint(f"Part 1 - Result = {result}")
    rprint(f"Part 1 took {format_execution_time(average_time)}")

    # Part 2
    result: int | None = None
    total_time: int = 0
    for _ in range(num_trials):
        start_time: int = time.perf_counter_ns()
        result = get_reflection_summary(lines, 1, verbose_mode)
        total_time += time.perf_counter_ns() - start_time
    average_time: float = total_time / num_trials
    rprint(f"Part 2 - Result = {result}")
    rprint(f"Part 2 took {format_execution_time(average_time)}")


def get_reflection_summary(
    lines: Sequence[str], smudge_threshold: int, verbose_mode: bool
) -> int:
    """Part 1 of Day XX of Advent of Code XXXX

    Parameters
    ----------
    lines : Sequence[str]
        The lines of the input file.
    verbose : bool
        Turn on verbose mode.

    Returns
    -------
    int
        The answer
    """
    patterns: list[Sequence[str]] = []
    current_pattern: list[str] = []
    for line in lines:
        line = line.rstrip("\n")
        if len(line) == 0:
            patterns.append(current_pattern)
            current_pattern = []
            continue
        current_pattern.append(line)
    if len(current_pattern) > 0:
        patterns.append(current_pattern)
    count: int = 0
    for pattern_idx, pattern in enumerate(patterns):
        # verbose_mode = pattern_idx == 94
        if verbose_mode:
            rprint(f"Pattern {pattern_idx}")
            print_pattern(pattern)
        vertical_split = find_vertical_reflection_point(
            pattern, smudge_threshold, verbose_mode
        )
        if vertical_split is not None:
            count += vertical_split + 1
            if verbose_mode:
                rprint(
                    f"Pattern {pattern_idx} is reflected along columns {vertical_split} and {vertical_split + 1}"
                )
            continue
        horizontal_split = find_horizontal_reflection_point(
            pattern, smudge_threshold, verbose_mode
        )
        if horizontal_split is not None:
            count += 100 * (horizontal_split + 1)
            if verbose_mode:
                rprint(
                    f"Pattern {pattern_idx} is reflected along rows {horizontal_split} and {horizontal_split + 1}"
                )
            continue
        rprint(f"Pattern {pattern_idx} is problematic!")
        print_pattern(pattern)
    return count


def find_vertical_reflection_point(
    pattern: Sequence[str], smudge_threshold: int, verbose_mode: bool
) -> int | None:
    width: int = len(pattern[0])
    for column_idx in range(width - 1):
        left_slice_size = column_idx + 1
        right_slice_size = width - column_idx - 1
        if verbose_mode:
            rprint(f"Reflection line between columns {column_idx} and {column_idx+1}")
        smudges: int = 0
        for _, row in enumerate(pattern):
            difference = left_slice_size - right_slice_size
            # Case: Left slice is larger
            if difference > 0:
                left_slice = row[: column_idx + 1][difference:]
                right_slice = row[column_idx + 1 :][::-1]
            # Case: Right slice is larger
            elif difference < 0:
                difference = abs(difference)
                left_slice = row[: column_idx + 1]
                right_slice = row[column_idx + 1 :][::-1][difference:]
            # Case: Same length
            else:
                left_slice = row[: column_idx + 1]
                right_slice = row[column_idx + 1 :][::-1]
            if verbose_mode:
                rprint(f"Left {left_slice} vs right {right_slice}")
            for left_char, right_char in zip(left_slice, right_slice):
                smudges += 1 if left_char != right_char else 0
        if smudges == smudge_threshold:
            return column_idx
    return None


def find_horizontal_reflection_point(
    pattern: Sequence[str], smudge_threshold: int, verbose_mode: bool
) -> int | None:
    height: int = len(pattern)
    width: int = len(pattern[0])
    for row_idx in range(height - 1):
        top_slice_size = row_idx + 1
        bottom_slice_size = height - row_idx - 1
        if verbose_mode:
            rprint(f"Reflection line between rows {row_idx} and {row_idx+1}")
        smudges: int = 0
        for column_idx in range(width):
            top_slice = ""
            bottom_slice = ""
            for top_idx in range(row_idx + 1):
                top_slice += pattern[top_idx][column_idx]
            for bottom_idx in range(height - 1, row_idx, -1):
                bottom_slice += pattern[bottom_idx][column_idx]
            # Reverse bottom slice
            difference = top_slice_size - bottom_slice_size
            # Case top slice is larger than bottom slice
            if difference > 0:
                # Truncate the top slice by shifting the beginning
                top_slice = top_slice[difference:]
            # Case bottom slice is larger than top slice
            elif difference < 0:
                # Truncate the bottom slice by shifting the beginning
                bottom_slice = bottom_slice[-difference:]
            # Case: They are the same length
            else:
                # Do nothing
                pass
            if verbose_mode:
                rprint(f"Top {top_slice} vs bottom {bottom_slice}")
            for top_char, bottom_char in zip(top_slice, bottom_slice):
                smudges += 1 if top_char != bottom_char else 0

        if smudges == smudge_threshold:
            return row_idx
    return None


def print_pattern(pattern: Sequence[str]) -> None:
    width: int = len(pattern[0])
    rprint("  ", end="")
    for column_idx in range(width):
        rprint(f"[bold white]{str(column_idx)[-1]}[/bold white]", end="")
    rprint()
    for row_idx, line in enumerate(pattern):
        rprint(f"[bold white]{str(row_idx)[-1]}[/bold white]", end=" ")
        for character in line:
            match character:
                case "#":
                    character = " "
                    style = "on red"
                case ".":
                    character = " "
                    style = "on black"
                case _:
                    character = "?"
                    style = "on magenta"
            rprint(f"[{style}]{character}[/{style}]", end="")
        rprint()
    rprint()


def _parse_args(args: Sequence[str]) -> argparse.Namespace:
    """Parses the command line arguments

    Parameters
    ----------
    args : Sequence[str]
        The command line arguments.

    Returns
    -------
    parsed_args : argparse.Namespace
        The parsed command line arguments.
    """
    parser = argparse.ArgumentParser(
        prog="aoc XXXX day XX",
        description="Program to solve Day XX of Advent of Code XXXX",
    )
    parser.add_argument("input_path", type=str, help="Path to input file")
    parser.add_argument(
        "-num_trials",
        "--num_trials",
        type=int,
        default=100,
        help="Number of trials to use when measuring run time",
    )
    parser.add_argument(
        "-v",
        "--v",
        "-verbose",
        "--verbose",
        action="store_true",
        dest="verbose",
        help="Turn on verbose mode",
    )
    return parser.parse_args(args)


if __name__ == "__main__":
    import sys

    main(sys.argv[1:])
