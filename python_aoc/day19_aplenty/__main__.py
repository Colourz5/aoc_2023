"""This module contains the solution to Day 19 of Advent of Code 2023"""

# Standard libraries
import argparse
import math
import time
from typing import Sequence

# External libraries
from rich import print as rprint

# Internal libraries
from python_aoc.common import format_execution_time


def main(args: Sequence[str]) -> None:
    parsed_args = _parse_args(args)
    input_path: str = parsed_args.input_path
    num_trials: int = parsed_args.num_trials
    verbose_mode: bool = parsed_args.verbose

    lines: Sequence[str] = []
    with open(input_path, "r", encoding="utf-8") as input_file:
        lines = input_file.readlines()

    # Part 1
    result: int | None = None
    total_time: int = 0
    for _ in range(num_trials):
        start_time: int = time.perf_counter_ns()
        result = calculate_part_rating(lines, verbose_mode)
        total_time += time.perf_counter_ns() - start_time
    average_time: float = total_time / num_trials
    rprint(f"Part 1 - Result = {result}")
    rprint(f"Part 1 took {format_execution_time(average_time)}")

    # Part 2
    result: int | None = None
    total_time: int = 0
    for _ in range(num_trials):
        start_time: int = time.perf_counter_ns()
        result = count_possible_combinations(lines, verbose_mode)
        total_time += time.perf_counter_ns() - start_time
    average_time: float = total_time / num_trials
    rprint(f"Part 2 - Result = {result}")
    rprint(f"Part 2 took {format_execution_time(average_time)}")


def calculate_part_rating(lines: Sequence[str], verbose_mode: bool) -> int:
    """Calculates the rating of all accepted parts.
    Solves part 1 of Day 19 of Advent of Code 2023.

    Parameters
    ----------
    lines : Sequence[str]
        The lines of the input file.
    verbose : bool
        Turn on verbose mode.

    Returns
    -------
    int
        The answer
    """
    # Parsing
    workflows: dict[str, tuple[list[str], str]] = {}
    parts: list[dict[str, int]] = []
    rule_state: bool = True
    for line in lines:
        line = line.rstrip("\n")
        if len(line) == 0:
            rule_state = False
            continue
        if rule_state:
            brace_idx = line.index("{")
            rule_name = line[:brace_idx]
            content = line[brace_idx + 1 : -1]
            conditions = content.split(",")
            workflows[rule_name] = (conditions[:-1], conditions[-1])
        else:
            line = line.strip("{}")
            tokens = line.split(",")
            tokens = [token.split("=") for token in tokens]
            current_parts = {}
            for part_type, value_str in tokens:
                current_parts[part_type] = int(value_str)
            parts.append(current_parts)

    total_rating: int = 0
    for part_idx, part in enumerate(parts):
        if verbose_mode:
            rprint(f"Processing part {part_idx}")
        workflow = workflows["in"]
        while True:
            conditions, fallback = workflow
            for condition in conditions:
                # Parse condition
                part_type = condition[0]
                comparison_operator = condition[1]
                # Parse comparison value
                delimiter_idx = condition.index(":")
                comparison_value = int(condition[2:delimiter_idx])
                # Next workflow
                next_workflow = condition[delimiter_idx + 1 :]
                # Value of the part
                value = part[part_type]
                match comparison_operator:
                    case "<":
                        # Pass condition
                        if value < comparison_value:
                            fallback = next_workflow
                            break
                    case ">":
                        # Pass condition
                        if value > comparison_value:
                            fallback = next_workflow
                            break
            # Failed all other checks
            match fallback:
                # Rejected
                case "R":
                    break
                # Accepted
                case "A":
                    total_rating += sum(part.values())
                    break
                # Jump to next workflow
                case _:
                    workflow = workflows[fallback]

    return total_rating


def count_possible_combinations(lines: Sequence[str], verbose_mode: bool) -> int:
    """Counts all possible combinations of part ratings that are accepted.
    Solves part 2 of Day 19 of Advent of Code 2023.

    Parameters
    ----------
    lines : Sequence[str]
        The lines of the input file.
    verbose : bool
        Turn on verbose mode.

    Returns
    -------
    int
        The answer
    """
    workflows: dict[str, tuple[list[str], str]] = {}
    for line in lines:
        line = line.rstrip("\n")
        if len(line) == 0:
            break
        brace_idx = line.index("{")
        rule_name = line[:brace_idx]
        content = line[brace_idx + 1 : -1]
        conditions = content.split(",")
        workflows[rule_name] = (conditions[:-1], conditions[-1])
    # List to store the ranges we are processing
    # Each item has a dict of the range for each part type
    # and also the next workflow it goes to
    possible_ranges: list[tuple[dict[str, range], str]] = [
        (
            {
                "x": range(1, 4001),
                "m": range(1, 4001),
                "a": range(1, 4001),
                "s": range(1, 4001),
            },
            "in",
        )
    ]
    total: int = 0
    step_idx: int = 0
    while len(possible_ranges) > 0:
        step_idx += 1
        if verbose_mode:
            rprint(f"Step {step_idx}")
        # Store the ranges after a single processing step
        next_ranges: list[tuple[dict[str, range], str]] = []
        for current_parts, next_workflow in possible_ranges:
            # Accepted
            if next_workflow == "A":
                # Calculate the number of allowed combinations
                accepted_combinations = math.prod(
                    [len(current_range) for current_range in current_parts.values()]
                )
                total += accepted_combinations
                if verbose_mode:
                    rprint(
                        f"The ranges {current_parts} are accepted, adding {accepted_combinations} total combinations"
                    )
                continue
            # Rejected
            elif next_workflow == "R":
                continue
            # Get the next work flow
            conditions, fallback = workflows[next_workflow]
            for condition in conditions:
                # Parse the condition
                part_type = condition[0]
                comparison_operator = condition[1]
                # Parse the comparison value
                delimiter_idx = condition.index(":")
                comparison_value = int(condition[2:delimiter_idx])
                # The next workflow if success
                destination = condition[delimiter_idx + 1 :]
                # The range for the requested part type
                part_range = current_parts[part_type]
                # The min and max value of the range
                min_value = part_range.start
                max_value = part_range.stop - 1
                match comparison_operator:
                    case "<":
                        # Whole range is less than
                        if max_value < comparison_value:
                            next_ranges.append((current_parts, destination))
                        # Split the range
                        else:
                            # Left hand side
                            left_range = range(min_value, comparison_value)
                            # Right hand side
                            right_range = range(comparison_value, max_value + 1)
                            # Create new parts and assign new ranges
                            left_ranges = current_parts.copy()
                            left_ranges[part_type] = left_range
                            right_ranges = current_parts.copy()
                            right_ranges[part_type] = right_range
                            # Move to next stage
                            next_ranges.append((left_ranges, destination))
                            # Failed range continues to next condition
                            current_parts[part_type] = right_range
                    case ">":
                        # Whole range is greater than
                        if min_value > comparison_value:
                            next_ranges.append((current_parts, destination))
                        # Split the range
                        else:
                            # Left hand side
                            left_range = range(min_value, comparison_value + 1)
                            # Right hand side
                            right_range = range(comparison_value + 1, max_value + 1)
                            # Create new parts and assign new ranges
                            left_ranges = current_parts.copy()
                            left_ranges[part_type] = left_range
                            right_ranges = current_parts.copy()
                            right_ranges[part_type] = right_range
                            # Failed range continues to next condition
                            current_parts[part_type] = left_range
                            # Move to next stage
                            next_ranges.append((right_ranges, destination))
            # Range that has failed every condition
            next_ranges.append((current_parts, fallback))
        # Update the possible ranges
        possible_ranges = next_ranges

    return total


def _parse_args(args: Sequence[str]) -> argparse.Namespace:
    """Parses the command line arguments

    Parameters
    ----------
    args : Sequence[str]
        The command line arguments.

    Returns
    -------
    parsed_args : argparse.Namespace
        The parsed command line arguments.
    """
    parser = argparse.ArgumentParser(
        prog="aoc 2023 day 19",
        description="Program to solve Day 19 of Advent of Code 2023",
    )
    parser.add_argument("input_path", type=str, help="Path to input file")
    parser.add_argument(
        "-num_trials",
        "--num_trials",
        type=int,
        default=100,
        help="Number of trials to use when measuring run time",
    )
    parser.add_argument(
        "-v",
        "--v",
        "-verbose",
        "--verbose",
        action="store_true",
        dest="verbose",
        help="Turn on verbose mode",
    )
    return parser.parse_args(args)


if __name__ == "__main__":
    import sys

    main(sys.argv[1:])
