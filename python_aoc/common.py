"""Common functions used by the other modules."""


def format_execution_time(time_taken: float) -> str:
    """Formats the execution time into a human readable string.

    Parameters
    ----------
    time_taken : float
        The time taken in seconds.

    Returns
    -------
    str
        The formatted string.
    """
    seconds = time_taken // 1_000_000_000
    milliseconds = (time_taken - seconds * 1_000_000_000) // 1_000_000
    microseconds = (
        time_taken - seconds * 1_000_000_000 - milliseconds * 1_000_000
    ) // 1_000
    nanoseconds = (
        time_taken
        - seconds * 1_000_000_000
        - milliseconds * 1_000_000
        - microseconds * 1_000
    )
    return f"{seconds:0.0f} seconds, {milliseconds:0.0f} milliseconds, {microseconds:0.0f} microseconds, {nanoseconds:0.0f} nanoseconds"
