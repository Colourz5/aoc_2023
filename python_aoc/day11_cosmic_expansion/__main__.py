"""This module contains the solution to Day 11 of Advent of Code 2023"""

# Standard libraries
import argparse
import time
from typing import Sequence

# External libraries
from rich import print as rprint

# Internal libraries
from python_aoc.common import format_execution_time


def main(args: Sequence[str]) -> None:
    parsed_args = _parse_args(args)
    input_path: str = parsed_args.input_path
    num_trials: int = parsed_args.num_trials
    verbose_mode: bool = parsed_args.verbose

    lines: Sequence[str] = []
    with open(input_path, "r", encoding="utf-8") as input_file:
        lines = input_file.readlines()

    # Part 1
    result: int | None = None
    total_time: int = 0
    for _ in range(num_trials):
        start_time: int = time.perf_counter_ns()
        result = calculate_sum_galaxy_distance(lines, 2, verbose_mode)
        total_time += time.perf_counter_ns() - start_time
    average_time: float = total_time / num_trials
    rprint(f"Part 1 - Result = {result}")
    rprint(f"Part 1 took {format_execution_time(average_time)}")

    # Part 2
    result: int | None = None
    total_time: int = 0
    for _ in range(num_trials):
        start_time: int = time.perf_counter_ns()
        result = calculate_sum_galaxy_distance(lines, 1_000_000, verbose_mode)
        total_time += time.perf_counter_ns() - start_time
    average_time: float = total_time / num_trials
    rprint(f"Part 2 - Result = {result}")
    rprint(f"Part 2 took {format_execution_time(average_time)}")


def calculate_sum_galaxy_distance(
    lines: Sequence[str], expansion_factor: int, verbose_mode: bool
) -> int:
    """Calculates the sum of the distances between each galaxy after accounting for expansion.

    Parameters
    ----------
    lines : Sequence[str]
        The lines of the input file.
    expansion_factor: int
        The expansion factor of the Universe.
    verbose_mode : bool
        Turn on verbose mode.

    Returns
    -------
    int
        The sum of the distances between each galaxy after expansion.
    """
    grid: list[list[str]] = []
    width: int | None = None
    for line in lines:
        line = line.rstrip("\n")
        if width is None:
            width = len(line)
        assert width == len(line)
        grid.append(list(line))
    assert width is not None

    marked_rows, marked_columns = get_expansion_strips(grid, width)
    galaxies: Sequence[tuple[int, int]] = calculate_galaxy_coordinates(
        grid, marked_rows, marked_columns, expansion_factor
    )
    result: int = calculate_shortest_distance_sum(galaxies)
    if verbose_mode:
        print_grid(grid)

    return result


def get_expansion_strips(
    grid: Sequence[Sequence[str]],
    width: int,
) -> tuple[Sequence[int], Sequence[int]]:
    """Get the rows and columns that need to be expanded.

    Parameters
    ----------
    grid : Sequence[Sequence[int]]
        The grid to look at.
    width : int
        The width of the grid.

    Returns
    -------
    marked_rows : Sequence[int]
        The indices of the rows that need to be expanded.
    marked_columns : Sequence[int]
        The indices of the columns that need to be expanded.
    """
    # Mark rows that need to expand
    marked_rows: list[int] = []
    for row_idx, row in enumerate(grid):
        if "#" not in row:
            marked_rows.append(row_idx)

    # Mark columns that need to expand
    marked_columns: list[int] = []
    for column_idx in range(width):
        column = [row[column_idx] for row in grid]
        if "#" not in column:
            marked_columns.append(column_idx)
    return (marked_rows, marked_columns)


def calculate_galaxy_coordinates(
    grid: Sequence[Sequence[str]],
    marked_rows: Sequence[int],
    marked_columns: Sequence[int],
    expansion_factor: int,
) -> Sequence[tuple[int, int]]:
    """Locates and calculates the coordinates of galaxies in the expanded Universe.

    Parameters
    ----------
    grid : Sequence[Sequence[str]]
        The grid to look at.
    marked_rows: Sequence[int]
        The rows to expand.
    marked_columns: Sequence[int]
        The columns to expand.
    expansion_factor : int
        The factor with which to expand by.

    Parameters
    ----------
    galaxies : Sequence[tuple[int, int]]
        A sequence of galaxy coordinates in the format (row, column).
    """
    # Get the locations of the galaxies
    galaxies: list[tuple[int, int]] = []
    # The row index in the expanded Universe
    expanded_row_idx: int = 0
    for row_idx, row in enumerate(grid):
        # Can skip because marked rows are defined as not having a galaxy
        if row_idx in marked_rows:
            # Add expanded rows
            expanded_row_idx += expansion_factor
            continue
        # The column index in the expanded Universe
        expanded_column_idx: int = 0
        for column_idx, cell in enumerate(row):
            # Can skip because marked columns are defined as not having a galaxy
            if column_idx in marked_columns:
                # Add expanded columns
                expanded_column_idx += expansion_factor
                continue
            # Found a galaxy
            if cell == "#":
                galaxies.append((expanded_row_idx, expanded_column_idx))
            # In the no-expansion case just add one column
            expanded_column_idx += 1
        # In the no-expansion case just add one row
        expanded_row_idx += 1
    return galaxies


def calculate_shortest_distance_sum(galaxies: Sequence[tuple[int, int]]) -> int:
    """Calculates the shortest distance between each galaxy pair and sums them.

    Parameters
    ----------
    galaxies : Sequence[tuple[int, int]]
        A sequence of galaxy coordinates.
        The first element is the row and the second is the column.

    Returns
    -------
    int
        The sum of the shortest distances.
    """
    result: int = 0
    # Iterate over every galaxy
    for current_galaxy_idx, current_galaxy in enumerate(galaxies[:-1]):
        # Iterate over the galaxies after it (to skip double ups)
        for _, other_galaxy in enumerate(galaxies[current_galaxy_idx + 1 :]):
            # Shortest distance is simply Manhattan distance as there are no obstacles
            distance_x = abs(other_galaxy[1] - current_galaxy[1])
            distance_y = abs(other_galaxy[0] - current_galaxy[0])
            distance = distance_x + distance_y
            result += distance
    return result


def print_grid(grid: Sequence[Sequence[str]]) -> None:
    """Prints the grid in a nice to look at format. This isn't that useful as we don't mutate the grid in this
    implementation.

    Parameters
    ----------
    grid : Sequence[Sequence[str]]
        The grid to print.
    """
    translate_cell: dict[str, str] = {"#": "o", ".": " "}
    for line in grid:
        for cell in line:
            display_cell = translate_cell[cell] if cell in translate_cell else cell
            match cell:
                case "#":
                    style = "blink yellow"
                case _:
                    style = "white"
            rprint(f"[{style}]{display_cell}[/{style}]", end="")
        rprint()
    rprint()


def _parse_args(args: Sequence[str]) -> argparse.Namespace:
    """Parses the command line arguments

    Parameters
    ----------
    args : Sequence[str]
        The command line arguments.

    Returns
    -------
    parsed_args : argparse.Namespace
        The parsed command line arguments.
    """
    parser = argparse.ArgumentParser(
        prog="aoc 2023 day 11",
        description="Program to solve Day 11 of Advent of Code 2023",
    )
    parser.add_argument("input_path", type=str, help="Path to input file")
    parser.add_argument(
        "-num_trials",
        "--num_trials",
        type=int,
        default=100,
        help="Number of trials to use when measuring run time",
    )
    parser.add_argument(
        "-v",
        "--v",
        "-verbose",
        "--verbose",
        action="store_true",
        dest="verbose",
        help="Turn on verbose mode",
    )
    return parser.parse_args(args)


if __name__ == "__main__":
    import sys

    main(sys.argv[1:])
