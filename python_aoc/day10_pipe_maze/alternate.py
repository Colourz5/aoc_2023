"""This module contains the solution to Day 10 of Advent of Code 2023"""

from __future__ import annotations

# Standard libraries
import argparse
from dataclasses import dataclass
from enum import Enum
import time
from typing import Sequence

# External libraries
from rich import print as rprint
from rich import progress as rprogress

# Internal libraries
from python_aoc.common import format_execution_time


@dataclass
class MovementOptions:
    above: bool
    below: bool
    left: bool
    right: bool


class NodeKind(Enum):
    GROUND = "."
    VERTICAL_PIPE = "|"
    HORIZONTAL_PIPE = "-"
    TOP_LEFT_BEND = "F"
    TOP_RIGHT_BEND = "7"
    BOTTOM_LEFT_BEND = "L"
    BOTTOM_RIGHT_BEND = "J"
    START = "S"

    def to_unicode(self) -> str:
        match self:
            case NodeKind.GROUND:
                return " "
            case NodeKind.VERTICAL_PIPE:
                return "\u2503"
            case NodeKind.HORIZONTAL_PIPE:
                return "\u2501"
            case NodeKind.TOP_LEFT_BEND:
                return "\u250F"
            case NodeKind.TOP_RIGHT_BEND:
                return "\u2513"
            case NodeKind.BOTTOM_LEFT_BEND:
                return "\u2517"
            case NodeKind.BOTTOM_RIGHT_BEND:
                return "\u251B"
            case NodeKind.START:
                return "S"

    def background(self) -> str:
        match self:
            case NodeKind.GROUND:
                return "black"
            case NodeKind.VERTICAL_PIPE:
                return "blue"
            case NodeKind.HORIZONTAL_PIPE:
                return "blue"
            case NodeKind.TOP_LEFT_BEND:
                return "blue"
            case NodeKind.TOP_RIGHT_BEND:
                return "blue"
            case NodeKind.BOTTOM_LEFT_BEND:
                return "blue"
            case NodeKind.BOTTOM_RIGHT_BEND:
                return "blue"
            case NodeKind.START:
                return "blue"

    def possible_directions(self) -> MovementOptions:
        match self:
            case NodeKind.GROUND:
                return MovementOptions(above=True, below=True, left=True, right=True)
            case NodeKind.VERTICAL_PIPE:
                return MovementOptions(above=True, below=True, left=False, right=False)
            case NodeKind.HORIZONTAL_PIPE:
                return MovementOptions(above=False, below=False, left=True, right=True)
            case NodeKind.TOP_LEFT_BEND:
                return MovementOptions(above=False, below=True, left=False, right=True)
            case NodeKind.TOP_RIGHT_BEND:
                return MovementOptions(above=False, below=True, left=True, right=False)
            case NodeKind.BOTTOM_LEFT_BEND:
                return MovementOptions(above=True, below=False, left=False, right=True)
            case NodeKind.BOTTOM_RIGHT_BEND:
                return MovementOptions(above=True, below=False, left=True, right=False)
            case NodeKind.START:
                return MovementOptions(above=True, below=True, left=True, right=True)


class Graph:
    def __init__(self):
        self._vertices: list[Node] = []
        self._edges: dict[Node, set[Node]] = {}

    def add_vertex(self, vertex: Node) -> None:
        self._vertices.append(vertex)
        if vertex not in self._edges:
            self._edges[vertex] = set()

    def add_edge(self, start: Node, end: Node) -> None:
        if start not in self._edges:
            self._edges[start] = set()
        if end not in self._edges:
            self._edges[end] = set()
        self._edges[start].add(end)
        self._edges[end].add(start)

    def get_vertices(self) -> Sequence[Node]:
        return self._vertices

    def get_edges(self, node: Node) -> Sequence[Node]:
        if node not in self._edges:
            rprint(self._edges)
        assert node in self._edges
        return list(self._edges[node])


@dataclass
class Node:
    kind: NodeKind
    row_idx: int
    column_idx: int
    enclosed: bool | None
    visited: bool
    distance: int | None
    # Graph neighbours
    left: Node | None
    right: Node | None
    above: Node | None
    below: Node | None

    def __str__(self) -> str:
        return self.kind.to_unicode()

    def __repr__(self) -> str:
        return f"({self.row_idx}, {self.column_idx})"

    def get_neighbours(self) -> Sequence[Node]:
        neighbours: list[Node] = []
        if self.left is not None:
            neighbours.append(self.left)
        if self.right is not None:
            neighbours.append(self.right)
        if self.above is not None:
            neighbours.append(self.above)
        if self.below is not None:
            neighbours.append(self.below)
        return neighbours

    def __hash__(self) -> int:
        return hash((self.kind, self.row_idx, self.column_idx))


def main(args: Sequence[str]) -> None:
    parsed_args = _parse_args(args)
    input_path: str = parsed_args.input_path
    num_trials: int = parsed_args.num_trials
    verbose_mode: bool = parsed_args.verbose

    lines: Sequence[str] = []
    with open(input_path, "r", encoding="utf-8") as input_file:
        lines = input_file.readlines()

    # Part 1
    result: int | None = None
    total_time: int = 0
    for _ in range(num_trials):
        start_time: int = time.perf_counter_ns()
        result = part1(lines, verbose_mode)
        total_time += time.perf_counter_ns() - start_time
    average_time: float = total_time / num_trials
    rprint(f"Part 1 - Result = {result}")
    rprint(f"Part 1 took {format_execution_time(average_time)}")

    # Part 2
    result: int | None = None
    total_time: int = 0
    for _ in range(num_trials):
        start_time: int = time.perf_counter_ns()
        result = part2(lines, verbose_mode)
        total_time += time.perf_counter_ns() - start_time
    average_time: float = total_time / num_trials
    rprint(f"Part 2 - Result = {result}")
    rprint(f"Part 2 took {format_execution_time(average_time)}")


def part1(lines: Sequence[str], verbose_mode: bool) -> int:
    """Part 1 of Day 10 of Advent of Code 2023

    Parameters
    ----------
    lines : Sequence[str]
        The lines of the input file.
    verbose_mode : bool
        Turn on verbose mode.

    Returns
    -------
    int
        The answer
    """
    grid: list[list[Node]] = []
    width: int | None = None
    work_queue: list[Node] = []
    row_idx: int = 0
    for line in lines:
        line = line.rstrip("\n")
        if width is None:
            width = len(line)
        else:
            assert len(line) == width
        nodes: list[Node] = []
        for column_idx, node_type in enumerate(line):
            distance = None if node_type != "S" else 0
            kind = NodeKind(node_type)
            node = Node(
                kind,
                row_idx=row_idx,
                column_idx=column_idx,
                enclosed=None,
                visited=False,
                distance=distance,
                left=None,
                right=None,
                above=None,
                below=None,
            )
            nodes.append(node)
            if kind == NodeKind.START:
                work_queue.append(node)
        grid.append(nodes)
        row_idx += 1

    # Dimensions
    height = len(grid)
    assert width is not None

    # Fill in the neighbours
    for row_idx, row in enumerate(grid):
        for column_idx, node in enumerate(row):
            populate_neighbours(grid, node, width, height)

    while len(work_queue) > 0:
        node = work_queue.pop(0)
        node.visited = True
        assert node.kind != NodeKind.GROUND
        assert node.distance is not None
        neighbours = node.get_neighbours()
        new_nodes = []
        new_distance = node.distance + 1
        for adjacent_node in neighbours:
            if adjacent_node.distance is None or new_distance < adjacent_node.distance:
                adjacent_node.distance = new_distance
            if not adjacent_node.visited:
                new_nodes.append(adjacent_node)
        work_queue.extend(new_nodes)
    # Find node that is the farthest
    farthest: int = 0
    for row_idx, row in enumerate(grid):
        for column_idx, node in enumerate(row):
            if node.distance is not None and node.distance > farthest:
                farthest = node.distance
    if verbose_mode:
        print_grid(grid)
    return farthest


def part2(lines: Sequence[str], verbose_mode: bool) -> int:
    """Part 2 of Day 10 of Advent of Code 2023

    Parameters
    ----------
    lines : Sequence[str]
        The lines of the input file.
    verbose_mode : bool
        Turn on verbose mode.

    Returns
    -------
    int
        The answer
    """
    grid: list[list[Node]] = []
    width: int | None = None
    work_queue: list[Node] = []
    row_idx: int = 0
    for line in lines:
        line = line.rstrip("\n")
        if width is None:
            width = len(line)
        else:
            assert len(line) == width
        nodes: list[Node] = []
        for column_idx, node_type in enumerate(line):
            distance = None if node_type != "S" else 0
            kind = NodeKind(node_type)
            node = Node(
                kind,
                row_idx=row_idx,
                column_idx=column_idx,
                enclosed=None,
                visited=False,
                distance=distance,
                left=None,
                right=None,
                above=None,
                below=None,
            )
            nodes.append(node)
            if kind == NodeKind.START:
                work_queue.append(node)
        grid.append(nodes)
        row_idx += 1

    # Dimensions
    height = len(grid)
    assert width is not None

    # Fill in S
    for row_idx, row in enumerate(grid):
        for column_idx, node in enumerate(row):
            populate_neighbours(grid, node, width, height)
            if node.kind != NodeKind.START:
                continue
            neighbours = node.get_neighbours()
            if len(set(neighbours)) == 2:
                # Top left bend
                if node.below is not None and node.right is not None:
                    node.kind = NodeKind.TOP_LEFT_BEND
                # Top right bend
                elif node.below is not None and node.left is not None:
                    node.kind = NodeKind.TOP_RIGHT_BEND
                # Bottom left bend
                elif node.above is not None and node.right is not None:
                    node.kind = NodeKind.BOTTOM_LEFT_BEND
                # Bottom right bend
                elif node.above is not None and node.left is not None:
                    node.kind = NodeKind.BOTTOM_RIGHT_BEND
                # Vertical pipe
                elif node.above is not None and node.below is not None:
                    node.kind = NodeKind.VERTICAL_PIPE
                # Horizontal pipe
                elif node.left is not None and node.right is not None:
                    node.kind = NodeKind.HORIZONTAL_PIPE
                # Unknown case
                else:
                    rprint(f"Unknown case: {neighbours}")

    # Need to mark boundary pipes
    while len(work_queue) > 0:
        node = work_queue.pop(0)
        node.visited = True
        assert node.kind != NodeKind.GROUND
        assert node.distance is not None
        neighbours = node.get_neighbours()
        new_nodes = []
        new_distance = node.distance + 1
        for adjacent_node in neighbours:
            if adjacent_node.distance is None or new_distance < adjacent_node.distance:
                adjacent_node.distance = new_distance
            if not adjacent_node.visited:
                new_nodes.append(adjacent_node)
        work_queue.extend(new_nodes)

    result: int = 0
    num_ground: int = 0
    for row_idx, row in enumerate(grid):
        for column_idx, node in enumerate(row):
            if node.visited:
                continue
            # Count the parity
            # Left
            crossings = 0
            for ray_idx in range(0, column_idx):
                ray_node = grid[row_idx][ray_idx]
                ray_kind = ray_node.kind
                if not ray_node.visited:
                    continue

                if (
                    ray_kind == NodeKind.VERTICAL_PIPE
                    or ray_kind == NodeKind.BOTTOM_LEFT_BEND
                    or ray_kind == NodeKind.BOTTOM_RIGHT_BEND
                ):
                    crossings += 1
            if crossings % 2 != 0:
                node.enclosed = True
                result += 1

    if verbose_mode:
        print_grid(grid)

    return result


def is_inside(graph: Graph, row_idx: int, column_idx: int) -> bool:
    intersections: int = 0
    processed_edges: list[tuple[Node, Node]] = []
    vertices = graph.get_vertices()
    skipped: int = 0
    rprint(f"Processing {len(vertices)} vertices")
    for vertex in rprogress.track(vertices):
        edges = graph.get_edges(vertex)
        for neighbour in edges:
            # Add to processed edges
            if (vertex, neighbour) in processed_edges or (
                neighbour,
                vertex,
            ) in processed_edges:
                skipped += 1
                continue
            else:
                processed_edges.append((vertex, neighbour))
            # Edge is vertex -> neighbour
            start_row = vertex.row_idx
            start_column = vertex.column_idx
            end_row = neighbour.row_idx
            # Fail on horizontal edges
            if start_row == end_row:
                skipped += 1
                continue
            # Draw horizontal ray through point and edge
            vertical_range_start = min(start_row, end_row)
            vertical_range_end = max(start_row, end_row)
            within_vertical_range = row_idx in range(
                vertical_range_start, vertical_range_end
            )
            if not within_vertical_range:
                skipped += 1
                continue
            if column_idx < start_column:
                intersections += 1
    rprint(f"Skipped {skipped} edges")
    return intersections % 2 != 0


def populate_neighbours(
    grid: Sequence[Sequence[Node]],
    node: Node,
    width: int,
    height: int,
) -> None:
    row_idx = node.row_idx
    column_idx = node.column_idx
    is_ground = node.kind == NodeKind.GROUND
    # Top, bottom, left, right
    movement_options: MovementOptions = node.kind.possible_directions()
    # Top
    if movement_options.above and row_idx - 1 >= 0:
        # Check that the next neighbour can be entered from below
        neighbour = grid[row_idx - 1][column_idx]
        neighbour_options = neighbour.kind.possible_directions()
        can_add = neighbour_options.below
        can_add &= not is_ground and neighbour.kind != NodeKind.GROUND
        if can_add:
            node.above = neighbour
    # Bottom
    if movement_options.below and row_idx + 1 < height:
        # Check that the next neighbour can be entered from above
        neighbour = grid[row_idx + 1][column_idx]
        neighbour_options = neighbour.kind.possible_directions()
        can_add = neighbour_options.above
        can_add &= not is_ground and neighbour.kind != NodeKind.GROUND
        if can_add:
            node.below = neighbour
    # Left
    if movement_options.left and column_idx - 1 >= 0:
        # Check that the next neighbour can be entered from the right
        neighbour = grid[row_idx][column_idx - 1]
        neighbour_options = neighbour.kind.possible_directions()
        can_add = neighbour_options.right
        can_add &= not is_ground and neighbour.kind != NodeKind.GROUND
        if can_add:
            node.left = neighbour
    # Right
    if movement_options.right and column_idx + 1 < width:
        # Check that the next neighbour can be entered from the left
        neighbour = grid[row_idx][column_idx + 1]
        neighbour_options = neighbour.kind.possible_directions()
        can_add = neighbour_options.left
        can_add &= not is_ground and neighbour.kind != NodeKind.GROUND
        if can_add:
            node.right = neighbour


def populate_neighbours_extend(
    grid: Sequence[Sequence[Node]],
    node: Node,
    width: int,
    height: int,
) -> None:
    row_idx = node.row_idx
    column_idx = node.column_idx
    is_ground = node.kind == NodeKind.GROUND
    # Top, bottom, left, right
    movement_options: MovementOptions = node.kind.possible_directions()
    # Extend up above
    above_idx = row_idx - 1
    for row_offset in range(above_idx, -1, -1):
        link_node = grid[row_offset][column_idx]
        if link_node.kind != NodeKind.VERTICAL_PIPE:
            above_idx = row_offset
            break
    # Extend down below
    below_idx = row_idx + 1
    for row_offset in range(below_idx, height):
        link_node = grid[row_offset][column_idx]
        if link_node.kind != NodeKind.VERTICAL_PIPE:
            below_idx = row_offset
            break
    # Extend to the left
    left_idx = column_idx - 1
    for column_offset in range(left_idx, -1, -1):
        link_node = grid[row_idx][column_offset]
        if link_node.kind != NodeKind.HORIZONTAL_PIPE:
            left_idx = column_offset
            break
    # Extend to the right
    right_idx = column_idx + 1
    for column_offset in range(right_idx, width):
        link_node = grid[row_idx][column_offset]
        if link_node.kind != NodeKind.HORIZONTAL_PIPE:
            right_idx = column_offset
            break
    # Top
    if movement_options.above and above_idx >= 0:
        # Check that the next neighbour can be entered from below
        neighbour = grid[above_idx][column_idx]
        neighbour_options = neighbour.kind.possible_directions()
        can_add = neighbour_options.below
        can_add &= not is_ground and neighbour.kind != NodeKind.GROUND
        if can_add:
            node.above = neighbour
    # Bottom
    if movement_options.below and below_idx < height:
        # Check that the next neighbour can be entered from above
        neighbour = grid[below_idx][column_idx]
        neighbour_options = neighbour.kind.possible_directions()
        can_add = neighbour_options.above
        can_add &= not is_ground and neighbour.kind != NodeKind.GROUND
        if can_add:
            node.below = neighbour
    # Left
    if movement_options.left and left_idx >= 0:
        # Check that the next neighbour can be entered from the right
        neighbour = grid[row_idx][left_idx]
        neighbour_options = neighbour.kind.possible_directions()
        can_add = neighbour_options.right
        can_add &= not is_ground and neighbour.kind != NodeKind.GROUND
        if can_add:
            node.left = neighbour
    # Right
    if movement_options.right and right_idx < width:
        # Check that the next neighbour can be entered from the left
        neighbour = grid[row_idx][right_idx]
        neighbour_options = neighbour.kind.possible_directions()
        can_add = neighbour_options.left
        can_add &= not is_ground and neighbour.kind != NodeKind.GROUND
        if can_add:
            node.right = neighbour


def print_grid(grid: Sequence[Sequence[Node]]) -> None:
    for row in grid:
        for node in row:
            if node.enclosed is None:
                if node.visited:
                    background_color = node.kind.background()
                else:
                    background_color = "green"
            elif node.enclosed:
                background_color = "purple"
            else:
                background_color = "cyan"
            rprint(
                f"[on {background_color}]{node.kind.to_unicode()}[/on {background_color}]",
                end="",
            )
        rprint()
    rprint()


def print_steps(grid: Sequence[Sequence[tuple[str, int | None]]]) -> None:
    max_steps = None
    for row in grid:
        for _, steps in row:
            if max_steps is None:
                max_steps = steps
            elif steps is not None and steps > max_steps:
                max_steps = steps
    max_digits = len(str(max_steps)) if max_steps is not None else 4
    for row in grid:
        for _, steps in row:
            if steps is not None:
                cell = str(steps).zfill(max_digits)
                background_color = "green"
            else:
                cell = max_digits * " "
                background_color = "red"
            rprint(f"[on {background_color}]{cell}[/on {background_color}]", end="")
        rprint()
    rprint()


def _parse_args(args: Sequence[str]) -> argparse.Namespace:
    """Parses the command line arguments

    Parameters
    ----------
    args : Sequence[str]
        The command line arguments.

    Returns
    -------
    parsed_args : argparse.Namespace
        The parsed command line arguments.
    """
    parser = argparse.ArgumentParser(
        prog="aoc XXXX day XX",
        description="Program to solve Day XX of Advent of Code XXXX",
    )
    parser.add_argument("input_path", type=str, help="Path to input file")
    parser.add_argument(
        "-num_trials",
        "--num_trials",
        type=int,
        default=100,
        help="Number of trials to use when measuring run time",
    )
    parser.add_argument(
        "-v",
        "--v",
        "-verbose",
        "--verbose",
        action="store_true",
        dest="verbose",
        help="Turn on verbose mode",
    )
    return parser.parse_args(args)


if __name__ == "__main__":
    import sys

    main(sys.argv[1:])
