"""This module contains the solution to Day 14 of Advent of Code 2023"""

# Standard libraries
import argparse
import time
from typing import Sequence

# External libraries
from rich import print as rprint

# Internal libraries
from python_aoc.common import format_execution_time


def main(args: Sequence[str]) -> None:
    """The main function

    Parameters
    ----------
    args : Sequence[str]
        The command line arguments.
    """
    parsed_args = _parse_args(args)
    input_path: str = parsed_args.input_path
    num_trials: int = parsed_args.num_trials
    verbose_mode: bool = parsed_args.verbose

    lines: Sequence[str] = []
    with open(input_path, "r", encoding="utf-8") as input_file:
        lines = input_file.readlines()

    # Part 1
    result: int | None = None
    total_time: int = 0
    for _ in range(num_trials):
        start_time: int = time.perf_counter_ns()
        result = calculate_load_after_tilt_north(lines, verbose_mode)
        total_time += time.perf_counter_ns() - start_time
    average_time: float = total_time / num_trials
    rprint(f"Part 1 - Result = {result}")
    rprint(f"Part 1 took {format_execution_time(average_time)}")

    # Part 2
    result: int | None = None
    total_time: int = 0
    for _ in range(num_trials):
        start_time: int = time.perf_counter_ns()
        result = calculate_load_after_trillion_spin_cycles(lines, verbose_mode)
        total_time += time.perf_counter_ns() - start_time
    average_time: float = total_time / num_trials
    rprint(f"Part 2 - Result = {result}")
    rprint(f"Part 2 took {format_execution_time(average_time)}")


def calculate_load_after_tilt_north(lines: Sequence[str], verbose_mode: bool) -> int:
    """Calculates the load on the northern support beams tilting the platform north. Solves part 1 of Day 14 of Advent of Code 2023.

    Parameters
    ----------
    lines : Sequence[str]
        The lines of the input file.
    verbose : bool
        Turn on verbose mode.

    Returns
    -------
    int
        The answer
    """
    grid: list[list[str]] = []
    for line in lines:
        line = line.rstrip("\n")
        grid.append(list(line))
    if verbose_mode:
        rprint("Before tilt")
        print_grid(grid)

    # Tilt north
    tilt_grid(grid, direction=(-1, 0))
    if verbose_mode:
        rprint("After tilt")
        print_grid(grid)
    total_load = calculate_north_load(grid)
    return total_load


def calculate_load_after_trillion_spin_cycles(
    lines: Sequence[str], verbose_mode: bool
) -> int:
    """Calculates the load on the northern support beams after a trillion spin cycles. Solves part 2 of Day 14 of Advent of Code 2023.

    Parameters
    ----------
    lines : Sequence[str]
        The lines of the input file.
    verbose : bool
        Turn on verbose mode.

    Returns
    -------
    int
        The answer
    """
    MAX_CYCLES: int = 1_000_000_000
    grid: list[list[str]] = []
    for line in lines:
        line = line.rstrip("\n")
        grid.append(list(line))
    if verbose_mode:
        rprint("Before tilt")
        print_grid(grid)

    initial_load: int = calculate_north_load(grid)
    # Grid hash -> (north load, cycle_number)
    past_grids: dict[int, tuple[int, int]] = {hash_grid(grid): (initial_load, 0)}
    # Cycle number -> north load
    past_loads: list[int] = [initial_load]
    # Start at cycle 1
    cycle_idx: int = 0
    # Place to store answer
    answer: int = 0
    while cycle_idx < MAX_CYCLES:
        # Increment cycle
        cycle_idx += 1
        # Perform spin cycle
        # 1. North tilt
        tilt_grid(grid, direction=(-1, 0))
        # 2. West tilt
        tilt_grid(grid, direction=(0, -1))
        # 3. South tilt
        tilt_grid(grid, direction=(+1, 0))
        # 4. East tilt
        tilt_grid(grid, direction=(0, +1))
        # Hash the grid
        current_hash = hash_grid(grid)
        # Calculate the load
        total_load = calculate_north_load(grid)
        if verbose_mode:
            rprint(f"Cycle {cycle_idx} - Load = {total_load}")
        if current_hash in past_grids:
            # The grid which we have seen before in number of cycles
            _, seen_idx = past_grids[current_hash]
            # Calculate the period
            period = cycle_idx - seen_idx
            # The offset into the cycle where we hit the trillion cycles
            remainder = (MAX_CYCLES - seen_idx) % period
            # The load at that offset
            answer_idx = seen_idx + remainder
            # Save the answer and break
            answer = past_loads[answer_idx]
            break
        # Add to past grids
        past_grids[current_hash] = (total_load, cycle_idx)
        past_loads.append(total_load)
    return answer


def tilt_grid(grid: list[list[str]], direction: tuple[int, int]) -> None:
    """Tilts the grid of rocks in some direction. This mutates the grid in place.

    Parameters
    ----------
    grid : list[list[str]]
        The grid to tilt.
    direction : tuple[int, int]
        The direction of the tilt. This is a tuple of the row offset and column offset
        that the rocks are allowed to move to in a single iteration.
    """
    # Dimensions
    height = len(grid)
    width = len(grid[0])
    row_offset, column_offset = direction
    # Keep iterating until nothing is moved
    while True:
        moved = False
        for row_idx, row in enumerate(grid):
            target_row = row_idx + row_offset
            # At the northern or southern edge
            if target_row < 0 or target_row >= height:
                continue
            for column_idx, cell in enumerate(row):
                target_column = column_idx + column_offset
                if cell != "O":
                    continue
                # At the northern or southern edge
                if target_column < 0 or target_column >= width:
                    continue
                target_cell = grid[target_row][target_column]
                if target_cell != ".":
                    continue
                # Move O to the target position
                grid[target_row][target_column] = "O"
                grid[row_idx][column_idx] = "."
                moved = True

        if not moved:
            break


def calculate_north_load(grid: Sequence[Sequence[str]]) -> int:
    """Calculates the load on the northern support beams from the round rocks.

    Parameters
    ----------
    grid : Sequence[Sequence[str]]
        The grid to calculate the load on.

    Returns
    -------
    int
        The load on the northern support beams.
    """
    height: int = len(grid)
    total_load: int = 0
    for row_idx, row in enumerate(grid):
        round_rock_count: int = 0
        for cell in row:
            round_rock_count += 1 if cell == "O" else 0
        load: int = (height - row_idx) * round_rock_count
        total_load += load
    return total_load


def print_grid(grid: Sequence[Sequence[str]]) -> None:
    """Prints a grid of cells.

    Parameters
    ----------
    grid : Sequence[Sequence[str]]
        The grid to print.
    """
    width: int = len(grid[0])
    rprint("  ", end="")
    for column_idx in range(width):
        rprint(f"[white]{column_idx}[/white]", end=" ")
    rprint()
    for row_idx, row in enumerate(grid):
        rprint(f"[white]{row_idx}[/white]", end="|")
        for cell in row:
            match cell:
                case "O":
                    cell = " "
                    style = "on red"
                case "#":
                    cell = " "
                    style = "on gray"
                case ".":
                    cell = " "
                    style = "on black"
                case _:
                    style = "magenta on cyan"
            rprint(f"[{style}]{cell}[/{style}]", end="|")
        rprint()
    rprint()


def hash_grid(grid: Sequence[Sequence[str]]) -> int:
    """Creates an integer representation of a grid to use a hash.

    Parameters
    ----------
    grid : Sequence[Sequence[str]]
        The grid to hash.

    Returns
    -------
    int
        The hash.
    """
    hash_str = ""
    for row_idx, row in enumerate(grid):
        for column_idx, cell in enumerate(row):
            match cell:
                case "O":
                    hash_str += f"1{row_idx}{column_idx}"
                case "#":
                    hash_str += f"0{row_idx}{column_idx}"
                case _:
                    pass
    return int.from_bytes(hash_str.encode(encoding="utf-8"))


def _parse_args(args: Sequence[str]) -> argparse.Namespace:
    """Parses the command line arguments

    Parameters
    ----------
    args : Sequence[str]
        The command line arguments.

    Returns
    -------
    parsed_args : argparse.Namespace
        The parsed command line arguments.
    """
    parser = argparse.ArgumentParser(
        prog="aoc 2023 day 14",
        description="Program to solve Day 14 of Advent of Code 2023",
    )
    parser.add_argument("input_path", type=str, help="Path to input file")
    parser.add_argument(
        "-num_trials",
        "--num_trials",
        type=int,
        default=100,
        help="Number of trials to use when measuring run time",
    )
    parser.add_argument(
        "-v",
        "--v",
        "-verbose",
        "--verbose",
        action="store_true",
        dest="verbose",
        help="Turn on verbose mode",
    )
    return parser.parse_args(args)


if __name__ == "__main__":
    import sys

    main(sys.argv[1:])
