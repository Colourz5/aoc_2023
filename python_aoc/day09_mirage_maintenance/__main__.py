"""This module contains the solution to Day 09 of Advent of Code 2023"""

# Standard libraries
import argparse
from functools import reduce
import time
from typing import Sequence

# External libraries
from rich import print as rprint

# Internal libraries
from python_aoc.common import format_execution_time


def main(args: Sequence[str]) -> None:
    parsed_args = _parse_args(args)
    input_path: str = parsed_args.input_path
    num_trials: int = parsed_args.num_trials
    verbose_mode: bool = parsed_args.verbose

    lines: Sequence[str] = []
    with open(input_path, "r", encoding="utf-8") as input_file:
        lines = input_file.readlines()

    # Part 1
    result: int | None = None
    total_time: int = 0
    for _ in range(num_trials):
        start_time: int = time.perf_counter_ns()
        result = extrapolate_history(lines, True, verbose_mode)
        total_time += time.perf_counter_ns() - start_time
    average_time: float = total_time / num_trials
    rprint(f"Part 1 - Result = {result}")
    rprint(f"Part 1 took {format_execution_time(average_time)}")

    # Part 2
    result: int | None = None
    total_time: int = 0
    for _ in range(num_trials):
        start_time: int = time.perf_counter_ns()
        result = extrapolate_history(lines, False, verbose_mode)
        total_time += time.perf_counter_ns() - start_time
    average_time: float = total_time / num_trials
    rprint(f"Part 2 - Result = {result}")
    rprint(f"Part 2 took {format_execution_time(average_time)}")


def extrapolate_history(
    lines: Sequence[str], forwards: bool, verbose_mode: bool
) -> int:
    """Extrapolates the history of a sequence of numbers and returns the sum of the extrapolated values.
    Solves part 1 and part 2 of Day 09 of Advent of Code 2023.

    Parameters
    ----------
    lines : Sequence[str]
        The lines of the input file.
    forwards: bool
        Whether to extrapolate forwards or backwards.
    verbose_mode : bool
        Turn on verbose mode.

    Returns
    -------
    int
        The sum of the extrapolated values.
    """
    result: int = 0
    for line in lines:
        line = line.rstrip("\n")
        tokens = line.split()
        numbers = [int(token) for token in tokens]
        next_number = extrapolate_numbers(numbers, forwards=forwards)
        if verbose_mode:
            rprint(f"Numbers = {numbers} with next = {next_number}")
        result += next_number
    return result


def extrapolate_numbers(numbers: Sequence[int], forwards: bool) -> int:
    """Calculate the next forwards/backwards extrapolated value from a sequence of integers.

    Parameters
    ----------
    numbers : Sequence[int]
        The number sequence to extrapolate.
    forwards : bool
        Whether to extrapolate forwards or backwards.

    Returns
    -------
    int
        The extrapolated value.
    """
    # If the entire sequence is all a single integer
    # then we have our difference needed to add to either the front or the end.
    if len(set(numbers)) == 1:
        return numbers[0]
    # We have a sequence of different numbers
    else:
        # Calculate the differences between each adjacent number
        differences = [y - x for x, y in zip(numbers[:-1], numbers[1:])]
        # Calculate the difference we need to add/subtract
        difference = extrapolate_numbers(differences, forwards)
        # Add to end if forward extrapolating
        if forwards:
            return numbers[-1] + difference
        # Otherwise subtract from front
        else:
            return numbers[0] - difference


def _parse_args(args: Sequence[str]) -> argparse.Namespace:
    """Parses the command line arguments

    Parameters
    ----------
    args : Sequence[str]
        The command line arguments.

    Returns
    -------
    parsed_args : argparse.Namespace
        The parsed command line arguments.
    """
    parser = argparse.ArgumentParser(
        prog="aoc 2023 day 09",
        description="Program to solve Day 09 of Advent of Code 2023",
    )
    parser.add_argument("input_path", type=str, help="Path to input file")
    parser.add_argument(
        "-num_trials",
        "--num_trials",
        type=int,
        default=100,
        help="Number of trials to use when measuring run time",
    )
    parser.add_argument(
        "-v",
        "--v",
        "-verbose",
        "--verbose",
        action="store_true",
        dest="verbose",
        help="Turn on verbose mode",
    )
    return parser.parse_args(args)


if __name__ == "__main__":
    import sys

    main(sys.argv[1:])
