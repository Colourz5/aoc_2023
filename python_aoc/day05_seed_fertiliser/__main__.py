"""This module contains the solution for Day 5 of AOC 2023."""

# Standard libraries
import argparse
import time
from typing import Sequence

# External libraries
from rich import print as rprint

# Internal libraries
from python_aoc.common import format_execution_time


def main(args: Sequence[str]) -> None:
    """Main function for Day 5 of AOC 2023.

    Parameters
    ----------
    args : Sequence[str]
        The command line arguments.
    """
    # Parse command line arguments
    parsed_args = _parse_args(args)
    input_path: str = parsed_args.input_path
    num_trials: int = parsed_args.num_trials

    # Read input file
    lines: Sequence[str] = []
    with open(input_path, "r", encoding="utf-8") as input_file:
        lines = input_file.readlines()

    rprint(f"Running {num_trials} trials...")

    # Part 1
    total_time = 0
    result = 0
    for _ in range(num_trials):
        start_time = time.perf_counter_ns()
        result = get_min_location_single(lines)
        elapsed_time = time.perf_counter_ns() - start_time
        total_time += elapsed_time
    rprint(f"Part 1 - Result = {result}")
    average_time = total_time / num_trials
    rprint(f"Part 1 took {format_execution_time(average_time)}")

    # Part 2
    total_time = 0
    result = 0
    for _ in range(num_trials):
        start_time = time.perf_counter_ns()
        result = get_min_location_ranges(lines)
        elapsed_time = time.perf_counter_ns() - start_time
        total_time += elapsed_time
    rprint(f"Part 2 - Result = {result}")
    average_time = total_time / num_trials
    rprint(f"Part 2 took {format_execution_time(average_time)}")


def get_min_location_single(lines: Sequence[str]) -> int:
    """Gets the minimum location number that corresponds to any of the initial seed numbers. This is the solution to Day 5
    Part 1 of AOC 2023.

    Parameters
    ----------
    lines : Sequence[str]
        The lines of the input file.

    Returns
    -------
    int
        The minimum location number that corresponds to any of the initial seed numbers.
    """
    # The seed/soil/fertiliser/... numbers
    farm_ids: list[int] = []
    # The mappings from seed to soil, soil to fertiliser, etc.
    mappings: list[dict[range, int]] = []
    # The current mapping being parsed
    current_mapping: dict[range, int] = {}
    for line in lines:
        tokens = line.split()
        # Empty line means we have parsed a single mapping
        if len(tokens) == 0:
            if len(current_mapping) > 0:
                mappings.append(current_mapping)
                current_mapping = {}
            continue
        # The initial line with the seed numbers
        if tokens[0] == "seeds:":
            farm_ids = [int(seed) for seed in tokens[1:]]
            continue
        # The mapping lines
        elif len(tokens) == 3:
            # Mappings are structured as (dest_start, src_start, range_length)
            dest_start = int(tokens[0])
            src_start = int(tokens[1])
            range_length = int(tokens[2])
            # dest = src + offset -> offset = dest - src
            offset = dest_start - src_start
            # The range of source numbers that are mapped to the destination numbers
            src_range = range(src_start, src_start + range_length)
            current_mapping[src_range] = offset
    # Add the last mapping if it exists
    if len(current_mapping) > 0:
        mappings.append(current_mapping)
        current_mapping = {}

    # Apply the mappings to the farm ids
    for _, mapping in enumerate(mappings):
        # The newly mapped farm ids
        new_ids: list[int] = farm_ids.copy()
        for seed_idx, farm_id in enumerate(farm_ids):
            # If the farm id is in the range of a source range, then apply the offset
            for src_range, offset in mapping.items():
                if farm_id in src_range:
                    new_ids[seed_idx] = farm_id + offset
                    break
        # Update the farm ids
        farm_ids = new_ids
    assert len(farm_ids) > 0
    return min(farm_ids)


def get_min_location_ranges(lines: Sequence[str]) -> int:
    """Gets the minimum location number that corresponds to any of the initial seed numbers. This is the solution to Day 5
    Part 2 of AOC 2023.

    Parameters
    ----------
    lines : Sequence[str]
        The lines of the input file.

    Returns
    -------
    int
        The minimum location number that corresponds to any of the initial seed numbers.
    """
    # The farm ids are now ranges (in contrast to single integers like in Part 1)
    farm_ids: list[range] = []
    # The mappings from seed to soil, soil to fertiliser, etc.
    mappings: list[dict[range, int]] = []
    # The current mapping being parsed
    current_mapping: dict[range, int] = {}
    for line in lines:
        tokens = line.split()
        # Empty line means we have parsed a single mapping
        if len(tokens) == 0:
            if len(current_mapping) > 0:
                mappings.append(current_mapping)
                current_mapping = {}
            continue
        # The initial line with the seed ranges
        if tokens[0] == "seeds:":
            # The seed ranges are structured as (start, length)
            for idx in range(1, len(tokens), 2):
                seed_start = int(tokens[idx])
                seed_length = int(tokens[idx + 1])
                farm_ids.append(range(seed_start, seed_start + seed_length))
            continue
        # The mapping lines
        elif len(tokens) == 3:
            # Mappings are structured as (dest_start, src_start, range_length)
            dest_start = int(tokens[0])
            src_start = int(tokens[1])
            range_length = int(tokens[2])
            # dest = src + offset -> offset = dest - src
            offset = dest_start - src_start
            # The range of source numbers that are mapped to the destination numbers
            src_range = range(src_start, src_start + range_length)
            current_mapping[src_range] = offset
    # Add the last mapping if it exists
    if len(current_mapping) > 0:
        mappings.append(current_mapping)
        current_mapping = {}

    # Apply the mappings to the farm ids as ranges
    for idx, mapping in enumerate(mappings):
        # The newly mapped farm id ranges
        new_ids: list[range] = []
        # Check each farm id range against each mapping
        for id_range in farm_ids:
            # The ranges mapped from the current farm id range
            to_add: list[range] = []
            for src_range, offset in mapping.items():
                left_overlap = id_range.start in src_range
                right_overlap = id_range.stop in src_range
                # If the farm id range is fully contained in the source range, then apply the offset to whole range
                if left_overlap and right_overlap:
                    to_add.append(
                        range(id_range.start + offset, id_range.stop + offset)
                    )
                    break
                # If the farm id range is partially contained in the source range, then split the range
                elif left_overlap:
                    # The left range is contained in the source range, so apply the offset to the left range
                    left_range = range(id_range.start + offset, src_range.stop + offset)
                    # The right range is not contained in the source range, so do not apply the offset
                    right_range = range(src_range.stop, id_range.stop)
                    to_add.append(left_range)
                    to_add.append(right_range)
                    break
                elif right_overlap:
                    # The left range is not contained in the source range, so do not apply the offset
                    left_range = range(id_range.start, src_range.start)
                    # The right range is contained in the source range, so apply the offset to the right range
                    right_range = range(
                        src_range.start + offset, id_range.stop + offset
                    )
                    to_add.append(left_range)
                    to_add.append(right_range)
                    break
                # NOTE: For some reason adding in this case causes the solution to be incorrect, so it is commented out
                # # The source range is entirely contained with in the farm id range
                # elif src_range.start in id_range and src_range.stop in id_range:
                #     left_range = range(id_range.start, src_range.start)
                #     middle_range = range(
                #         src_range.start + offset, src_range.stop + offset
                #     )
                #     right_range = range(src_range.stop, id_range.stop)
                #     to_add.append(left_range)
                #     to_add.append(middle_range)
                #     to_add.append(right_range)
                #     break
                else:
                    pass
            # If there are no ranges to add, then the farm id range is identically mapped
            if len(to_add) == 0:
                new_ids.append(id_range)
            # Otherwise, add the newly mapped ranges
            else:
                new_ids.extend(to_add)
        # Update the farm ids
        farm_ids = new_ids
    assert len(farm_ids) > 0
    # Return the minimum location number
    return min(farm_ids, key=lambda x: x.start).start


def _parse_args(args: Sequence[str]) -> argparse.Namespace:
    """Parses the command line arguments.

    Parameters
    ----------
    args : Sequence[str]
        The command line arguments.

    Returns
    -------
    argparse.Namespace
        The parsed arguments.
    """
    parser = argparse.ArgumentParser(
        prog="day05_seed_fertiliser", description="Day 5 of Advent of Code 2023"
    )
    parser.add_argument("input_path", help="File path to input file")
    parser.add_argument(
        "-num_trials",
        "--num_trials",
        type=int,
        default=100,
        help="Number of trials for benchmarking",
    )
    return parser.parse_args(args)


if __name__ == "__main__":
    import sys

    main(sys.argv[1:])
