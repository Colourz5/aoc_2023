"""This module contains Advent of Code 2023 Day 3 - Gear Ratios"""

# Standard libraries
import argparse
from dataclasses import dataclass
import time
from typing import Sequence

# External libraries
from rich import print as rprint

from python_aoc.common import format_execution_time


@dataclass
class PartNumberToken:
    """A token representing a part number in the engine schematic.

    Attributes
    ----------
    value : int
        The part number.
    row_index : int
        The row index of the token.
    span : range
        The column indices of the token.
    touched : bool
        Whether the token is touching a symbol.
    """

    value: int
    row_index: int
    span: range
    touched: bool


@dataclass
class SymbolToken:
    """A token representing a symbol in the engine schematic.

    Attributes
    ----------
    character : str
        The symbol character.
    row_index : int
        The row index of the token.
    column_index : int
        The column index of the token.
    gear_ratio : int | None
        The gear ratio of the token if it is a gear, otherwise None.
    """

    character: str
    row_index: int
    column_index: int
    gear_ratio: int | None


def main(args: Sequence[str]) -> None:
    """The main function.

    Parameters
    ----------
    args : Sequence[str]
        The command line arguments.
    """
    parsed_args = _parse_args(args)
    input_path: str = parsed_args.input_path
    num_trials: int = parsed_args.num_trials

    lines: Sequence[str] = []
    with open(input_path, "r", encoding="utf-8") as input_file:
        lines = input_file.readlines()

    # Visualisation
    part_tokens, symbol_tokens = tokenise(lines)
    touch_parts(part_tokens, symbol_tokens)
    find_gears(part_tokens, symbol_tokens)
    draw_grid(lines, part_tokens, symbol_tokens)

    total_time = 0
    result = 0

    # Part 1
    for _ in range(num_trials):
        start_time = time.perf_counter_ns()
        part_tokens, symbol_tokens = tokenise(lines)
        touch_parts(part_tokens, symbol_tokens)
        result = calculate_part_number_sum(part_tokens)
        total_time += time.perf_counter_ns() - start_time
    average_time = total_time / num_trials
    rprint(f"Part 1 - Result {result}")
    rprint(f"Part 1 took {format_execution_time(average_time)}")

    # Part 2
    for _ in range(num_trials):
        start_time = time.perf_counter_ns()
        part_tokens, symbol_tokens = tokenise(lines)
        find_gears(part_tokens, symbol_tokens)
        result = calculate_gear_ratio_sum(symbol_tokens)
        total_time += time.perf_counter_ns() - start_time
    average_time = total_time / num_trials
    rprint(f"Part 2 - Result {result}")
    rprint(f"Part 2 took {format_execution_time(average_time)}")


def calculate_part_number_sum(part_tokens: Sequence[PartNumberToken]) -> int:
    """Calculate the sum of the part numbers that are touching a symbol.

    Parameters
    ----------
    part_tokens : Sequence[PartNumberToken]
        The part number tokens. These are assumed to have been preprocessed i.e. all part numbers which are touching a symbol
        have been marked as touched.

    Returns
    -------
    int
        The sum of the part numbers that are touching a symbol.
    """
    total = 0
    # Sum the values that were touched
    for part_token in part_tokens:
        total += part_token.value if part_token.touched else 0
    return total


def calculate_gear_ratio_sum(symbol_tokens: Sequence[SymbolToken]) -> int:
    """Calculate the sum of the gear ratios.

    Parameters
    ----------
    symbol_tokens : Sequence[SymbolToken]
        The symbol tokens. These are assumed to have been preprocessed i.e. all symbols that are gears have their gear ratio
        already calculated.
    """
    total = 0
    # Sum the gear ratios
    for symbol_token in symbol_tokens:
        total += symbol_token.gear_ratio if symbol_token.gear_ratio is not None else 0
    return total


def touch_parts(
    part_tokens: Sequence[PartNumberToken],
    symbol_tokens: Sequence[SymbolToken],
) -> None:
    """Mark all part numbers that are touching a symbol as touched. This mutates the part tokens in place.

    Parameters
    ----------
    part_tokens : Sequence[PartNumberToken]
        The part number tokens.
    symbol_tokens : Sequence[SymbolToken]
        The symbol tokens.
    """
    for symbol_token in symbol_tokens:
        # Iterate over the 8 surrounding cells
        for row_offset in range(-1, 2):
            for column_offset in range(-1, 2):
                if row_offset == 0 and column_offset == 0:
                    continue
                row_index = symbol_token.row_index + row_offset
                column_index = symbol_token.column_index + column_offset
                # Check if the cell is a part number
                for part_token in part_tokens:
                    if (
                        part_token.row_index == row_index
                        and column_index in part_token.span
                        and not part_token.touched
                    ):
                        part_token.touched = True
                        break


def find_gears(
    part_tokens: Sequence[PartNumberToken], symbol_tokens: Sequence[SymbolToken]
) -> None:
    """Find all gears and calculate their gear ratio. This mutates the symbol tokens in place.

    Parameters
    ----------
    part_tokens : Sequence[PartNumberToken]
        The part number tokens.
    symbol_tokens : Sequence[SymbolToken]
        The symbol tokens.
    """
    for symbol_token in symbol_tokens:
        adjacent_parts: Sequence[PartNumberToken] = []
        # Iterate over the 8 surrounding cells
        for row_offset in range(-1, 2):
            for column_offset in range(-1, 2):
                if row_offset == 0 and column_offset == 0:
                    continue
                row_index = symbol_token.row_index + row_offset
                column_index = symbol_token.column_index + column_offset
                # Check if the cell is a part number
                for part_token in part_tokens:
                    if (
                        part_token.row_index == row_index
                        and column_index in part_token.span
                    ):
                        part_token.touched = True
                        if part_token not in adjacent_parts:
                            adjacent_parts.append(part_token)
                        break
        if len(adjacent_parts) == 2:
            symbol_token.gear_ratio = adjacent_parts[0].value * adjacent_parts[1].value


def tokenise(
    lines: Sequence[str],
) -> tuple[Sequence[PartNumberToken], Sequence[SymbolToken]]:
    """Tokenise the engine schematic.

    Parameters
    ----------
    lines : Sequence[str]
        The lines of the engine schematic.

    Returns
    -------
    part_tokens : Sequence[PartNumberToken]
        The part number tokens.
    symbol_tokens : Sequence[SymbolToken]
        The symbol tokens.
    """
    part_tokens: Sequence[PartNumberToken] = []
    symbol_tokens: Sequence[SymbolToken] = []
    # Tokenise
    for row_index, line in enumerate(lines):
        start_index = None
        for column_index, character in enumerate(line):
            # Number parsing
            is_digit = character.isdigit()
            if start_index is None and is_digit:
                start_index = column_index
            if start_index is not None and not is_digit:
                end_index = column_index
                value = int(line[start_index:end_index])
                token = PartNumberToken(
                    value, row_index, range(start_index, end_index), False
                )
                if token not in part_tokens:
                    part_tokens.append(token)
                start_index = None
            # Symbol parsing
            if not is_digit and character != "." and character != "\n":
                token = SymbolToken(character, row_index, column_index, None)
                if token not in symbol_tokens:
                    symbol_tokens.append(token)
    return (part_tokens, symbol_tokens)


def draw_grid(
    lines: Sequence[str],
    part_tokens: Sequence[PartNumberToken],
    symbol_tokens: Sequence[SymbolToken],
) -> None:
    """Draw the engine schematic grid.

    Parameters
    ----------
    lines : Sequence[str]
        The lines of the engine schematic.
    part_tokens : Sequence[PartNumberToken]
        The part number tokens.
    symbol_tokens : Sequence[SymbolToken]
        The symbol tokens.
    """
    # Draw the grid
    for row_index, line in enumerate(lines):
        for column_index, character in enumerate(line):
            is_digit = character.isdigit()
            # Draw symbols
            if not is_digit:
                if character == ".":
                    rprint(f"[black]{character}[/black]", end="")
                else:
                    # Draw gear
                    is_gear = False
                    for symbol_token in symbol_tokens:
                        if (
                            symbol_token.row_index == row_index
                            and symbol_token.column_index == column_index
                        ):
                            if symbol_token.gear_ratio is not None:
                                rprint(
                                    f"[bright white]{character}[/bright white]", end=""
                                )
                                is_gear = True
                    # Draw non-gears
                    if not is_gear:
                        rprint(f"[bold yellow]{character}[/bold yellow]", end="")
                continue

            # Draw part numbers
            is_touched = False
            for part_token in part_tokens:
                if (
                    part_token.row_index == row_index
                    and column_index in part_token.span
                    and part_token.touched
                ):
                    is_touched = True
                    break
            # Draw touched part numbers
            if is_touched:
                rprint(f"[bold green]{character}[/bold green]", end="")
            # Draw untouched part numbers
            else:
                rprint(f"[bold red]{character}[/bold red]", end="")
    rprint()


def _parse_args(args: Sequence[str]) -> argparse.Namespace:
    """Parse command line arguments.

    Parameters
    ----------
    args : Sequence[str]
        The command line arguments.

    Returns
    -------
    argparse.Namespace
        The parsed arguments.
    """
    parser = argparse.ArgumentParser(
        prog="day03_gear_ratios", description="Advent of Code 2023 Day 3 Gear Ratios"
    )
    parser.add_argument("input_path", type=str, help="Path to input file.")
    parser.add_argument(
        "-num_trials", "--num_trials", type=int, default=100, help="Number of trials."
    )
    return parser.parse_args(args)


if __name__ == "__main__":
    import sys

    main(sys.argv[1:])
