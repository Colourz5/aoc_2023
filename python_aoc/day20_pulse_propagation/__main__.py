"""This module contains the solution to Day 20 of Advent of Code 2023"""

# Standard libraries
import argparse
from dataclasses import dataclass
from enum import Enum
import math
import time
from typing import Sequence

# External libraries
from rich import print as rprint

# Internal libraries
from python_aoc.common import format_execution_time


BUTTON_PRESSER: str = "BUTTON_PRESSER"
"""A dummy module name that bootstraps the button press."""


class ModuleKind(Enum):
    """The types of modules.

    Variants
    --------
    BROADCASTER
        When it receives a signal, send the same signal to its destination modules.
    FLIP_FLOP
        A flip flop has an internal state, either ON or OFF, with it always starting as OFF.
        When it receives a low signal, switch states and send the resulting state to each of its
        destination modules.
    CONJUNCTION
        A conjunction remembers the last signal of each of its input modules. If all of the last
        signals were HIGH then send a LOW signal, otherwise send a HIGH signal to all of its
        destination modules.
    BUTTON
        When a button receives a signal by me, it sends LOW signals to the broadcaster module.
    UNTYPED
        An untyped module can only receive signals and does nothing.
    """

    BROADCASTER = 0
    FLIP_FLOP = 1
    CONJUNCTION = 2
    BUTTON = 3
    UNTYPED = 4


@dataclass
class Module:
    destinations: Sequence[str]
    state: bool
    kind: ModuleKind
    inputs: dict[str, bool]


def main(args: Sequence[str]) -> None:
    parsed_args = _parse_args(args)
    input_path: str = parsed_args.input_path
    num_trials: int = parsed_args.num_trials
    verbose_mode: bool = parsed_args.verbose
    super_verbose_mode: bool = parsed_args.super_verbose

    lines: Sequence[str] = []
    with open(input_path, "r", encoding="utf-8") as input_file:
        lines = input_file.readlines()

    # Part 1
    result: int | None = None
    total_time: int = 0
    for _ in range(num_trials):
        start_time: int = time.perf_counter_ns()
        result = count_pulses(lines, super_verbose_mode)
        total_time += time.perf_counter_ns() - start_time
    average_time: float = total_time / num_trials
    rprint(f"Part 1 - Result = {result}")
    rprint(f"Part 1 took {format_execution_time(average_time)}")

    # Part 2
    result: int | None = None
    total_time: int = 0
    for _ in range(num_trials):
        start_time: int = time.perf_counter_ns()
        result = count_presses_till_rx(lines, verbose_mode, super_verbose_mode)
        total_time += time.perf_counter_ns() - start_time
    average_time: float = total_time / num_trials
    rprint(f"Part 2 - Result = {result}")
    rprint(f"Part 2 took {format_execution_time(average_time)}")


def count_pulses(lines: Sequence[str], verbose_mode: bool) -> int:
    """Calculates the product of the number of low pulses and the number of high pulses sent after 1,000 button presses.
    Solves part 1 of Day 20 of Advent of Code 2023.

    Parameters
    ----------
    lines : Sequence[str]
        The lines of the input file.
    verbose_mode: bool
        Turn on verbose mode.

    Returns
    -------
    int
        The answer
    """
    NUM_REQUIRED_CYCLES: int = 1000
    modules = parse_modules(lines)
    low_count: int = 0
    high_count: int = 0
    # Simulate and count pulses
    for _ in range(NUM_REQUIRED_CYCLES):
        current_low_count, current_high_count = press_button_module(
            modules,
            verbose_mode,
        )
        low_count += current_low_count
        high_count += current_high_count

    return low_count * high_count


def count_presses_till_rx(
    lines: Sequence[str], verbose_mode: bool, super_verbose_mode: bool
) -> int:
    """Counts the number of required button presses to send the module `rx` a single low signal.
    Solves part 2 of Day 20 of Advent of Code 2023.

    Parameters
    ----------
    lines : Sequence[str]
        The lines of the input file.
    verbose_mode : bool
        Turn on verbose mode.
    super_verbose_mode : bool
        Turn on super verbose mode.

    Returns
    -------
    int
        The answer

    Notes
    -----
    This function has a lot of assumptions as the solution is hard to generalise.
    The assumptions are:
        - `rx` is a single input untyped module
        - the module that feeds into `rx` is a conjunction module (feeder module)
        - the input modules (critical modules) of the feeder module periodically send a single HIGH pulse to the feeder
        - the cycles are simple with the offset not being coprime to the period, i.e. we can just
            get the least common multiple of the periods of all the critical modules to get the answer.
    """
    modules: dict[str, Module] = parse_modules(lines)
    if "rx" not in modules:
        if verbose_mode:
            rprint("The module `rx` does not exist! Can't do part 2.")
        return 0
    assert "rx" in modules
    # Get the feeder module
    feeders = tuple(modules["rx"].inputs.keys())
    assert len(feeders) == 1, "This solver can only handle one feeder module into `rx`"
    (feed,) = feeders
    assert (
        modules[feed].kind == ModuleKind.CONJUNCTION
    ), "This solver can only handle a `conjunction` module as the feeder"
    # Modules that feed into the feeder module
    critical_modules = tuple(modules[feed].inputs.keys())
    # Track the times each critical module sends a HIGH signal to the feeder module
    feed_and_times: tuple[str, dict[str, list[int]]] = (
        feed,
        {name: [] for name in critical_modules},
    )
    if verbose_mode or super_verbose_mode:
        rprint(f"The feeder module is {feed}")
        rprint(f"with the critical modules being {critical_modules}")

    step_idx: int = 0
    while True:
        step_idx += 1
        # Press the button
        _ = press_button_module(
            modules,
            super_verbose_mode,
            step_count=step_idx,
            feed_and_times=feed_and_times,
        )
        # Collect the periods
        periods: dict[str, int] = {}
        for module, repeats in feed_and_times[1].items():
            if len(repeats) < 2:
                break
            period = repeats[1] - repeats[0]
            periods[module] = period
        # When we have all the periods, calculate the answer
        if len(periods) == len(feed_and_times[1]):
            if verbose_mode:
                for module, period in periods.items():
                    rprint(f"Module {module} has period {period}")
            return math.lcm(*periods.values())


def parse_modules(lines: Sequence[str]) -> dict[str, Module]:
    """Parse the lines into modules.

    Paramaters
    ----------
    lines : Sequence[str]
        The lines in the input.

    Returns
    -------
    modules : dict[str, Module]
        The parsed modules. The key is the name of the module.
    """
    modules: dict[str, Module] = {}
    for line in lines:
        line = line.rstrip("\n")
        tokens = line.split("->")
        module_name = tokens[0].strip()
        destinations = [token.strip() for token in tokens[1].split(", ")]
        if module_name.startswith("%"):
            name = module_name[1:]
            module_kind = ModuleKind.FLIP_FLOP
        elif module_name.startswith("&"):
            name = module_name[1:]
            module_kind = ModuleKind.CONJUNCTION
        elif module_name == "broadcaster":
            name = module_name
            module_kind = ModuleKind.BROADCASTER
        else:
            name = module_name
            module_kind = ModuleKind.UNTYPED
        modules[name] = Module(
            destinations=destinations,
            state=False,
            kind=module_kind,
            inputs={},
        )
    # Create button module
    modules["button"] = Module(
        destinations=["broadcaster"],
        state=False,
        kind=ModuleKind.BUTTON,
        inputs={},
    )
    # Parse input only modules
    new_modules: dict[str, Module] = {}
    for module_name, module in modules.items():
        for destination in module.destinations:
            if destination in modules:
                continue
            new_modules[destination] = Module(
                destinations=[],
                state=False,
                kind=ModuleKind.UNTYPED,
                inputs={},
            )
    modules.update(new_modules)
    # Fill out the inputs for all modules
    for module_name, module in modules.items():
        for destination in module.destinations:
            assert destination in modules
            modules[destination].inputs[module_name] = False
    assert (
        BUTTON_PRESSER not in modules
    ), f"Dummy trigger for button `{BUTTON_PRESSER}` will shadow an actual module"
    return modules


def press_button_module(
    modules: dict[str, Module],
    verbose_mode: bool,
    step_count: int = 0,
    feed_and_times: tuple[str, dict[str, list[int]]] | None = None,
) -> tuple[int, int]:
    """Presses the button module once and plays out all pulses triggered.

    Parameters
    ----------
    modules : dict[str, Module]
        The modules.
    verbose_mode: bool
        Turn on verbose mode.
    step_count : int, optional
        The current step count. This is used for part 2. Defaults to `0`.
    feed_and_times : tuple[str, dict[str, list[int]]] | None, optional
        The feeder module and the critical modules. The dictionary contains the critical
        modules and a list of whenever the critical modules sends a high signal to the feeder module.
        This is used for part 2. Defaults to `None`.

    Returns
    -------
    low_count : int
        The number of low pulses sent.
    high_count: int
        The number of high pulses sent.
    """
    # Pulse queue contains signals where each signal consists of:
    # 1. a destination module
    # 2. a source module
    # 3. the signal value itself
    pulse_queue: list[tuple[str, str, bool]] = [("button", BUTTON_PRESSER, False)]
    # Part 1 counters
    low_count: int = 0
    high_count: int = 0
    # Track pulse number
    pulse_idx: int = 0

    # Helper function
    def send_pulse(current_module: Module, output_signal: bool) -> None:
        nonlocal low_count
        nonlocal high_count
        nonlocal pulse_queue
        for next_dest_name in current_module.destinations:
            if verbose_mode:
                rprint(f"Sending {output_signal} to {next_dest_name}")
            # Send signal that is the same as the final state
            if output_signal:
                high_count += 1
            else:
                low_count += 1
            pulse_queue.append((next_dest_name, dest_name, output_signal))

    while len(pulse_queue) > 0:
        pulse_idx += 1
        dest_name, src_name, signal = pulse_queue.pop(0)
        current_module = modules[dest_name]
        if verbose_mode:
            rprint(f"Pulse {pulse_idx}")
            rprint(f"Processing module {dest_name}")
            rprint(f"receiving {signal} from {src_name}")

        # Part 2 related stuff
        if feed_and_times is not None:
            feed, periods = feed_and_times
            # We hit the feed module
            if dest_name == feed and signal:
                if src_name in periods:
                    if verbose_mode:
                        rprint(
                            f"Hit a critical module {src_name} that is sending {signal} to {dest_name} at press {step_count}"
                        )
                    periods[src_name].append(step_count)

        # Handle different module behaviour
        match current_module.kind:
            case ModuleKind.FLIP_FLOP:
                if verbose_mode:
                    rprint(
                        f"it is a flip flop with current_state {current_module.state}"
                    )
                # Flip flops ignore HIGH signals
                if signal:
                    continue
                # Flip state
                current_module.state = not current_module.state
                send_pulse(current_module, current_module.state)
            case ModuleKind.CONJUNCTION:
                # Update memory
                current_module.inputs[src_name] = signal
                if verbose_mode:
                    rprint("it is a conjunction")
                    rprint(f"receiving {signal} from {src_name}")
                    rprint("Memory is")
                    rprint(current_module.inputs)
                # Send LOW if all HIGH else HIGH
                send_pulse(current_module, not all(current_module.inputs.values()))
            case ModuleKind.BROADCASTER:
                assert src_name == "button"
                if verbose_mode:
                    rprint("it is the broadcaster module")
                # Forward signal to destination modules
                send_pulse(current_module, signal)
            case ModuleKind.BUTTON:
                assert src_name == BUTTON_PRESSER
                if verbose_mode:
                    rprint("it is the button module")
                # Send LOW to broadcaster
                send_pulse(current_module, False)
            # Untyped does nothing
            case ModuleKind.UNTYPED:
                if verbose_mode:
                    rprint("it is an untyped module")
                pass

        if verbose_mode:
            rprint()
    return (low_count, high_count)


def _parse_args(args: Sequence[str]) -> argparse.Namespace:
    """Parses the command line arguments

    Parameters
    ----------
    args : Sequence[str]
        The command line arguments.

    Returns
    -------
    parsed_args : argparse.Namespace
        The parsed command line arguments.
    """
    parser = argparse.ArgumentParser(
        prog="aoc 2023 day 20",
        description="Program to solve Day 20 of Advent of Code 2023",
    )
    parser.add_argument("input_path", type=str, help="Path to input file")
    parser.add_argument(
        "-num_trials",
        "--num_trials",
        type=int,
        default=100,
        help="Number of trials to use when measuring run time",
    )
    parser.add_argument(
        "-v",
        "--v",
        "-verbose",
        "--verbose",
        action="store_true",
        dest="verbose",
        help="Turn on verbose mode",
    )
    parser.add_argument(
        "-sv",
        "--sv",
        "-sverbose",
        "--sverbose",
        action="store_true",
        dest="super_verbose",
        help="Turn on super verbose mode",
    )
    return parser.parse_args(args)


if __name__ == "__main__":
    import sys

    main(sys.argv[1:])
