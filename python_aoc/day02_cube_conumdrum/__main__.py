"""Advent of Code Day 2: Cube Conundrum"""

# Standard libraries
import argparse
import functools
import operator
from typing import Sequence
import time

# External libraries
from rich import print as rprint

# Internal libraries
from ..common import format_execution_time


ACTUAL_CUBES_IN_BAG: dict[str, int] = {
    "red": 12,
    "green": 13,
    "blue": 14,
}
"""dict[str, int]: The actual number of cubes in the bag for each colour. This determines whether a game is possible."""


def main(args: Sequence[str]) -> None:
    """Main function for Day 2 of AOC 2023.

    Parameters
    ----------
    args : Sequence[str]
        The command line arguments.
    """
    parsed_args = _parse_args(args)
    input_path: str = parsed_args.input_path
    num_trials: int = parsed_args.num_trials

    lines: Sequence[str] = []
    with open(input_path, "r", encoding="utf-8") as input_file:
        lines = input_file.readlines()

    # Part 1
    total_time = 0
    result = 0
    for _ in range(num_trials):
        start_time = time.perf_counter_ns()
        result = sum_possible_game_ids(lines)
        total_time += time.perf_counter_ns() - start_time
    average_time = total_time / num_trials
    rprint(f"Part 1 - Result = {result}")
    rprint(f"Part 1 took {format_execution_time(average_time)}")

    # Part 2
    total_time = 0
    result = 0
    for _ in range(num_trials):
        start_time = time.perf_counter_ns()
        result = sum_game_power(lines)
        total_time += time.perf_counter_ns() - start_time
    average_time = total_time / num_trials
    rprint(f"Part 2 - Result = {result}")
    rprint(f"Part 2 took {format_execution_time(average_time)}")


def sum_possible_game_ids(lines: Sequence[str]) -> int:
    """Calculates the sum of all possible game IDs. This is the solution to Part 1 of AOC 2023 Day 2.

    Parameters
    ----------
    lines : Sequence[str]
        The lines of the input file.

    Returns
    -------
    int
        The sum of all possible game IDs.
    """
    total = 0
    for line in lines:
        tokens = line.split(" ")
        game_id = int(tokens[1][:-1])
        # Is game possible?
        max_cubes: dict[str, int] = {
            "red": 0,
            "green": 0,
            "blue": 0,
        }
        num_entries = (len(tokens) - 2) // 2
        for entry_idx in range(num_entries):
            num_cubes = int(tokens[2 * (entry_idx + 1)])
            cube_colour = tokens[2 * (entry_idx + 1) + 1].rstrip(",;\n")
            assert cube_colour in max_cubes
            max_cubes[cube_colour] = max(max_cubes[cube_colour], num_cubes)
        possible = True
        for cube_colour, max_cube in max_cubes.items():
            if max_cube > ACTUAL_CUBES_IN_BAG[cube_colour]:
                possible = False
                break
        total += game_id if possible else 0
    return total


def sum_game_power(lines: Sequence[str]) -> int:
    """Calculates the sum of all game powers. This is the solution to Part 2 of AOC 2023 Day 2.

    Parameters
    ----------
    lines : Sequence[str]
        The lines of the input file.

    Returns
    -------
    int
        The sum of all game powers.

    Notes
    -----
    The game power is defined as the product of the maximum number of cubes of each colour in a given game.
    """
    total = 0
    for line in lines:
        tokens = line.split(" ")
        max_cubes: dict[str, int] = {
            "red": 0,
            "green": 0,
            "blue": 0,
        }
        num_entries = (len(tokens) - 2) // 2
        for entry_idx in range(num_entries):
            num_cubes = int(tokens[2 * (entry_idx + 1)])
            cube_colour = tokens[2 * (entry_idx + 1) + 1].rstrip(",;\n")
            assert cube_colour in max_cubes
            max_cubes[cube_colour] = max(max_cubes[cube_colour], num_cubes)

        total += functools.reduce(operator.mul, max_cubes.values(), 1)
    return total


def _parse_args(args: Sequence[str]) -> argparse.Namespace:
    """Parses the command line arguments.

    Parameters
    ----------
    args : Sequence[str]
        The command line arguments.
    """
    parser = argparse.ArgumentParser(prog="day2-cubeconumdrum")
    parser.add_argument("input_path", type=str, help="Path to input file.")
    parser.add_argument(
        "-num_trials",
        "--num_trials",
        type=int,
        default=100,
        help="Number of trials to run.",
    )
    return parser.parse_args(args)


if __name__ == "__main__":
    import sys

    main(sys.argv[1:])
