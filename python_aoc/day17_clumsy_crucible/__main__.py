"""This module contains the solution to Day 17 of Advent of Code 2023"""

# Standard libraries
import argparse
from heapq import heappush, heappop
import time
from typing import Sequence

# External libraries
from rich import print as rprint

# Internal libraries
from python_aoc.common import format_execution_time

type Node = tuple[int, int, int, int, int, int]
type SeenNode = tuple[int, int, int, int, int]


def main(args: Sequence[str]) -> None:
    parsed_args = _parse_args(args)
    input_path: str = parsed_args.input_path
    num_trials: int = parsed_args.num_trials
    verbose_mode: bool = parsed_args.verbose

    lines: Sequence[str] = []
    with open(input_path, "r", encoding="utf-8") as input_file:
        lines = input_file.readlines()

    # Part 1
    result: int | None = None
    total_time: int = 0
    for _ in range(num_trials):
        start_time: int = time.perf_counter_ns()
        result = part1(lines, verbose_mode)
        total_time += time.perf_counter_ns() - start_time
    average_time: float = total_time / num_trials
    rprint(f"Part 1 - Result = {result}")
    rprint(f"Part 1 took {format_execution_time(average_time)}")

    # Part 2
    result: int | None = None
    total_time: int = 0
    for _ in range(num_trials):
        start_time: int = time.perf_counter_ns()
        result = part2(lines, verbose_mode)
        total_time += time.perf_counter_ns() - start_time
    average_time: float = total_time / num_trials
    rprint(f"Part 2 - Result = {result}")
    rprint(f"Part 2 took {format_execution_time(average_time)}")


def part1(lines: Sequence[str], verbose_mode: bool) -> int:
    """Part 1 of Day 17 of Advent of Code 2023.

    Parameters
    ----------
    lines : Sequence[str]
        The lines of the input file.
    verbose : bool
        Turn on verbose mode.

    Returns
    -------
    int
        The answer
    """
    grid: Sequence[Sequence[int]] = [
        [int(cell) for cell in line.rstrip("\n")] for line in lines
    ]
    assert len(grid) > 0
    assert len(grid[0]) > 0
    width: int = len(grid[0])
    height: int = len(grid)
    total_cells: int = width * height

    work_queue: list[Node] = [(0, 0, 0, 0, 0, 1)]
    seen: set[SeenNode] = set()

    iteration: int = 0
    while len(work_queue) > 0:
        iteration += 1
        if verbose_mode:
            rprint(f"Iteration {iteration} with {len(work_queue)} nodes")
            rprint(f"Seen {len(seen)} out of a grid of {total_cells} nodes")
        cost, row_idx, column_idx, row_offset, column_offset, step_count = heappop(
            work_queue
        )
        if row_idx == height - 1 and column_idx == width - 1:
            return cost
        seen_node = (row_idx, column_idx, row_offset, column_offset, step_count)
        if seen_node in seen:
            continue

        seen.add(seen_node)

        # Can move straight
        if step_count < 3 and (row_offset, column_offset) != (0, 0):
            target_row = row_idx + row_offset
            target_column = column_idx + column_offset
            if (
                target_row < 0
                or target_row >= height
                or target_column < 0
                or target_column >= width
            ):
                pass
            else:
                heappush(
                    work_queue,
                    (
                        cost + grid[target_row][target_column],
                        target_row,
                        target_column,
                        row_offset,
                        column_offset,
                        step_count + 1,
                    ),
                )

        # Turns
        for row_turn, column_turn in ((-1, 0), (+1, 0), (0, -1), (0, +1)):
            if (row_turn, column_turn) == (row_offset, column_offset) or (
                row_turn,
                column_turn,
            ) == (-row_offset, -column_offset):
                continue
            target_row = row_idx + row_turn
            target_column = column_idx + column_turn
            if (
                target_row < 0
                or target_row >= height
                or target_column < 0
                or target_column >= width
            ):
                continue
            heappush(
                work_queue,
                (
                    cost + grid[target_row][target_column],
                    target_row,
                    target_column,
                    row_turn,
                    column_turn,
                    1,
                ),
            )

    raise ValueError("Failed to reach the end!")


def part2(lines: Sequence[str], verbose_mode: bool) -> int:
    """Part 2 of Day 17 of Advent of Code 2023.

    Parameters
    ----------
    lines : Sequence[str]
        The lines of the input file.
    verbose : bool
        Turn on verbose mode.

    Returns
    -------
    int
        The answer
    """
    grid: Sequence[Sequence[int]] = [
        [int(cell) for cell in line.rstrip("\n")] for line in lines
    ]
    assert len(grid) > 0
    assert len(grid[0]) > 0
    width: int = len(grid[0])
    height: int = len(grid)
    total_cells: int = width * height

    work_queue: list[Node] = [(0, 0, 0, 0, 0, 0)]
    seen: set[SeenNode] = set()

    iteration: int = 0
    while len(work_queue) > 0:
        iteration += 1
        if verbose_mode:
            rprint(f"Iteration {iteration} with {len(work_queue)} nodes")
            rprint(f"Seen {len(seen)} out of a grid of {total_cells} nodes")
        cost, row_idx, column_idx, row_offset, column_offset, step_count = heappop(
            work_queue
        )
        if row_idx == height - 1 and column_idx == width - 1:
            return cost
        seen_node = (row_idx, column_idx, row_offset, column_offset, step_count)
        if seen_node in seen:
            continue

        seen.add(seen_node)

        # Can move straight
        if step_count < 10 and (row_offset, column_offset) != (0, 0):
            target_row = row_idx + row_offset
            target_column = column_idx + column_offset
            if (
                target_row < 0
                or target_row >= height
                or target_column < 0
                or target_column >= width
            ):
                pass
            else:
                heappush(
                    work_queue,
                    (
                        cost + grid[target_row][target_column],
                        target_row,
                        target_column,
                        row_offset,
                        column_offset,
                        step_count + 1,
                    ),
                )

        # Turns
        if step_count == 0 or step_count > 3:
            for row_turn, column_turn in ((-1, 0), (+1, 0), (0, -1), (0, +1)):
                if (row_turn, column_turn) == (row_offset, column_offset) or (
                    row_turn,
                    column_turn,
                ) == (-row_offset, -column_offset):
                    continue
                target_row = row_idx + row_turn
                target_column = column_idx + column_turn
                if (
                    target_row < 0
                    or target_row >= height
                    or target_column < 0
                    or target_column >= width
                ):
                    continue
                heappush(
                    work_queue,
                    (
                        cost + grid[target_row][target_column],
                        target_row,
                        target_column,
                        row_turn,
                        column_turn,
                        1,
                    ),
                )

    raise ValueError("Failed to reach the end!")


def _parse_args(args: Sequence[str]) -> argparse.Namespace:
    """Parses the command line arguments

    Parameters
    ----------
    args : Sequence[str]
        The command line arguments.

    Returns
    -------
    parsed_args : argparse.Namespace
        The parsed command line arguments.
    """
    parser = argparse.ArgumentParser(
        prog="aoc XXXX day XX",
        description="Program to solve Day XX of Advent of Code XXXX",
    )
    parser.add_argument("input_path", type=str, help="Path to input file")
    parser.add_argument(
        "-num_trials",
        "--num_trials",
        type=int,
        default=100,
        help="Number of trials to use when measuring run time",
    )
    parser.add_argument(
        "-v",
        "--v",
        "-verbose",
        "--verbose",
        action="store_true",
        dest="verbose",
        help="Turn on verbose mode",
    )
    return parser.parse_args(args)


if __name__ == "__main__":
    import sys

    main(sys.argv[1:])
