"""This module contains the solution to Day 7 of AOC 2023.

The approach taken by this solution was inspired by the Reddit user
`jitwit` on the Advent of Code subreddit on the Day 7 AOC 2023 thread
"""
from __future__ import annotations

# Standard libraries
import argparse
from collections import Counter
import time
from typing import Sequence

# External libraries
from rich import print as rprint

# Internal libraries
from python_aoc.common import format_execution_time

JACK_ORDERING: str = "23456789TJQKA"
JOKER_ORDERING: str = "J23456789TQKA"


def main(args: Sequence[str]) -> None:
    parsed_args = _parse_args(args)
    input_path: str = parsed_args.input_path
    num_trials: int = parsed_args.num_trials

    lines: Sequence[str] = []
    with open(input_path, "r", encoding="utf-8") as input_file:
        lines = input_file.readlines()

    # Part 1
    result = 0
    total_time = 0
    for _ in range(num_trials):
        start_time = time.perf_counter_ns()
        result = calculate_total_winnings(lines, joker_mode=False)
        total_time += time.perf_counter_ns() - start_time
    average_time = total_time / num_trials
    rprint(f"Part 1 - Result = {result}")
    rprint(f"Part 1 took {format_execution_time(average_time)}")

    # Part 2
    result = 0
    total_time = 0
    for _ in range(num_trials):
        start_time = time.perf_counter_ns()
        result = calculate_total_winnings(lines, joker_mode=True)
        total_time += time.perf_counter_ns() - start_time
    average_time = total_time / num_trials
    rprint(f"Part 2 - Result = {result}")
    rprint(f"Part 2 took {format_execution_time(average_time)}")


def calculate_total_winnings(lines: Sequence[str], joker_mode: bool) -> int:
    def card_strength(cards: str, ordering: str) -> int:
        assert len(str(len(ordering))) < 3
        digits = [str(ordering.index(card)).rjust(2, "0") for card in cards]
        return int("".join(digits))

    def hand_strength(cards: str, joker_mode: bool) -> int:
        card_counts = Counter(cards)
        if joker_mode:
            num_jokers = card_counts["J"]
            # Remove Jokers
            del card_counts["J"]
            if len(card_counts) == 0:
                card_counts["J"] = 0
            # Add to most common
            most_common, num_common = card_counts.most_common(1)[0]
            card_counts[most_common] = num_common + num_jokers
        assert card_counts.total() == 5
        counts_list = [str(count) for _, count in card_counts.most_common()]
        counts = "".join(counts_list).ljust(5, "0")
        return int(counts)

    result: int = 0
    registry: list[tuple[str, int]] = []
    ordering: str = JOKER_ORDERING if joker_mode else JACK_ORDERING
    for line in lines:
        tokens = line.split()
        cards = tokens[0]
        assert len(cards) == 5
        bid = int(tokens[1])
        registry.append((cards, bid))

    registry.sort(
        key=lambda x: (
            hand_strength(x[0], joker_mode=joker_mode),
            card_strength(x[0], ordering=ordering),
        )
    )
    for idx, (cards, bid) in enumerate(registry):
        result += (idx + 1) * bid
    return result


def _parse_args(args: Sequence[str]) -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        prog="aoc 2023 day 07", description="This program solves Day 7 of AOC 2023"
    )
    parser.add_argument("input_path", type=str, help="Path to the input file")
    parser.add_argument(
        "-num_trials",
        "--num_trials",
        type=int,
        default=100,
        help="Number of trials to run per part",
    )
    return parser.parse_args(args)


if __name__ == "__main__":
    import sys

    main(sys.argv[1:])
