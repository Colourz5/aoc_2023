"""This module contains the solution to Day 7 of AOC 2023"""
from __future__ import annotations

# Standard libraries
import argparse
from collections import Counter
from enum import IntEnum
from itertools import combinations_with_replacement
import time
from typing import Sequence

# External libraries
from rich import print as rprint
from python_aoc.common import format_execution_time

JACK_ORDERING: str = "AKQJT98765432"
JOKER_ORDERING: str = "AKQT98765432J"
TRANSFORMABLE_CARDS: str = "AKQT98765432"


class HandKind(IntEnum):
    HIGH_CARD = 0
    ONE_PAIR = 1
    TWO_PAIR = 2
    THREE_OF_A_KIND = 3
    FULL_HOUSE = 4
    FOUR_OF_A_KIND = 5
    FIVE_OF_A_KIND = 6

    def __str__(self) -> str:
        match self:
            case HandKind.HIGH_CARD:
                return "High Card"
            case HandKind.ONE_PAIR:
                return "One Pair"
            case HandKind.TWO_PAIR:
                return "Two Pair"
            case HandKind.THREE_OF_A_KIND:
                return "Three of a Kind"
            case HandKind.FULL_HOUSE:
                return "Full House"
            case HandKind.FOUR_OF_A_KIND:
                return "Four of a Kind"
            case HandKind.FIVE_OF_A_KIND:
                return "Five of a Kind"


class Hand:
    def __init__(self, cards: str, bid: int, joker_mode: bool):
        assert len(cards) == 5
        if joker_mode:
            self.ordering: str = JOKER_ORDERING
            self.kind: HandKind = parse_hand_joker(cards)
        else:
            self.ordering: str = JACK_ORDERING
            self.kind: HandKind = parse_hand(cards)
        self.cards: str = cards
        self.bid = bid

    def __repr__(self) -> str:
        return f"Hand: {self.cards} Bid: ${self.bid} Kind: {str(self.kind)}"

    def __eq__(self, other: object) -> bool:
        assert isinstance(other, Hand)
        return self.kind == other.kind and self.cards == other.cards

    def __lt__(self, other: object) -> bool:
        assert isinstance(other, Hand)
        assert len(self.cards) == len(other.cards)
        if self.kind == other.kind:
            for our_card, their_card in zip(self.cards, other.cards):
                if our_card == their_card:
                    continue
                our_power = self.ordering.index(our_card)
                their_power = self.ordering.index(their_card)
                if their_power < our_power:
                    return True
                else:
                    return False
            return True
        else:
            return self.kind < other.kind


def main(args: Sequence[str]) -> None:
    parsed_args = _parse_args(args)
    input_path: str = parsed_args.input_path
    num_trials: int = parsed_args.num_trials

    lines: Sequence[str] = []
    with open(input_path, "r", encoding="utf-8") as input_file:
        lines = input_file.readlines()

    # Part 1
    result = 0
    total_time = 0
    for _ in range(num_trials):
        start_time = time.perf_counter_ns()
        result = calculate_total_winnings(lines, joker_mode=False)
        total_time += time.perf_counter_ns() - start_time
    average_time = total_time / num_trials
    rprint(f"Part 1 - Result = {result}")
    rprint(f"Part 1 took {format_execution_time(average_time)}")

    # Part 2
    result = 0
    total_time = 0
    for _ in range(num_trials):
        start_time = time.perf_counter_ns()
        result = calculate_total_winnings(lines, joker_mode=True)
        total_time += time.perf_counter_ns() - start_time
    average_time = total_time / num_trials
    rprint(f"Part 2 - Result = {result}")
    rprint(f"Part 2 took {format_execution_time(average_time)}")


def calculate_total_winnings(lines: Sequence[str], joker_mode: bool) -> int:
    result: int = 0
    registry: list[Hand] = []
    for line in lines:
        tokens = line.split()
        cards = tokens[0]
        assert len(cards) == 5
        bid = int(tokens[1])
        registry.append(Hand(cards, bid, joker_mode))
    registry.sort()
    for idx, hand in enumerate(registry):
        rank = idx + 1
        result += rank * hand.bid
    return result


def parse_hand(hand: str) -> HandKind:
    card_counts = Counter(hand)
    num_distinct = len(card_counts)
    # Five of a kind
    if num_distinct == 1:
        return HandKind.FIVE_OF_A_KIND
    # Four of a kind
    elif num_distinct == 2:
        _, num_common = card_counts.most_common(1)[0]
        if num_common == 4:
            return HandKind.FOUR_OF_A_KIND
        else:
            return HandKind.FULL_HOUSE
    elif num_distinct == 3:
        _, num_common = card_counts.most_common(1)[0]
        if num_common == 3:
            return HandKind.THREE_OF_A_KIND
        else:
            return HandKind.TWO_PAIR
    elif num_distinct == 4:
        return HandKind.ONE_PAIR
    else:
        return HandKind.HIGH_CARD


def parse_hand_joker(hand: str) -> HandKind:
    if "J" in hand:
        num_jokers = hand.count("J")
        kind = HandKind.HIGH_CARD
        other_cards: str = "".join([card for card in hand if card != "J"])
        for joker_replacements in combinations_with_replacement(
            TRANSFORMABLE_CARDS, num_jokers
        ):
            new_cards = other_cards + "".join(joker_replacements)
            new_kind = parse_hand(new_cards)
            kind = max(kind, new_kind)
        return kind
    else:
        return parse_hand(hand)


def _parse_args(args: Sequence[str]) -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        prog="aoc 2023 day 07", description="This program solves Day 7 of AOC 2023"
    )
    parser.add_argument("input_path", type=str, help="Path to the input file")
    parser.add_argument(
        "-num_trials",
        "--num_trials",
        type=int,
        default=100,
        help="Number of trials to run for each part",
    )
    return parser.parse_args(args)


if __name__ == "__main__":
    import sys

    main(sys.argv[1:])
