"""This module contains the solution to Day 08 of Advent of Code 2023"""

# Standard libraries
import argparse
from dataclasses import dataclass
from functools import reduce
import math
import time
from typing import Sequence

# External libraries
from rich import print as rprint

# Internal libraries
from python_aoc.common import format_execution_time


@dataclass
class Ghost:
    """A struct containing data pertaining to a ghost.

    Attributes
    ----------
    location : str
        The current location.
    past_locations : list[str]
        A list of the past locations
    past_moves : list[int]
        A list of indices to the move used in the past.
    start_cycle : int
        The index where the start of the first cycle appears.
    end_cycle : int
        The index where the end of the first cycle appears.
    exits : list[int]
        A list of indices into the `past_locations` list where an exit is.
    done : bool
        A flag to signify that the ghost's cycle has been found.
    """

    location: str
    past_locations: list[str]
    past_moves: list[int]
    start_cycle: int
    end_cycle: int
    exits: list[int]
    done: bool


def main(args: Sequence[str]) -> None:
    parsed_args = _parse_args(args)
    input_path: str = parsed_args.input_path
    num_trials: int = parsed_args.num_trials
    verbose_mode: bool = parsed_args.verbose

    lines: Sequence[str] = []
    with open(input_path, "r", encoding="utf-8") as input_file:
        lines = input_file.readlines()

    # Part 1
    result: int | None = None
    total_time: int = 0
    for _ in range(num_trials):
        start_time: int = time.perf_counter_ns()
        result = calculate_number_of_steps(lines, verbose_mode)
        total_time += time.perf_counter_ns() - start_time
    average_time: float = total_time / num_trials
    rprint(f"Part 1 - Result = {result}")
    rprint(f"Part 1 took {format_execution_time(average_time)}")

    # Part 2
    result: int | None = None
    total_time: int = 0
    for _ in range(num_trials):
        start_time: int = time.perf_counter_ns()
        result = calculate_number_of_steps_parallel(lines, verbose_mode)
        total_time += time.perf_counter_ns() - start_time
    average_time: float = total_time / num_trials
    rprint(f"Part 2 - Result = {result}")
    rprint(f"Part 2 took {format_execution_time(average_time)}")


def calculate_number_of_steps(lines: Sequence[str], verbose_mode: bool) -> int:
    """Calculates the number of steps needed to go from AAA to ZZZ. Solves part 1 of Day 08 of Advent of Code 2023.

    Parameters
    ----------
    lines : Sequence[str]
        The lines of the input file.
    verbose : bool
        Turn on verbose mode.

    Returns
    -------
    int
       The number of steps needed to go from AAA to ZZZ.
    """
    result: int = 0
    moves: str = ""
    locations: dict[str, tuple[str, str]] = {}
    for line in lines:
        line = line.rstrip("\n")
        tokens = line.split()
        if len(tokens) == 0:
            continue
        elif len(tokens) == 1:
            moves = tokens[0]
        elif len(tokens) == 4:
            location = tokens[0]
            left = tokens[2][1:-1]
            right = tokens[3][:-1]
            locations[location] = (left, right)
        else:
            rprint(f"Unexpected number of tokens {len(tokens)}")
            return 0

    # Start location
    location = "AAA"
    if location not in locations:
        rprint(f"Location {location} not found in map!")
        return 0
    while location != "ZZZ":
        for move in moves:
            left, right = locations[location]
            match move:
                case "L":
                    new_location = left
                case "R":
                    new_location = right
                case _:
                    rprint(f"Invalid move {move}")
                    return 0
            if verbose_mode:
                rprint(f"Moving from location {location} to {new_location}")
            location = new_location
            result += 1
    if verbose_mode:
        rprint(f"The locations are {locations}")
    return result


def calculate_number_of_steps_parallel(lines: Sequence[str], verbose_mode: bool) -> int:
    """Calculates the number of steps needed for all ghosts to be on an exit.
    Solves part 2 of Day 08 of Advent of Code 2023.
    WARNING: This solution does not work for inputs allowed in the problem brief.
    It just so happens that all the criteria necessary for the LCM to be the answer is satisfied.

    Parameters
    ----------
    lines : Sequence[str]
        The lines of the input file.
    verbose_mode : bool
        Turn on verbose mode.

    Returns
    -------
    int
        The answer
    """
    moves: str = ""
    locations: dict[str, tuple[str, str]] = {}
    ghosts: list[Ghost] = []
    for line in lines:
        line = line.rstrip("\n")
        tokens = line.split()
        if len(tokens) == 0:
            continue
        elif len(tokens) == 1:
            moves = tokens[0]
        elif len(tokens) == 4:
            location = tokens[0]
            left = tokens[2][1:-1]
            right = tokens[3][:-1]
            locations[location] = (left, right)
            if location[2] == "A":
                ghosts.append(Ghost(location, [], [], 0, 0, [], False))
        else:
            rprint(f"Unexpected number of tokens {len(tokens)}")
            return 0

    cycle_count = 0
    while True:
        complete = True
        # Iterate over all the moves
        for move_idx, move in enumerate(moves):
            # Move each ghost with the current move
            for ghost_idx, ghost in enumerate(ghosts):
                # Skip ghosts where we have already found the cycle
                if ghost.done:
                    continue
                # Move the ghost
                location = ghost.location
                left, right = locations[location]
                match move:
                    case "L":
                        new_location = left
                    case "R":
                        new_location = right
                    case _:
                        rprint(f"Invalid move {move}")
                        return 0
                if verbose_mode:
                    rprint(
                        f"Moving ghost {ghost_idx} from location {location} to {new_location}"
                    )
                # We haven't reached the end yet
                if new_location[2] != "Z":
                    complete = False
                # We might have hit a cycle
                if location in ghost.past_locations:
                    start_index = ghost.past_locations.index(location)
                    # Check that the move index is the same
                    start_move = ghost.past_moves[start_index]
                    if start_move == move_idx:
                        # Calculate the current move count
                        end_index = cycle_count * len(moves) + move_idx
                        ghost.start_cycle = start_index
                        ghost.end_cycle = end_index
                        # Mark done
                        ghost.done = True
                        # NOTE: This isn't used but it was useful to help figure out how to solve the problem
                        # i.e. use LCM
                        # Find the exits inbetween the start and end cycle
                        location_slice = ghost.past_locations[start_index:end_index]
                        for loc_idx, exit_location in enumerate(location_slice):
                            if exit_location[2] == "Z":
                                ghost.exits.append(loc_idx + start_index)
                # Store the moves and locations
                ghost.past_locations.append(location)
                ghost.past_moves.append(move_idx)
                # Update the location
                ghost.location = new_location
        # Leave loop once we have found all cycles or reached the exit
        if complete:
            break
        cycle_count += 1
    if verbose_mode:
        rprint(f"The locations are {locations}")
    # Calculate the periods of each ghost's cycle.
    # NOTE: Luckily the cycles are all on the exits and there is only one exit in each cycle.
    #       Otherwise, this solution is not necessarily correct.
    periods = [ghost.end_cycle - ghost.start_cycle for ghost in ghosts]
    # Calculate least common multiple
    common_divisor = reduce(math.gcd, periods)
    base_periods = [period // common_divisor for period in periods]
    common_multiple = reduce(lambda x, y: x * y, base_periods)
    # LCM
    answer: int = common_divisor * common_multiple
    return answer


def _parse_args(args: Sequence[str]) -> argparse.Namespace:
    """Parses the command line arguments

    Parameters
    ----------
    args : Sequence[str]
        The command line arguments.

    Returns
    -------
    parsed_args : argparse.Namespace
        The parsed command line arguments.
    """
    parser = argparse.ArgumentParser(
        prog="aoc XXXX day XX",
        description="Program to solve Day XX of Advent of Code XXXX",
    )
    parser.add_argument("input_path", type=str, help="Path to input file")
    parser.add_argument(
        "-num_trials",
        "--num_trials",
        type=int,
        default=100,
        help="Number of trials to use when measuring run time",
    )
    parser.add_argument(
        "-v",
        "--v",
        "-verbose",
        "--verbose",
        action="store_true",
        dest="verbose",
        help="Turn on verbose mode",
    )
    return parser.parse_args(args)


if __name__ == "__main__":
    import sys

    main(sys.argv[1:])
