const fs = require("fs");

function calculateTotalScore(lines) {
    var totalScore = 0;
    for (let i = 0; i < lines.length; i++) {
        const tokens = lines[i].match(/\S+/g);
        var winningNumbers = [];
        var scratchNumbers = [];
        var winningFlag = true;
        for (let j = 2; j < tokens.length; j++) {
            if (tokens[j] == "|") {
                winningFlag = false;
                continue;
            }
            if (winningFlag) {
                winningNumbers.push(parseInt(tokens[j]));
            } else {
                scratchNumbers.push(parseInt(tokens[j]));
            }
        }
        var currentScore = 0;
        for (let j = 0; j < scratchNumbers.length; j++) {
            if (winningNumbers.includes(scratchNumbers[j])) {
                if (currentScore == 0) {
                    currentScore = 1;
                } else {
                    currentScore *= 2;
                }
            }
        }
        totalScore += currentScore;
    }
    return totalScore;
}

function recurseCards(cardRegistry, cardNumber, depth) {
    const numMatching = cardRegistry[cardNumber - 1];
    if (numMatching == 0) {
        return 1;
    } else {
        var totalCards = 1;
        for (let i = cardNumber + 1; i <= cardNumber + numMatching; i++) {
            totalCards += recurseCards(cardRegistry, i, depth + 1);
        }
        return totalCards;
    }
}

function calculateActualScore(lines) {
    var cardRegistry = [];
    for (let i = 0; i < lines.length; i++) {
        const tokens = lines[i].match(/\S+/g);
        const cardNumber = parseInt(tokens[1].replace(":", ""))
        var winningNumbers = [];
        var scratchNumbers = [];
        var winningFlag = true;
        for (let j = 2; j < tokens.length; j++) {
            if (tokens[j] == "|") {
                winningFlag = false;
                continue;
            }
            if (winningFlag) {
                winningNumbers.push(parseInt(tokens[j]));
            } else {
                scratchNumbers.push(parseInt(tokens[j]));
            }
        }
        var numMatching = 0;
        for (let j = 0; j < scratchNumbers.length; j++) {
            numMatching += winningNumbers.includes(scratchNumbers[j]) ? 1 : 0;
        }
        cardRegistry.push(numMatching);
    }
    var result = 0;
    for (let i = 0; i < cardRegistry.length; i++) {
        const cardNumber = i + 1;
        result += recurseCards(cardRegistry, cardNumber, 0);
    }

    return result;
}

function main() {
    const args = process.argv.slice(2);
    if (args.length < 1) {
        console.log("Please give a path to the input file!");
        return;
    }
    const inputPath = args[0];
    if (!fs.existsSync(inputPath)) {
        console.log("Could not find file at path %s", inputPath);
        return
    }
    const content = fs.readFileSync(inputPath, "utf-8");
    const lines = content.replaceAll("\r\n", "\n").split("\n");

    // Part 1
    var result = calculateTotalScore(lines);
    console.log("Part 1 - Result = %d", result);

    // Part 2
    result = calculateActualScore(lines);
    console.log("Part 2 - Result = %d", result);
}

main()