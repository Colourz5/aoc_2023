package main

import (
	"fmt"
	"os"
	"slices"
	"strconv"
	"strings"
	"time"
	"unicode"
)

type PartNumberToken struct {
	value       int
	row_index   int
	start_index int
	end_index   int
	touched     bool
}

type SymbolToken struct {
	character    rune
	row_index    int
	column_index int
}

type Coordinate struct {
	row_index    int
	column_index int
}

func get_neighbour_indices(coordinate Coordinate) []Coordinate {
	neighbours := []Coordinate{
		// Top left
		{
			row_index:    coordinate.row_index - 1,
			column_index: coordinate.column_index - 1,
		},
		// Top
		{
			row_index:    coordinate.row_index - 1,
			column_index: coordinate.column_index,
		},
		// Top right
		{
			row_index:    coordinate.row_index - 1,
			column_index: coordinate.column_index + 1,
		},
		// Bottom left
		{
			row_index:    coordinate.row_index + 1,
			column_index: coordinate.column_index - 1,
		},
		// Bottom
		{
			row_index:    coordinate.row_index + 1,
			column_index: coordinate.column_index,
		},
		// Bottom right
		{
			row_index:    coordinate.row_index + 1,
			column_index: coordinate.column_index + 1,
		},
		// Left
		{
			row_index:    coordinate.row_index,
			column_index: coordinate.column_index - 1,
		},
		// Right
		{
			row_index:    coordinate.row_index,
			column_index: coordinate.column_index + 1,
		},
	}
	return neighbours
}

func tokenise_lines(lines []string) ([]PartNumberToken, []SymbolToken) {
	var number_tokens []PartNumberToken
	var symbol_tokens []SymbolToken
	for row_index, line := range lines {
		var start_index = -1
		for column_index, char := range line {
			// Start of number
			if start_index == -1 && unicode.IsDigit(char) {
				start_index = column_index
			}
			// No longer part of a number!
			if start_index != -1 && !unicode.IsDigit(char) {
				end_index := column_index
				current_slice := line[start_index:end_index]
				value, err := strconv.Atoi(current_slice)
				if err != nil {
					fmt.Printf("Could not parse %s as a number!\n", current_slice)
					panic(err)
				}
				number_tokens = append(number_tokens, PartNumberToken{value: value, row_index: row_index, start_index: start_index, end_index: end_index, touched: false})
				start_index = -1
			}

			// If symbol
			if !unicode.IsDigit(char) && char != '.' {
				symbol_tokens = append(symbol_tokens, SymbolToken{character: char, row_index: row_index, column_index: column_index})
			}
		}
		if start_index != -1 {
			end_index := len(line)
			current_slice := line[start_index:end_index]
			value, err := strconv.Atoi(current_slice)
			if err != nil {
				fmt.Printf("Could not parse %s as a number!\n", current_slice)
				panic(err)
			}
			number_tokens = append(number_tokens, PartNumberToken{value: value, row_index: row_index, start_index: start_index, end_index: end_index, touched: false})
			start_index = -1
		}
	}
	return number_tokens, symbol_tokens
}

func calculate_part_number_sum(lines []string) int {
	var total = 0
	// Get tokens
	number_tokens, symbol_tokens := tokenise_lines(lines)
	for _, symbol := range symbol_tokens {
		// For every symbol look at neighbouring cells
		neighbours := get_neighbour_indices(Coordinate{row_index: symbol.row_index, column_index: symbol.column_index})
		for _, neighbour := range neighbours {
			// Check if there is a part number token occupying those cells
			for token_index := 0; token_index < len(number_tokens); token_index++ {
				token := number_tokens[token_index]
				// Ignore if already touched by a symbol
				if token.touched || token.row_index != neighbour.row_index {
					continue
				}
				// Check if touched
				in_range := neighbour.column_index >= token.start_index && neighbour.column_index < token.end_index
				if in_range {
					token.touched = true
				}
				// Mutate the number_tokens slice
				number_tokens[token_index] = token
			}
		}
	}
	// Sum all part number tokens which are touched by a symbol
	for _, token := range number_tokens {
		if token.touched {
			total += token.value
		}
	}
	return total
}

func calculate_gear_ratio_sum(lines []string) int {
	var total = 0
	// Get tokens
	number_tokens, symbol_tokens := tokenise_lines(lines)
	for _, symbol := range symbol_tokens {
		// Only look at * symbols
		if symbol.character != '*' {
			continue
		}
		// Add all adjacent part number tokens
		neighbours := get_neighbour_indices(Coordinate{row_index: symbol.row_index, column_index: symbol.column_index})
		var adjacent_values []PartNumberToken
		for _, neighbour := range neighbours {
			for _, token := range number_tokens {
				// Wrong row
				if token.row_index != neighbour.row_index {
					continue
				}
				// Only add to list if not already added and actually adjacent
				in_range := neighbour.column_index >= token.start_index && neighbour.column_index < token.end_index
				if in_range && !slices.Contains(adjacent_values, token) {
					adjacent_values = append(adjacent_values, token)
				}
			}
		}
		// * are gears if they have only two adjacent values
		if len(adjacent_values) == 2 {
			total += adjacent_values[0].value * adjacent_values[1].value
		}
	}
	return total
}

func format_execution_time(time_taken int) string {
	seconds := time_taken / 1000000000
	milliseconds := (time_taken - (seconds * 1000000000)) / 1000000
	microseconds := (time_taken - (seconds * 1000000000) - (milliseconds * 1000000)) / 1000
	nanoseconds := time_taken - (seconds * 1000000000) - (milliseconds * 1000000) - (microseconds * 1000)
	return fmt.Sprintf("%d seconds, %d milliseconds, %d microseconds, %d nanoseconds", seconds, milliseconds, microseconds, nanoseconds)
}

func main() {
	args := os.Args[1:]
	if len(args) < 1 {
		fmt.Println("Please input a path to the input file!")
		return
	}
	var num_trials = 100
	if len(args) >= 2 {
		given_trials, err := strconv.Atoi(args[1])
		if err != nil {
			fmt.Printf("Expected a numeric for num_trials! Got %s instead!", args[1])
		}
		num_trials = given_trials
	}
	data, err := os.ReadFile(args[0])
	if err != nil {
		fmt.Println("File reading error", err)
		return
	}
	lines := strings.Split(strings.ReplaceAll(string(data), "\r\n", "\n"), "\n")
	var result = 0
	var total_time = 0
	var average_time int
	// Part 1
	for i := 0; i < num_trials; i++ {
		start_time := time.Now()
		result = calculate_part_number_sum(lines)
		elapsed := time.Now().Sub(start_time)
		total_time += int(elapsed.Nanoseconds())
	}
	average_time = total_time / num_trials
	fmt.Printf("Part 1 - Result = %d\n", result)
	fmt.Printf("Part 1 took %v\n", format_execution_time(average_time))
	// Part 2
	total_time = 0
	for i := 0; i < num_trials; i++ {
		start_time := time.Now()
		result = calculate_gear_ratio_sum(lines)
		elapsed := time.Now().Sub(start_time)
		total_time += int(elapsed.Nanoseconds())
	}
	average_time = total_time / num_trials
	fmt.Printf("Part 2 - Result = %d\n", result)
	fmt.Printf("Part 2 took %v\n", format_execution_time(average_time))
}
