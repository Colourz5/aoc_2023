use std::{io::BufRead};
use std::iter::zip;

fn main() {
    let args: Vec<String> = std::env::args().collect();
    if args.len() < 2 {
        println!("Usage: {} <input file>", args[0]);
        return;
    }
    let input_path = &args[1];
    let file = std::fs::File::open(input_path).expect("Could not open file");
    let reader = std::io::BufReader::new(file);

    let lines: Vec<String> = reader.lines().map(|l| l.expect("Could not parse line")).collect();

    let result = calculate_number_of_ways_product(&lines);
    println!("Part 1 - Result {result}");
    let result = calculate_number_of_ways_product_bad_kerning(&lines);
    println!("Part 2 - Result {result}");
}

fn calculate_number_of_ways_product(lines: &Vec<String>) -> u32 {
    let mut times: Vec<u32> = Vec::new();
    let mut records: Vec<u32> = Vec::new();
    for line in lines.iter() {
        let tokens: Vec<&str> = line.split_whitespace().collect();
        let mut numbers: Vec<u32> = tokens.iter().skip(1).map(|x| x.parse::<u32>().expect("Should be a number!")).collect();
        if tokens[0] == "Time:" {
            times.append(&mut numbers);
        } else if tokens[0] == "Distance:" {
            records.append(&mut numbers);
        }
    }
    let mut result = 1;
    for (time, record) in zip(times, records) {
        let mut num_ways = 0;
        for velocity in 1..time {
            let distance = velocity * (time - velocity);
            if distance > record {
                num_ways += 1;
            }
        }
        result *= num_ways;
    }
    return result;
}

fn calculate_number_of_ways_product_bad_kerning(lines: &Vec<String>) -> u64 {
    let mut time: Option<u64> = None;
    let mut record: Option<u64> = None;
    for line in lines.iter() {
        let tokens: Vec<&str> = line.split_whitespace().collect();
        let number_token = tokens.iter().skip(1).map(|x| x.to_string()).collect::<Vec<String>>().join("");
        let number = number_token.parse::<u64>().expect("Should be a number");
        if tokens[0] == "Time:" {
            time = Some(number);
        } else if tokens[0] == "Distance:" {
            record = Some(number);
        }
    }
    let mut result = 0;
    match (time, record) {
        (Some(time), Some(record)) => {
            for velocity in 1..time {
                let distance = velocity * (time - velocity);
                if distance > record {
                    result += 1;
                }
            }
        },
        _ => {}
    };
    return result;
}
