use std::env;
use std::io::BufRead;

const VALID_DIGIT_STRINGS: [&'static str; 9] = [
    "one", "two", "three", "four", "five", "six", "seven", "eight", "nine",
];

struct BenchmarkTime {
    seconds: u64,
    milliseconds: u16,
    microseconds: u16,
    nano_seconds: u16,
}

fn main() {
    let args: Vec<String> = std::env::args().collect();
    if args.len() < 2 {
        println!("Usage: {} <input file>", args[0]);
        return;
    }
    let num_trials: u32;
    if args.len() < 3 {
        num_trials = 1000;
    } else {
        if let Some(n) = args[2].parse::<u32>().ok() {
            num_trials = n;
        } else {
            println!("Usage: {} <input file> <num trials>", args[0]);
            return;
        }
    }
    println!("Running {} trials...", num_trials);

    // Open file
    let filename = &args[1];
    let file = std::fs::File::open(filename).expect("Could not open file");
    let reader = std::io::BufReader::new(file);

    // Read file
    let lines: Vec<String> = reader
        .lines()
        .map(|l| l.expect("Could not parse line"))
        .collect();

    // Part 1
    let mut total_time = std::time::Duration::ZERO;
    let mut result = 0;
    for _ in 0..num_trials {
        let start = std::time::Instant::now();
        result = calculate_calibration_value_sum(&lines);
        total_time += start.elapsed();
    }
    println!("Part 1 - Result = {result}");
    let average_time = total_time / num_trials;
    let formatted_run_time = format_execution_time(average_time);
    println!(
        "Part 1 took {} seconds, {} milliseconds, {} microseconds, {} nanoseconds",
        formatted_run_time.seconds,
        formatted_run_time.milliseconds,
        formatted_run_time.microseconds,
        formatted_run_time.nano_seconds
    );

    // Part 2
    let mut total_time = std::time::Duration::ZERO;
    let mut result = 0;
    for _ in 0..num_trials {
        let start = std::time::Instant::now();
        result = calculate_calibration_value_sum_with_letters(&lines);
        total_time += start.elapsed();
    }
    println!("Part 2 - Result = {result}");
    let average_time = total_time / num_trials;
    let formatted_run_time = format_execution_time(average_time);
    println!(
        "Part 2 took {} seconds, {} milliseconds, {} microseconds, {} nanoseconds",
        formatted_run_time.seconds,
        formatted_run_time.milliseconds,
        formatted_run_time.microseconds,
        formatted_run_time.nano_seconds
    );
}

fn calculate_calibration_value_sum(lines: &Vec<String>) -> u32 {
    let mut result = 0;
    for line in lines {
        let mut first_digit: Option<u32> = None;
        let mut second_digit: Option<u32> = None;
        for current_char in line.chars() {
            if let Some(digit) = current_char.to_digit(10) {
                if first_digit.is_none() {
                    first_digit = Some(digit);
                    second_digit = Some(digit);
                } else {
                    second_digit = Some(digit);
                }
            }
        }
        match (first_digit, second_digit) {
            (Some(first), Some(second)) => {
                let value = 10 * first + second;
                result += value;
            }
            _ => {}
        }
    }
    return result;
}

fn calculate_calibration_value_sum_with_letters(lines: &Vec<String>) -> u32 {
    let mut result = 0;
    for line in lines {
        let mut first_digit: Option<u32> = None;
        let mut second_digit: Option<u32> = None;
        let mut char_idx = 0;
        for current_char in line.chars() {
            if let Some(digit) = current_char.to_digit(10) {
                if first_digit.is_none() {
                    first_digit = Some(digit);
                    second_digit = Some(digit);
                } else {
                    second_digit = Some(digit);
                }
            } else {
                // Handle letters
                for (digit_idx, digit_string) in VALID_DIGIT_STRINGS.iter().enumerate() {
                    let digit_string_length = digit_string.len();
                    // Slice line from char_idx to char_idx + digit_string_length
                    let start_idx = char_idx;
                    let end_idx = char_idx + digit_string_length;
                    if end_idx > line.len() {
                        continue;
                    }
                    let slice = &line[start_idx..end_idx];
                    if slice == *digit_string {
                        if first_digit.is_none() {
                            first_digit = Some((digit_idx + 1) as u32);
                            second_digit = Some((digit_idx + 1) as u32);
                        } else {
                            second_digit = Some((digit_idx + 1) as u32);
                        }
                    }
                }
            }
            char_idx += 1;
        }
        match (first_digit, second_digit) {
            (Some(first), Some(second)) => {
                let value = 10 * first + second;
                result += value;
            }
            _ => {}
        }
    }
    return result;
}

fn format_execution_time(time_taken: std::time::Duration) -> BenchmarkTime {
    let total_nanos = time_taken.as_nanos();
    let secs = total_nanos / 1_000_000_000;
    let millis = (total_nanos - 1_000_000_000 * secs) / 1_000_000;
    let micros = (total_nanos - 1_000_000_000 * secs - 1_000_000 * millis) / 1_000;
    let nanos = total_nanos - 1_000_000_000 * secs - 1_000_000 * millis - 1_000 * micros;
    return BenchmarkTime {
        seconds: secs as u64,
        milliseconds: millis as u16,
        microseconds: micros as u16,
        nano_seconds: nanos as u16,
    };
}
