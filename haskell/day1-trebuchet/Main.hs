import Data.Char (digitToInt, isDigit, toLower)
import Data.List (elemIndex, tails)
import Data.Maybe (mapMaybe)
import Debug.Trace (trace)
import System.Environment
import System.IO
import Text.Parsec (parse)

main :: IO ()
main = do
  args <- getArgs
  putStrLn "Command line arguments:"
  mapM_ putStrLn args
  let inputPath = head args
  handle <- openFile inputPath ReadMode
  lines <- readLinesToList handle
  hClose handle
  -- Part 1
  let calibrationValueSum = calculateCalibrationValueSum lines
  putStrLn ("Part 1 - Result = " ++ show calibrationValueSum)
  -- Part 2
  let calibrationValueSum = calculateCalibrationValueSumWithLetters lines
  putStrLn ("Part 2 - Result = " ++ show calibrationValueSum)

readLinesToList :: Handle -> IO [String]
readLinesToList handle = do
  iseof <- hIsEOF handle
  if iseof
    then return []
    else do
      line <- hGetLine handle
      rest <- readLinesToList handle
      return (line : rest)

calculateCalibrationValueSum :: [String] -> Int
calculateCalibrationValueSum strings = sum (map (calculateCalibrationValueFromLine collectDigitsFromLine) strings)

calculateCalibrationValueSumWithLetters :: [String] -> Int
calculateCalibrationValueSumWithLetters strings = sum (map (calculateCalibrationValueFromLine collectDigitsFromLine) strings)

calculateCalibrationValueFromLine :: (String -> [Int]) -> String -> Int
calculateCalibrationValueFromLine digitExtractor string =
  case digitExtractor string of
    [] -> 0
    digits -> 10 * head digits + last digits

collectDigitsFromLine :: String -> [Int]
collectDigitsFromLine string = map digitToInt (filter isDigit string)
